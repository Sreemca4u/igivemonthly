<?php

require_once(dirname(dirname(__DIR__)) . '/includes/includes.php');

require_once('twitteroauth/autoload.php');
use Abraham\TwitterOAuth\TwitterOAuth;

$requestToken = [];
$requestToken['oauth_token'] = $_SESSION['oauth_token'];
$requestToken['oauth_token_secret'] = $_SESSION['oauth_token_secret'];

if (isset($_REQUEST['oauth_token']) && $requestToken['oauth_token'] !== $_REQUEST['oauth_token']) {
    echo 'Get Twitter OAUTH Token error';
} else {
    
    $connection = new TwitterOAuth(Config::twitterConsumerKey(), Config::twitterConsumerSecret(), $requestToken['oauth_token'], $requestToken['oauth_token_secret']);
    
    try {
        $accessToken = $connection->oauth("oauth/access_token", ["oauth_verifier" => $_REQUEST['oauth_verifier']]);
    } catch (Exception $e) {
        echo 'Error Getting an Access Token. Caught exception:<br /><br />' . $e->getMessage();
    }
    
    if ($connection->getLastHttpCode() == 200) {
        
        $_SESSION['access_token'] = $accessToken;
        $connection = new TwitterOAuth(Config::twitterConsumerKey(), Config::twitterConsumerSecret(), $accessToken['oauth_token'], $accessToken['oauth_token_secret']);
        
        try {
            $twitterUser = $connection->get("account/verify_credentials");
        } catch (Exception $e) {
            echo 'Error Getting Account Details. Caught exception:<br /><br />' . $e->getMessage();
        }
        
        #die(var_dump($twitterUser));
        
        if ($connection->getLastHttpCode() == 200 && is_object($twitterUser) && isset($twitterUser->screen_name) && isset($twitterUser->name)) {

            $_SESSION['twitter_name'] = $twitterUser->screen_name;
            $_SESSION['twitter_image'] = $twitterUser->profile_image_url;
            $_SESSION['twitter_id'] = $twitterUser->name;
            
            # Now we need to scan the database and see if there is a Twitter handle matching the authorized Twitter id.
            
            $check__s = "SELECT * FROM sponsored_charities WHERE twitter=?";
            $check__q = $DB->prepare($check__s);
            $check__q->execute(array($twitterUser->name));
            
            if ($check__q->rowCount() != NULL) {

                $check__d = $check__q->fetchObject();
                $_SESSION['charity_id'] = $check__d->id;
                /*unset($_SESSION['user_id']); // log the member out to avoid conflicts

                $_SESSION['user_id'] = false;
                unset($_SESSION['user_id']);
                session_write_close();*/

                header("location: /charity/$check__d->id/");
                exit;

            } else {

                header('location: /pages/admin/charity-sign-in-fail/');
                exit;

            }
            
        } else {
            echo 'Error Getting Account Details';
        }
        
    } else {
        echo 'Error Getting an Access Token';
    }
    
}