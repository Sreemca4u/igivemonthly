<?php

require_once(dirname(dirname(__DIR__)) . '/includes/includes.php');

require_once('twitteroauth/autoload.php');
use Abraham\TwitterOAuth\TwitterOAuth;
if (isset($_SESSION['name']) && isset($_SESSION['twitter_id'])) {# Check whether user already logged in with twitter
	echo "Name :".$_SESSION['name']."<br>";
	echo "Twitter ID :".$_SESSION['twitter_id']."<br>";
	echo "Image :<img src='".$_SESSION['image']."'/><br>";
	echo "<br/><a href='logout.php'>Logout</a>";
} else {# Not logged in
    
    $connection = new TwitterOAuth(Config::twitterConsumerKey(), Config::twitterConsumerSecret());
    
    # Generate a Request Token
    try {
        $requestToken = $connection->oauth('oauth/request_token', array('oauth_callback'=>Config::twitterCallbackUrl()));
    } catch (Exception $e) {
        echo 'Error Generating a Request Token. Caught exception:<br /><br />' . $e->getMessage();
    }
    
    if ($connection->getLastHttpCode() == 200 && is_array($requestToken)) {
        $_SESSION['oauth_token'] = $requestToken['oauth_token'];
        $_SESSION['oauth_token_secret'] = $requestToken['oauth_token_secret'];
        
        # Build  Authorize URL
        try {
            $url = $connection->url('oauth/authorize', array('oauth_token'=>$requestToken['oauth_token']));
        } catch (Exception $e) {
            echo 'Error Building Authorize URL. Caught exception:<br /><br />' . $e->getMessage();
        }

        #if ($connection->getLastHttpCode() == 200) {
        if (!empty($url) && is_string($url)) {
            header('Location: ' . $url);
            exit;
        } else {
            echo 'Error Building Authorize URL';
        }
        
    } else {
        echo 'Error Generating a Request Token';
    }

}