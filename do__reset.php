<?php

require_once('includes/includes.php');

$email = $_REQUEST['email'];

$unique__key = md5(rand() . time());

$check__s = "SELECT id, email, first_name FROM users WHERE email = ? LIMIT 1";
$check__q = $DB->prepare($check__s);
$check__q->execute(array($email));

if($check__q->rowCount() != NULL) {

	$check__d = $check__q->fetchObject();
	$s = "UPDATE users SET reset_key = ? WHERE id = ?";
	$q = $DB->prepare($s);
	$q->execute(array($unique__key, $check__d->id));

# Now send email verification 

	$body_client = 'Dear ' . ucfirst($check__d->first_name) .'<br /><br />';
	$body_client = "You are receiving this e-mail because you requested a password reset for your user account at igivemonthly.com.<br /><br />";
	$body_client .= "Please go to the following page and choose a new password.<br /><br />";
	$body_client .= "<a href='http://igivemonthly.kloc.co.uk/pages/admin/reset-password/?key=".$unique__key."/'>click here</a><br /><br />";
	$body_client .= "If however this wasn't you please <a href='http://www.igivemonthly.com/process.php?a=disavow__reset__email.php&id=$check__d->id'>click here<br /><br />";
	$body_client .= "Thanks for using our site!<br /><br />";
	$body_client .= "The iGiveMonthly Team";

	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= "From: noreply@igivemonthly.com \r\n";
	$headers .= "Reply-To: noreply@igivemonthly.com \r\n";

	if(mail($check__d->email, "IGM - Reset Password", $body_client, $headers)) {
	     header('location: pages/admin/forgot-sent/');
    } else {
        echo $body_client; // REMOVE ON GO LIVE. Purely for testing and demo purposes
    }

	


} else {

	header('location: pages/admin/forgot-not-found/');

}