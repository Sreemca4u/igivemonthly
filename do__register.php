<?php

require_once('includes/includes.php');

$_SESSION['fname'] = $form__firstname = $_POST['fname'];
$_SESSION['lname'] = $form__lastname = $_POST['lname'];
$_SESSION['email'] = $email = $form__email = strtolower($_POST['email']);

$form__password = $_POST['password'];
$form__password = md5(SALT . $form__password);

//$form__dob = $_POST['dob'];
$_SESSION['gender'] = $form__gender = $_POST['gender'];

$form__remember = isset($_POST['remember'])?1:0;

//$form__dob = DateTime::createFromFormat('j/n/Y', $form__dob)->getTimestamp();
//$form__dob = date('Y-m-d', $form__dob);

# We always use FORM data to register the user
//print "<p>FORM Data: Fname: $form__firstname Lname: $form__lastname Email: $form__email Password: $form__password DOB: $form__dob Gender: $form__gender</p>";
//print "<p>SOCIAL Data: User token: $user_token Gender: $gender Fname: $firstname Lname: $lastname Email: $email</p>";


#	Check if registered already
$already__q = $DB->prepare('select * from users where email = ?');
$already__q->execute(array($email));

if($already__q->rowCount() > 0) {

	header('location: pages/admin/register-found/');
	exit;

}

# Register this user in OUR database
if (isset($form__image)) {
    $S = "insert into users (first_name, last_name, email, password, status, profile, gender, image_url, social_url, created_at, updated_at) values (?, ?, ?, ?, 'NOT_VERIFIED', 'Something about yourself', ?, ?, ?, now(), now())";
    $Q = $DB->prepare($S);
    $Q->execute(array($form__firstname, $form__lastname, $form__email, $form__password, $form__gender, $form__image, $form__image));
} else {
    $S = "insert into users (first_name, last_name, email, password, status, profile, gender, created_at, updated_at) values (?, ?, ?, ?, 'NOT_VERIFIED', 'Something about yourself', ?, now(), now())";
    $Q = $DB->prepare($S);
    $Q->execute(array($form__firstname, $form__lastname, $form__email, $form__password, $form__gender));
}
$M = $DB->lastInsertId();

# Now add info in the user_social_link table
if(isset($_SESSION['fb_token'])) {
	LinkFBTokenToUserId($_SESSION['fb_token'], $M);
	GetFBProfileImage($_SESSION['fb_token'], $M);
}
if(isset($_SESSION['gplus_token'])) {
    
	LinkGoogleTokenToUserId($_SESSION['gplus_token'], $M);
	#GetGoogleProfileImage($_SESSION['gplus_token'], $M);
}
//if($form__remember == 'Y') { setcookie('usertoken', $user_token, time() + (86400 * 30), "/"); }; // 86400 = 1 day

$_SESSION['user_id'] = $M;

header("location: member/$M/");
