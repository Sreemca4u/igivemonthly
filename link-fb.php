<?php

require_once('includes/includes.php');
require_once('pages/members/include__member__functions.php');

$fb = new \Facebook\Facebook(array('app_id'=>Config::facebookAppId(), 'app_secret'=>Config::facebookAppSecret(), 'default_graph_version'=>Config::facebookVersion()));# NOTE: Config Facebook
$response = $fb->get('/me?fields=id,first_name,last_name,email,gender', $_GET['token']);

$DB->prepare('update users set facebook_id = ? where id = ?')
    ->execute(array($response->getGraphUser()->getId(), $_SESSION['user_id']));

header('location: pages/members/link/');