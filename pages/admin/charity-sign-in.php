<div id="crumbs"><div class="inner"><p><strong><a href="pages/home/">Home</a></strong> / Sign-in</p></div></div>

<div id="content">
	<div class="inner">
		<div class="left">
			<h3>Charity Sign-in</h3>
			<p>Sign-in via below, if you have an issue signing in, please contact us.</p>
			<p><a href="scripts/login-with-twitter/login.php"><img src="sketch/sign-in-with-twitter-gray.png" alt="sign-in-with-twitter-gray" /></a></p>
		</div>
	</div>
</div>