<script type="text/javascript">
	var oa = document.createElement('script');
	oa.type = 'text/javascript'; oa.async = true;
	oa.src = '//igivemonthly.api.oneall.com/socialize/library.js'
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(oa, s)
</script>


<!-- <div id="crumbs"><div class="inner"><p><strong><a href="pages/home/">Home</a></strong> / Reset</p></div></div> -->


<div id="content">
	<div class="inner">
		<div class="left wide center">


			<h3>Reset Password</h3>

			<?php // Quick implementation to avoid changing htaccess

				$key = basename($_SERVER['REQUEST_URI']); // Get key from querystring
				$key = str_replace('?key=', '', $key); // Strip out querystring heading
			?>

			<form action="do__reset__password.php" method="post" class="center">
				<h3>Reset your password by typing it in below</h3>
				<dl class="double"><dt>Password</dt><dd><input type="password" name="password" /></dd></dl>
				<dl class="double"><dt>Confirm</dt><dd><input type="password" name="confirm" /></dd></dl>
				<p><input type="submit" value="Reset Password" /></p>
				<p>No account yet? <a href="pages/admin/register/">Sign up</a> now, it's quick and free.</p>
					<input type="hidden" name="key" value="<?php echo $key; ?>" />
			</form>

		</div>
	</div>
</div>
