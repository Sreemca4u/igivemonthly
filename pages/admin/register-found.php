<script type="text/javascript">
	var oa = document.createElement('script');
	oa.type = 'text/javascript'; oa.async = true;
	oa.src = '//igivemonthly.api.oneall.com/socialize/library.js'
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(oa, s)
</script>




<div id="crumbs"><div class="inner"><p><strong><a href="pages/home/">Home</a></strong> / Register</p></div></div>

<meta name="google-signin-scope" content="profile email">
<meta name="google-signin-client_id" content="<? echo Config::googleClientId(); /* NOTE: Config Google Sign On Client Id */ ?>">
<script src="https://apis.google.com/js/platform.js?onload=startApp" async defer></script>

<div id="content">
	<div class="inner">
		<div class="left wide center">
			<h3>Register with a social network</h3>

			<script>
				// This is called with the results from from FB.getLoginStatus().
				function statusChangeCallback(response) {
					if (response.status === 'connected') {
						// Logged into your app and Facebook.
						fetchAPI(response);
					}
				}

				// This function is called when someone finishes with the Login
				// Button.  See the onlogin handler attached to it in the sample
				// code below.
				function checkLoginState() {
					FB.getLoginStatus(function(response) {
						statusChangeCallback(response);
					});
				}

				window.fbAsyncInit = function() {
					FB.init({
						appId      : '<? echo Config::facebookAppId(); /* NOTE: Config Facebook App ID */ ?>',
						cookie     : true,  // enable cookies to allow the server to access
											// the session
						xfbml      : true,  // parse social plugins on this page
						version    : '<? echo Config::facebookVersion(); /* NOTE: Config Facebook Version */ ?>'
					});

				};

				// Load the SDK asynchronously
				(function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) return;
					js = d.createElement(s); js.id = id;
					js.src = "//connect.facebook.net/en_US/sdk.js";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));

				function fetchAPI(response) {
					window.open('login-fb.php?token='+response.authResponse.accessToken, '_self');
				}

				function doFbLogin() {
					FB.login(checkLoginState, 'public_profile,email');
				}
			</script>
			<script>
				function onSignIn(googleUser) {
					window.open('login-gplus.php?token='+JSON.stringify(googleUser.getAuthResponse()), '_self');
				};
				var startApp = function() {
					gapi.load('auth2', function(){
						// Retrieve the singleton for the GoogleAuth library and set up the client.
						auth2 = gapi.auth2.init({
							client_id: '<? echo Config::googleClientId(); /* NOTE: Config Google Sign On Client Id (Again!)*/ ?>',
							cookiepolicy: 'single_host_origin',
							// Request scopes in addition to 'profile' and 'email'
							//scope: 'additional_scope'
						});
						attachSignin(document.getElementById('gplus_login'));
					});
				};
				function attachSignin(element) {
					auth2.attachClickHandler(element, {},
							onSignIn, function(error) {
								alert(JSON.stringify(error, undefined, 2));
							});
				}
			</script>

			<!-- The plugin will be embedded into this div //-->
			<div>
				<div style="display: inline-block; vertical-align: middle;"><img src="images/facebook.png" style="cursor: pointer" onclick="doFbLogin()" /></div>
				<div style="display: inline-block; vertical-align: middle;"><img src="images/gplus.png" style="cursor: pointer" id="gplus_login" /></div>
			</div>

			<br clear="all" />
			
			<h2><ins>OR</ins></h2>

			<!--<p>You've successfully signed in with <b><?php print ucfirst($_SESSION['user_service']); ?></b> but your <?php print ucfirst($_SESSION['user_service']); ?> account isn't linked with an
			iGiveMonthly account. Please <a href="pages/admin/login/">Login</a> (where you can link your account in settings) or register below.</p>-->
			
			<p><b style='color: #cc0000;'>Email address already present, please login or register with a different email address</b></p>


			<form action="do__register.php" method="post" class="center" id="validate">
				<h3>Sign up with your email address</h3>
				<dl class="double"><dt>First name:</dt><dd><input type="text" name="fname" value="<?php print $_SESSION['fname']; ?>" required /></dd></dl>
				<dl class="double"><dt>Last name:</dt><dd><input type="text" name="lname" value="<?php print $_SESSION['lname']; ?>" required /></dd></dl>
				<dl class="double">
					<dt>Gender</dt>
					<dd>
						<select name="gender">
							<option value="male" <?php if(strtolower($_SESSION['gender']) == 'male') { print 'selected="selected"'; } ?>>Male</option>
							<option value="female" <?php if(strtolower($_SESSION['gender']) == 'female') { print 'selected="selected"'; } ?>>Female</option>
						</select>
					</dd>
				</dl>
				<dl class="double"><dt>Email</dt><dd><input type="text" name="email" value="<?php print $_SESSION['email']; ?>" required /></dd></dl>
				<dl class="double"><dt>Password</dt><dd><input type="password" name="password" required /></dd></dl>
				<dl class="double"><dt>Retype Password</dt><dd><input type="password" name="re__type__password" required /></dd></dl>
				<dl class="single"><dt>Agree to terms and conditions?</dt><dd><input type="checkbox" name="agreed__terms" value="Y" required /></dd></dl>
				<p><input type="submit" value="Register" /></p>
			</form>


		</div>


		</div>
	</div>
</div>
