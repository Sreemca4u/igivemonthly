<div id="crumbs"><div class="inner"><p><strong><a href="pages/home/">Home</a></strong> / Sign-in</p></div></div>


<div id="content">
	<div class="inner">
		<div class="left">
			
			
			<h3>Charity Sign-in</h3>
			
			
			<p>I'm sorry the Twitter handle <strong>'<?php print "$_SESSION[twitter_id]"; ?>'</strong> wasn't found in our records. If this is an error, please contact us.</p>
			
			<p>If a charity is not listed, you can submit the charity to us <a href="pages/charities/not-listed/">here</a></p>
			

		</div>
	</div>
</div>