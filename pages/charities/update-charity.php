<div id="content">
	<div class="inner">
		<div class="left wide">
			<h3>Update Profile</h3>
			<p>Update your profile &amp; add contact details for us to contact you.</p>
            <p>* (Required)</p>
			<?php
			$charity__s = "SELECT profile, contact_name, contact_job_title, contact_telephone, contact_email FROM charities WHERE id = ? LIMIT 1";
			$charity__q = $DB->prepare($charity__s);
			$charity__q->execute(array($_SESSION['charity_id']));
			$charity__d = $charity__q->fetchObject();

			// print "<p>@ $charity__s @</p>";
			?>
			<hr />
			<form action="process.php" method="post">
				<fieldset>
					<dl><dt>Profile:</dt><dd><textarea name="profile" rows="10" id="descTextArea"><?php print $charity__d->profile; ?></textarea><br /><span id="stat"></span></dd></dl>
					<dl><dt>Contact Name: *</dt><dd><input type="text" name="contact_name" value="<?php print $charity__d->contact_name; ?>" required="requied"></dd></dl>
                    <dl><dt>Contact Job Title: *</dt><dd><input type="text" name="contact_job_title" value="<?php print $charity__d->contact_job_title; ?>" required="requied"></dd></dl>
                    <dl><dt>Contact Telephone Number: *</dt><dd><input type="text" name="contact_telephone" value="<?php print $charity__d->contact_telephone; ?>" required="requied"></dd></dl>
                    <dl><dt>Contact Email Address: *</dt><dd><input type="email" name="contact_email" value="<?php print $charity__d->contact_email; ?>" required="requied"></dd></dl>
				</fieldset>
				<p><input type="submit" value="Update Profile" /></p>
				<input type="hidden" name="a" value="update-charity-profile.php" />
			</form>
			<hr />
		</div>
	</div>
</div>
<script>
	function countChar(val) {
	    var len = val.value.length;
	    if (len >= 500) {
	        val.value = val.value.substring(0, 500);
	        $('#stat').text(0);
	    } else {
	        $('#stat').text(500 - len + ' remaining');
	    }
	}
	countChar($('#descTextArea').get(0));
	$('#descTextArea').keyup(function() {
	    countChar(this);
	});
</script>