<div id="content">
	<div class="inner">
		<div class="left wide">
			<?php 

			$cid = $_SESSION['charity_id'];

			$charity__s = "SELECT name FROM charities WHERE id = ? LIMIT 1";
			$charity__q = $DB->prepare($charity__s);
			$charity__q->execute(array($cid));
			$charity__d = $charity__q->fetchObject();

			print '<h3>Thank You</h3>';
			print "<p>Thank you for sponsoring <strong>". $charity__d->name . "</strong>. If you are a charity, you can now post news articles on your <a href='charity/$cid/'>web page</a>.</p>";			
			print "<p>This charity is now able to login with their official Twitter handle to edit their enhanced profile page.</p>";		

			?>			
		</div>
	</div>
</div>