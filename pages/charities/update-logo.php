<div id="content">
	<div class="inner">
		<div class="left wide">
			
			
			<h3>Update Logo</h3>
			
			
			<p>Update your logo.</p>
			
			
			<?php
			
			
			$charity__s = "SELECT * FROM live__node__data WHERE aim = '14' AND id = ? LIMIT 1";
			$charity__q = $DB->prepare($charity__s);
			$charity__q->execute(array($_SESSION['charity_id']));
			$charity__d = $charity__q->fetchObject();
			
			if(isset($_SESSION['error'])) { print "<p class='red'><strong>$_SESSION[error]</strong></p>"; }
			$_SESSION['error'] = false;
			unset($_SESSION['error']);
			session_write_close();
			
			
			?>
			
			<p><strong>Please note: Image dimensions must be 200px wide and 100px high.</strong></p>
			
			
			<form action="process.php" method="post" enctype="multipart/form-data">
				<fieldset>
					<dl><dt>File:</dt><dd><input type="file" name="file__to__upload" id="file__to__upload"></dd></dl>
				</fieldset>
				<p><input type="submit" value="Upload Logo" /></p>
				<input type="hidden" name="a" value="update-charity-logo.php" />
			</form>
			
			
			<hr />

		</div>
	</div>
</div>