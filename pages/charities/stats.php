<!--<div id="crumbs"><div class="inner"><p><strong><a href="pages/home/">Home</a> / <a href="member/<?php print $_SESSION['auth__token']; ?>/">Profile</a> </strong> / Stats</p></div></div>-->
<div id="content">
	<div class="inner">
		<div class="left wide">
			<h3>Stats</h3>
			<?php
			
			$sid = $_SESSION['charity__id'];
			
			# Range, will need to be clever
			
            $filter = null;
			if (isset($_REQUEST['filter'])) {
                $filter = $_REQUEST['filter'];
            }
            $from = null;
            if (isset($_POST['from'])) {
                $from = $_POST['from'];
            }
            $to = null;
            if (isset($_POST['to'])) {
                $to = $_POST['to'];
            }
			$params = [];
			if($filter == 'month') { 
				$S = "SELECT * FROM charity_donations WHERE when_donated > DATE_SUB(NOW(), INTERVAL 1 MONTH) AND charity_id  = ? ORDER BY when_donated DESC";
				$params = [$sid];
				$header__text = '<h3>Last month</h3>';
			} elseif($filter == 'year') {
				$S = "SELECT * FROM charity_donations WHERE when_donated > DATE_SUB(NOW(), INTERVAL 12 MONTH) AND icharity_idd  = ? ORDER BY when_donated DESC";
				$params = [$sid];
				$header__text = '<h3>Last 12 months</h3>';
			} elseif($filter == 'quarter') {
				$S = "SELECT * FROM charity_donations WHERE when_donated > DATE_SUB(NOW(), INTERVAL 3 MONTH) AND charity_id  = ? ORDER BY when_donated DESC";
				$params = [$sid];
				$header__text = '<h3>Last 3 months</h3>';
			} elseif($filter == 'custom') {
				$S = "SELECT * FROM charity_donations WHERE when_donated BETWEEN ? AND ? AND charity_id = ? ORDER BY when_donated DESC";
				$params = [$from, $to, $sid];
				$header__text = "<h3>From: $from To: $to</h3>";
			} else {
				$S = "SELECT * FROM charity_donations WHERE charity_id = ? ORDER BY when_donated DESC";
				$params = [$sid];
				$header__text = '<h3>All</h3>';
			}
			
			// print "<p>@ $S @</p>";
			
			$Q = $DB->prepare($S);
			$Q->execute($params);

			print '<ul class="filter">';
			print "<li><a href='charitystats/$sid/month/'>Last 30 days</a></li>";
			print "<li><a href='charitystats/$sid/quarter/'>Last 90 days</a></li>";
			print "<li><a href='charitystats/$sid/year/'>Yearly</a></li>";
			print "<li><a href='charitystats/$sid/all/'>All</a></li>";
			print "<li><form action='charitystats/$sid/custom/' method='post' class='filter'>Custom filter: ";
			print "<input type='text' name='from' placeholder='From' class='datetimepicker' value='$from' />";
			print "<input type='text' name='to' placeholder='To' class='datetimepicker' value='$to' />";
			print '<input type="submit" value="Filter Range" style="position: absolute;"/>';
			print "</form></li>";
			print '</ul>';	
			
			print $header__text;
			
			if($Q->rowCount() != NULL) {
			
				for($t = 0; $t < $Q->rowCount(); $t++) {

					$D = $Q->fetchObject();
					$D->when_donated = date('d F 2015', strtotime($D->when_donated));

					print '<dl class="strip stats">';
					print '<dd>' . $D->when_donated . '</dd>';
					#if($D->av == 'MONEY') { print "<dd>&pound;$D->ap</dd>"; } else { print "<dd>Hrs. $D->ap</dd>"; }
                    print "<dd>&pound;$D->amount</dd>";
					print '<dd class="wide"><em>' . $D->first_name . ' ' . $D->last_name . '</em></dd>';
					//print "<dd class='edit'><a href='process.php?a=delete__donation.php&did=$D->id' class='confirm'><img src='sketch/delete.png' style='width: 16px;' /></a></dd>";
					//print "<dd class='edit'><a href='pages/members/edit-donation/$D->id/'><img src='sketch/pencil_go.png' style='width: 16px;' /></a></dd>";
					print '</dl>';
			
				}
			
			} else {
				
				print "<p>No dates found.</p>";
				
			}

			/*function isFav($cid) {
				global $DB;
				$join__s = "SELECT * FROM live__node__data WHERE NODE = '132' AND AK = ? AND BK = ? LIMIT 1";
				$join__q = $DB->prepare($join__s);
				$join__q->execute(array($cid, $_SESSION['auth__id']));
				if($join__q->rowCount() != NULL) {
					print '<dd><img src="sketch/star.png" alt="star" width="23" height="20" class="auto" /></dd>';
				};
			}*/
			
			?>
		</div>
	</div>
</div>