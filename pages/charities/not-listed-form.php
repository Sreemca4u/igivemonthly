<!--<div id="crumbs"><div class="inner"><p><strong><a href="pages/home/">Home</a></strong> / Browse Charities</p></div></div>-->


<div id="content">
	<div class="inner">
		<div class="left wide">


			<h3>Suggest A Charity</h3>

			<p>We have a growing list charities. If however your charity is not listed, you can submit here.</p>

			<form action="process.php" method="post" id="validate">


				<fieldset>
					<dl><dt>Charity Name *</dt><dd><input type="text" name="name" required autocomplete="off" /></dd></dl>
					<dl><dt>Charity Website *</dt><dd><input type="text" name="website" required autocomplete="off" /></dd></dl>
					<dl><dt>Charity Number</dt><dd><input type="text" name="charity__number" autocomplete="off" /></dd></dl>
					<dl><dt>Twitter Page</dt><dd><input type="text" name="twitter_page" autocomplete="off" /></dd></dl>
				</fieldset>


				<fieldset>
					<dl><dt>Your Name *</dt><dd><input type="text" name="person_name" required autocomplete="off" /></dd></dl>
					<dl><dt>Your Email Address *</dt><dd><input type="email" name="person_email" required autocomplete="off" /></dd></dl>
					<dl><dt>Confirm You Represent A Charity *</dt><dd><input type="checkbox" name="repro" value="y" required autocomplete="off" /></dd></dl>
				</fieldset>


				<p><input type="submit" value="Submit" /></p>


				<input type="hidden" name="a" value="suggest-charity.php" />


			</form>


		</div>
	</div>
</div>
