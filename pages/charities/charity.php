<script async src="//cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
<script>
	$(function() {
		$('#theurlid').on('paste', function () {
			var element = this;
			setTimeout(function () {
				var text = $(element).val();
				the_url = $('#theurlid').val();
				// alert(the_url);
				$('#thecardinside').append("<a class='embedly-card' href='"+ the_url +"'></a>");
			}, 100);
		});

		$('#show__cards').click(function() {
			$('.hide__card').slideToggle();
			($('#show__cards').text() === "More News Articles") ? $('#show__cards').text("Less News Articles") : $('#show__cards').text("More News Articles");

			return false;
		});
	});

	$(function() {

		<?php if(isset($_SESSION['user_id'])) { print '$("body").addClass("crumbs");'; }; ?>

	});
</script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

<?php

	$d = $_GET['d'];
	$charity__s = "SELECT * FROM charities WHERE id = ? LIMIT 1";
	$charity__q = $DB->prepare($charity__s);
	$charity__q->execute(array($d));
	$charity__d = $charity__q->fetchObject();
	$charity__d->description = preg_replace('/\n/', '<br />', $charity__d->description);

	$_SESSION['charity__id'] = $charity__d->id;


if(isset($_SESSION['user_id'])) { ?>

<div id="crumbs">
	<div class="inner">
		<p>
			<a href="pages/members/add-donation/<?php echo $charity__d->id; ?>/" class="green__button fright">Add Donation</a>
			<?php following($charity__d->id, 'green__button fright'); ?>
		</p>
	</div>
</div>

<?php } ?>

<div id="content">
	<div class="inner">
		<div class="left">
			<?php


			# If charity login id matches twitter login handle, then this is the authorised


			$authorised__twitter__user = false;

			// if($d == $_SESSION['charity__id']) { $authorised__twitter__user = true; }

			if (array_key_exists('charity_id', $_SESSION)) {

				if ($d == $_SESSION['charity_id']) {

					$authorised__twitter__user = true;

				}

			}

			//if($_SESSION['twitter_id'] == $charity__d->ev) { $authorised__twitter__user = true; } else { $authorised__twitter__user = false; }
# Sponsored

			$sponsored__b = false;

			$sponsored__s = "SELECT * FROM sponsored_charities WHERE id = ? LIMIT 1";
			$sponsored__q = $DB->prepare($sponsored__s);
			$sponsored__q->execute(array($charity__d->id));
            
			if($sponsored__q->rowCount() != NULL) {
                
                $sponsored__d = $sponsored__q->fetchObject();
				$sponsored__b = true;

				print '<div class="sponsored__by">';
				print " Page sponsored by: <b>$sponsored__d->first_name $sponsored__d->last_name</b>";
				print "</div>";

			}

# Charity logo and upload new one			
					
					
			if($authorised__twitter__user == true && $sponsored__b == true) {

				if($charity__d->image_name != NULL) {

					if(is_file("uploads/charity-logos/$charity__d->image_name")) {

						print "<img src='uploads/charity-logos/$charity__d->image_name' class='charity__logo' />";						

					} else {

						print "<img src='sketch/logo--coming--soon.png' class='charity__logo' /><br />";
					}

				} else {

					print "<img src='sketch/logo--coming--soon.png' class='charity__logo' /><br />";

				}

				print "<p>";
				print "<a href='pages/charities/update-logo/' class='green__button' style='margin-left: 0px; '>Upload new logo</a> ";
				print "<a href='pages/charities/update-charity/' class='green__button' style='margin-left: 5px; '>Update Profile</a>";
				print "</p>";

			} else {

				if($charity__d->image_name != NULL) {
							
					if(is_file("uploads/charity-logos/$charity__d->image_name")) {

						print "<img src='uploads/charity-logos/$charity__d->image_name' class='charity__logo' />";						

					} else {

						print "<img src='sketch/logo--coming--soon.png' class='charity__logo' /><br />";
					}
									

				} else {

					print "<img src='sketch/logo--coming--soon.png' class='charity__logo' /><br />";

				}

			}

# Charity information

			print "<h3>$charity__d->name</h3>";

			if (!empty($charity__d->number)) {

				print "<p>Charity Number: $charity__d->number</p>";

			}

			if (!empty($charity__d->website)) {

				print "<p>Website: <a href='http://$charity__d->website' target='_new'>$charity__d->website</a></p>";

			}

			print "<h4>About Us</h4>";
			print "<p>$charity__d->description</p>";

			if (!empty($charity__d->twitter)) {

				print "<br /><h4>Contact Us</h4>";
				print "<p><a href='http://www.twitter.com/$charity__d->twitter' target='_new'>Follow us on Twitter: @$charity__d->twitter</a></p>";

			}

			?>
            <hr />
			<h4>Share</h4>

			<!-- AddToAny BEGIN -->

			<div class="a2a_kit a2a_kit_size_32 a2a_default_style">
				<a class="a2a_button_facebook"></a>
				<a class="a2a_button_twitter"></a>
				<a class="a2a_button_google_plus"></a>

			</div>
			<script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>

			<!-- AddToAny END -->

			<hr />
			<?php
# news

			if($authorised__twitter__user == true && $sponsored__b == true) {

				print '<form action="process.php" method="post">';
				print '<fieldset>';
				print '<dl><dt>Paste URL</dt><dd><input type="text" name="theurl" id="theurlid" /></dd></dl>';
				print '<div id="thecardinside"></div>';
				print '<p><input type="submit" value="Add News" /></p>';
				print '<input type="hidden" name="a" value="add-charity-news.php" />';
				print '</fieldset>';
				print '</form>';
				print '<br />';

			}

            # News

			$news__s = "SELECT * FROM news WHERE charity_id = ? ORDER BY created_at DESC LIMIT 100";
			$news__q = $DB->prepare($news__s);
			$news__q->execute(array($charity__d->id));
            if($news__q->rowCount() > 0) {

				print "<h3>News</h3>";
				$items = $news__q->rowCount();
				for($ne = 0; $ne < $items; $ne++) {
					$news__d = $news__q->fetchObject();
					$over = '';
					if($ne > 4) { $over = 'hide__card'; } else { $over = ''; }
					print "<div class='card $over'><a class='embedly-card' href='$news__d->url'></a>";
					if($authorised__twitter__user == true) { print "<p><a href='process.php?a=delete-news-article.php&id=$news__d->id' class='delete__card green__button'>Delete</a></p>"; }
					print "</div>";
				}
				if($news__q->rowCount() > 9) { print '<br /><br /><p><a href="#" id="show__cards" class="green__button">More News Articles</a></p>'; }
			}
			?>
		</div><div class="right"><div class="padd">
			<div class="boxy">
				<h5>Friends of <?php echo $charity__d->name; ?></h5>
				<p>List of members that are friends:</p>

				<?php list__members($d); ?>

				<p><?php # NOTE: Friends
                    #if(isset($_SESSION['user_id'])) { if($authorised__twitter__user == false) { following($d, 'green__button'); } }
                    if(isset($_SESSION['user_id'])) { following($d, 'green__button'); }
                    ?></p>
			</div>

<!-- How much you have donated to this charity -->

			<?php
			if(isset($_SESSION['user_id'])) {

				$donated__to__charity__s = "SELECT sum(amount) AS total FROM donations WHERE user_id = ? AND charity_id = ?";
				$donated__to__charity__q = $DB->prepare($donated__to__charity__s);
				$donated__to__charity__q->execute(array($_SESSION['user_id'], $charity__d->id));

				print '<div class="boxy personal">';

				print '<h5>Total donations you\'ve made</h5>';

				//print "<p>$donated__to__charity__s</p>";


				if($donated__to__charity__q->rowCount() != NULL) {
					$donated__to__charity__d = $donated__to__charity__q->fetchObject();
					if($donated__to__charity__d->total > 0) {
						print "<p>&pound;$donated__to__charity__d->total</p>";

					} else {

						print "<p>You have not recorded any donations to '$charity__d->name' if you would like to, click here. </p>";

						print "<p><a href='pages/members/add-donation/$d/' class='green__button'>Add donation</a></p>";

					}

				};

				print '</div>';

			}

			?>
<?php
				if($sponsored__q->rowCount() == NULL) {
					print '<div class="boxy">';
					print '<h5>Claim This Page</h5>';
					print '<p>Claim this page and once authenticated you can benefit from:</p>';
					print '<p>';
					print '<em>- Twitter timeline embedded on your page.</em><br />';
					print '<em>- Stats of visitors and tracked donations.</em><br />';
					print '<em>- Add news articles, promote your charity.</em><br />';
					print '</p>';
					require_once('scripts/stripe/lib/Stripe.php');
					Stripe::setApiKey(Config::stripeApiKey());# NOTE: Config Stripe API Key
					print "<p><strong>Grand Total: &pound;85.00</strong></p>";
					print '<p><i>Please note: that you won\'t be able to edit content unless you are logged in as the charity via Twitter.</i></p>';
					print '<p>Secure payments information <a class="popup" href="popup.php">here</a></p>';

					if(basename($_SERVER['REQUEST_URI']) == '?error=true') {
						print '<p class="error">Unfortunately there was a problem processing your payment. Please try again.</p>';
					}

					print '<form action="process.php" method="post" class="basket_form">';
					print '<script src="https://checkout.stripe.com/checkout.js" class="stripe-button"';
					print 'data-key="' . Config::stripeDataKey() . '"';# NOTE: Config Stripe Data Key
					// $price_int = filter_var(850.00, FILTER_SANITIZE_NUMBER_INT);
					print 'data-amount="8500"';
					print 'data-currency="gbp"';
					print 'data-name="I Give Monthly"';
					print 'data-description="'. $charity__d->name . '">';
					print '</script>';
					print '<input type="hidden" name="a" value="sponsor-charity.php" />';
					print '<input type="hidden" name="cid" value="' . $d . '" />';
					print '<br /><br />';
					print '</form>';
					print '</div>';
				}

			if($authorised__twitter__user == false && $sponsored__b == true) {

				print '<div class="boxy">';

				print '<h5>Charity sign-in</h5>';

				print "<p><a href='/scripts/login-with-twitter/login.php'><img src='sketch/sign-in-with-twitter-gray.png' alt='sign-in-with-twitter-gray' /></a></p>";

				print '</div>';

			}

			?>
			<div class="boxy">
				<h5>Money tracked so far:</h5>
				<?php

					//$amount = charity_raised($d); print "$amount";

					print trackedCharityTotal($_SESSION['country'], $d);

					if($authorised__twitter__user == true && $sponsored__b == true) { print "<p><a href='charitystats/$_SESSION[charity__id]/' class='green__button'>See All Stats</a></p>"; }

				?>
			</div>
		<?php

			if(trim($charity__d->twitter) != NULL && $sponsored__q->rowCount() != NULL) {

				print '<div class="boxy">';

				print '<h5>Social:</h5>';

                print '<a class="twitter-timeline" data-widget-id="' . Config::twitterWidgetId() . '" data-screen-name="' . $charity__d->twitter . '" data-tweet-limit="5"></a>';# NOTE: Config Twitter Widget ID

				print '</div>';

			};

			?>
		</div>
		</div></div>
	</div>
</div>

<?php

# List members

function list__members($cid) {

	global $DB;

    $join__s = "SELECT * FROM charity_user WHERE charity_id = ? LIMIT 10";
    $join__q = $DB->prepare($join__s);
    $join__q->execute([$cid]);

    for($n = 0; $n < $join__q->rowCount(); $n++) {
        $join__d = 	$join__q->fetchObject();
        $members__s = "SELECT * FROM users WHERE id = ? LIMIT 1";
        $members__q = $DB->prepare($members__s);
        $members__q->execute(array($join__d->user_id));
        $members__d = $members__q->fetchObject();
        echo '<div class="friends-pics"><a href="member/' . $members__d->id . '/"><img src="';
        if (!empty($members__d->image_url)) {
            echo $members__d->image_url;
        } else {
            echo 'images/default-avatar.jpg';
        }
        echo '">' . $members__d->first_name . ' ' . $members__d->last_name . '</a></div>';
    }

}

function charity_raised($cid) {

	global $DB;

	$amount__s = "SELECT sum(amount) AS thetotal FROM donations WHERE cv = ?";
	$amount__q = $DB->prepare($amount__s);
	$amount__q->execute(array($cid));
	$amount__d = $amount__q->fetchObject();

	if($amount__d->thetotal > 0) {
		return "<p>&pound;$amount__d->thetotal</p>";
	} else {
		return "<p>No money tracked so far.</p>";
	}

}

function following($cid, $aclass) {

	global $DB;

	$join__s = "SELECT * FROM charity_user WHERE user_id = ? AND charity_id = ? LIMIT 1";
	$join__q = $DB->prepare($join__s);
	$join__q->execute(array($_SESSION['user_id'], $cid));

    #echo $_SESSION['user_id'];
    
	if($join__q->rowCount() != NULL) {
		print "<a href='process.php?a=unfollow-charity.php&cid=$cid' class='$aclass'>Un-Follow Charity</a>";
	} else {
		print "<a href='process.php?a=follow-charity.php&cid=$cid' class='$aclass'>Follow Charity</a>";
	}
}
