<!--<div id="crumbs"><div class="inner"><p><strong><a href="pages/home/">Home</a></strong> / Browse Charities</p></div></div>-->


<div id="content">
	<div class="inner">
		<div class="left wide">
			
			
			<h3>Browse Charities</h3>
			
			<p>We have a growing list charities, look for your charity using the search filter below.</p>
			
			<p><strong>If a charity is not listed, you can submit the charity to us <a href="pages/charities/not-listed/">here</a></strong></p>
			
			<p><input type="text" class="text-input" id="filter" value="" placeholder="Type here to filter..." /></p>
			
			<p class="alpha">
				<a href="#">A</a> <a href="#">B</a> <a href="#">C</a> <a href="#">D</a> <a href="#">E</a> 
				<a href="#">F</a> <a href="#">G</a> <a href="#">H</a> <a href="#">I</a> <a href="#">J</a> 
				<a href="#">K</a> <a href="#">L</a> <a href="#">M</a> <a href="#">N</a> <a href="#">O</a> 
				<a href="#">P</a> <a href="#">Q</a> <a href="#">R</a> <a href="#">S</a> <a href="#">T</a> 
				<a href="#">U</a> <a href="#">V</a> <a href="#">W</a> <a href="#">X</a> <a href="#">Y</a> <a href="#">Z</a>
			</p>
			
			<hr />
			
			<?php
				if(isset($_SESSION['user_id'])) {
					$S = "SELECT * FROM charities ORDER BY name ASC";
					$Q = $DB->query($S);
					
					
					for($cc = 0; $cc < $Q->rowCount(); $cc++) {
						
						$D = $Q->fetchObject();
						if($D->image_name != NULL) {
							if(file_exists("uploads/charity-logos/$D->image_name")) {
								print "<div class='charity--logos'><a href='charity/$D->id/'><img src='uploads/charity-logos/$D->image_name' /></a>$D->name</div>";
							} else {
								print "<div class='charity--logos'><a href='charity/$D->id/'><img src='sketch/logo--coming--soon.png' /></a>$D->name</div>";
							}
						} else {
							print "<div class='charity--logos'><a href='charity/$D->id/'><img src='sketch/logo--coming--soon.png' /></a>$D->name</div>";
						}
					}
				
				// print "";
				
				} else {

					print '<p>Sorry, but you need to be signed in to access this feature. Please <a href="pages/admin/register/" class="green__button">Register</a> or <a href="pages/admin/login/" class="green__button">Login</a></p>';

				}
			
				
			?>
				
	

		</div>
	</div>
</div>


<script>
	
	$(function() {
	
		$("#filter").keyup(function(){
	        var filter = $(this).val(), count = 0;
	        $("div.charity--logos").each(function(){
	            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
	                $(this).fadeOut(100);
	            } else {
	                $(this).show(100);
	                count++;
	            }
	        });
	    });
	    
	    
	    $('p.alpha a').click(function() {
	    	var letter = $(this).text();
	    	//alert(letter);
			$("div.charity--logos").each(function(){
				if ($(this).text()[0] == letter) {
					$(this).show();
				} else {
					$(this).hide();
				}
			});
			return false;
	    });	
	    
	    
	});

</script>
