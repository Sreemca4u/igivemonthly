<div id="content">
	<div class="inner">
		<div class="left wide">
<?php

if(isset($_SESSION['user_id'])) {

?>
<script async src="//cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
<script>
	$(function() {
		$('#theurlid').on('paste', function () {
			var element = this;
			setTimeout(function () {
				var text = $(element).val();
				the_url = $('#theurlid').val();
				// alert(the_url);
				$('#thecardinside').append("<a class='embedly-card' href='"+ the_url +"'></a>");
			}, 100);
		});
		
		$('#show__cards').click(function() {
			$('.hide__card').slideToggle();
			($('#show__cards').text() === "More News Articles") ? $('#show__cards').text("Less News Articles") : $('#show__cards').text("More News Articles");
			return false;
		});
	});
</script>

<!--<div id="crumbs"><div class="inner"><p><strong><a href="pages/home/">Home</a></strong> / Latest News</p></div></div>-->

			<?php print__page(489); ?>
			<div class="clearfix mosaicflow">
			<?php
				$news__s = "SELECT * FROM news ORDER BY created_at ASC LIMIT 100";
				$news__q = $DB->query($news__s);
				
				if($news__q->rowCount() > 0) {
					
					$items = $news__q->rowCount();
					
					for($ne = 0; $ne < $items; $ne++) {
					
						$news__d = $news__q->fetchObject();
						
						$over = '';
						
						if($ne > 9) { $over = 'hide__card'; } else { $over = ''; }
						
						print "<div class='news_cards $over mosaicflow__item'>";
						print "<a class='embedly-card' href='$news__d->url'></a><a href='/charity/$news__d->charity_id/' class='green__button'>Charity page</a>";
						print "</div>";
					}
				}
				
				# If more than 10, then display button to see more 
				
				if($news__q->rowCount() > 9) { print '<br /><br /><p><a href="#" id="show__cards" class="green__button">More News Articles</a></p>'; }
			?>
			</div>

<?php

} else {
    echo '<h3>Latest News</h3><p>Latest news updates from our sponsored partner charities.</p>';
    print '<p>Sorry, but you need to be signed in to access this feature. Please <a href="pages/admin/register/" class="green__button">Register</a> or <a href="pages/admin/login/" class="green__button">Login</a></p>';
}
?>
		</div>
	</div>
</div>