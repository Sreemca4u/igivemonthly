<!--<div id="crumbs"><div class="inner"><p><strong><a href="pages/home/">Home</a></strong> / Features</p></div></div>-->


<div id="content">
	<div class="inner">
		<div class="left50">

			<h2>Top 10 Charities</h2>

			<p>Here are the current top 10 charities.</p>

			<?php


			$charities__s = "SELECT bk, sum(ap) AS total FROM live__node__data WHERE aim = 23 GROUP BY bk ORDER BY total DESC";
			$charities__q = $DB->query($charities__s);


			for($c = 0; $c < $charities__q->rowCount(); $c++) {


				$charities__d = $charities__q->fetchObject();

# Charity

				$charity__s = "SELECT * FROM live__node__data WHERE aim = '14' AND id = ? ORDER BY av ASC";
				$charity__q = $DB->prepare($charity__s);
				$charity__q->execute(array($charities__d->bk));
				$charity__d = $charity__q->fetchObject();

				print "<p>$charity__d->av - &pound;$charities__d->total</p>";


			}


			?>

		</div><div class="right50">

			<h2>Top 10 Members</h2>

			<p>Here are the current top 10 members.</p>


			<?php


			$members__s = "SELECT ak, sum(ap) AS total FROM live__node__data WHERE aim = 23 GROUP BY ak ORDER BY total DESC";
			$members__q = $DB->query($members__s);


			for($m = 0; $m < $members__q->rowCount(); $m++) {


				$members__d = $members__q->fetchObject();

				# Member

				$member__s = "SELECT * FROM users WHERE id = ? ORDER BY av ASC";
				$member__q = $DB->prepare($member__s);
				$member__q->execute(array($members__d->ak));
				$member__d = $member__q->fetchObject();


				print "<p>$member__d->first_name $member__d->last_name - &pound;$members__d->total</p>";


			}


		?>


		</div>
	</div>
</div>
