<!--<div id="crumbs"><div class="inner"><p><strong><a href="pages/home/">Home</a></strong> / Contact</p></div></div>-->


<div id="content">
	<div class="inner">
		<div class="left">
			<?php
            
            print__page(497);
    
            if (!isset($_SESSION['mailName'])) {
                $_SESSION['mailName'] = '';
            }
            if (!isset($_SESSION['mailEmail'])) {
                $_SESSION['mailEmail'] = '';
            }
            if (!isset($_SESSION['mailTelephone'])) {
                $_SESSION['mailTelephone'] = '';
            }
            if (!isset($_SESSION['mailQuery'])) {
                $_SESSION['mailQuery'] = '';
            }
            
            echo '<form action="pages/content/post.php" method="post"><fieldset>';
            if (isset($_SESSION['mailSuccesss'])) {
                if ($_SESSION['mailSuccesss']) {
                    echo '<div class="alert alert-success">Your query has been sent successfully</div>';
                    $_SESSION['mailName'] = '';
                    $_SESSION['mailEmail'] = '';
                    $_SESSION['mailTelephone'] = '';
                    $_SESSION['mailQuery'] = '';
                } else {
                    echo '<div class="alert alert-danger">Applogies, your query has not been sent successfully: ' . $_SESSION['mailMessage'] . '</div>';
                }
                unset($_SESSION['mailSuccesss']);
            }
            echo '<dl><dt>Name:</dt><dd><input type="text" name="mailName" value="' . htmlspecialchars_decode($_SESSION['mailName'], ENT_NOQUOTES) . '" required="required" /></dd></dl>';
            echo '<dl><dt>Email:</dt><dd><input type="email" name="mailEmail" value="' . htmlspecialchars_decode($_SESSION['mailEmail'], ENT_NOQUOTES) . '" required="required" /></dd></dl>';
            echo '<dl><dt>Telephone:</dt><dd><input type="text" name="mailTelephone" value="' . htmlspecialchars_decode($_SESSION['mailTelephone'], ENT_NOQUOTES) . '" /></dd></dl>';
            echo '<dl><dt>Query:</dt><dd><textarea name="mailQuery" rows="10" required="required">' . htmlspecialchars_decode($_SESSION['mailQuery'], ENT_NOQUOTES) . '</textarea></dd></dl>';
            echo '</fieldset><p><input type="submit" value="Submit" /></p></form>';
            
            ?>

		</div><div class="right">

			<h3>Map</h3>

			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4964.438287052397!2d-0.09003749999999999!3d51.527540299999984!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761ca66f39e1b9%3A0xd64c7b36fddd6ec8!2s!5e0!3m2!1sen!2suk!4v1443560077112" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>


			<h4>Address</h4>

			<p>Kemp House<br />
			152 - 160 City Road<br />
			London<br />
			EC1V 2NX<br />
			UK
			</p>

		</div>
	</div>
</div>
