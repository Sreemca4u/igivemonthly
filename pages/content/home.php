<script async src="//cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
<script>
	$(function() {
		$('#theurlid').on('paste', function () {
			var element = this;
			setTimeout(function () {
				var text = $(element).val();
				the_url = $('#theurlid').val();
				// alert(the_url);
				$('#thecardinside').append("<a class='embedly-card' href='"+ the_url +"'></a>");
			}, 100);
		});

		$('#show__cards').click(function() {

			$('.hide__card').slideToggle();
			($('#show__cards').text() === "More News Articles") ? $('#show__cards').text("Less News Articles") : $('#show__cards').text("More News Articles");
			return false;

		});

	});
</script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

<div id="splash">
	<div id="message">
		<h1>Welcome to iGiveMonthly</h1>
		<h2>Keeping track of your donations</h2>
		<?php
		if(isset($_SESSION['user_id'])) {
			print '<p><a href="pages/members/add-donation/">Add Donation</a><a href="pages/charities/browse/">Sponsor A Charity</a></p>';
        } else {
			print '<p><a href="pages/admin/login/">Get Started</a></p>';
		};
		?>
	</div>
	<ul class="slides">
		<li><img src="sketch/slideshow/shutterstock_213019441.jpg" alt="shutterstock_213019441" /></li>
		<li><img src="sketch/slideshow/shutterstock_212062051.jpg" alt="shutterstock_212062051" /></li>
		<li><img src="sketch/slideshow/shutterstock_196175651.jpg" alt="shutterstock_196175651" /></li>
		<li><img src="sketch/slideshow/shutterstock_226456312.jpg" alt="shutterstock_226456312" /></li>
		<li><img src="sketch/slideshow/shutterstock_146777567.jpg" alt="shutterstock_146777567" /></li>
	</ul>
</div>
<div id="adverts">
	<div class="inner">
		<h3>A Selection Of Our Partner Charities</h3>
			<?php
				$sponsored__s = "SELECT * FROM sponsored_charities ORDER BY RAND() LIMIT 4";# NOTE 4 Random Sponsored Charities
				$sponsored__q = $DB->prepare($sponsored__s);
                $sponsored__q->execute();

				if($sponsored__q->rowCount() > 0) {
					for($ss = 0; $ss < $sponsored__q->rowCount(); $ss++) {
						$sponsored__d = $sponsored__q->fetchObject();
                        $sponsored__d->image_name = 'uploads/charity-logos/' . $sponsored__d->image_name;
                        if (!is_file($sponsored__d->image_name)) {
                            $sponsored__d->image_name = 'sketch/logo--coming--soon.png';
                        }
                        if(isset($_SESSION['user_id'])) {
                            echo '<div class="advert">';
                            print "<a href='charity/$sponsored__d->id/'><img src='$sponsored__d->image_name' /></a>";
                            # NOTE: If logged in, show twitter feed underneath
                            if (trim($sponsored__d->twitter) != NULL) {
                                print '<a class="twitter-timeline" data-widget-id="' . Config::twitterWidgetId() . '" data-screen-name="' . $sponsored__d->twitter . '" data-tweet-limit="5"></a>';# NOTE: Config Twitter Widget ID
                            }
                            echo '</div>';
                        } else {
                            print "<a href='charity/browse/'><img src='$sponsored__d->image_name' /></a>";
                        }
					}
				} else {
					print "<p>No charities sponsored.</p>";
				};
			?>
	</div>
</div>
<div id="content">
	<div class="inner">
		<div class="left">
			<?php

            # NOTE: Left Content
            
			if(isset($_SESSION['user_id'])) {
                
                # NOTE: Latest 5 news if logged in
                
                $news__s = "SELECT * FROM news ORDER BY created_at desc LIMIT 5";
				$news__q = $DB->query($news__s);
				if($news__q->rowCount() > 0) {
					$items = $news__q->rowCount();
					for($ne = 0; $ne < $items; $ne++) {
						$news__d = $news__q->fetchObject();
						$over = '';
						if($ne > 9) { $over = 'hide__card'; } else { $over = ''; }
						print "<div><a class='embedly-card' href='$news__d->url'></a>";
						print "<a href='pages/news/main/' class='green__button'>More News</a>";
						print "<a href='charity/$news__d->charity_id/' class='green__button'>Charity page</a></div>";
					}
				}

			} else {

				print__page(486);

			}

			?>
		</div><div class="right slidenews">
<?php
        # NOTE: Right Content
        
            # NOTE: If not logged in, then display screenshot

			if(!isset($_SESSION['user_id'])) {
				print "<img src='sketch/front/screen__shot.png' />";
            }
            # NOTE: Latest 4 donations
            print '<div class="latest__donations"><h3>Latest Donations</h3>';
                $top__s = "SELECT * FROM charity_donations ORDER BY when_donated DESC LIMIT 4";
                $top__q = $DB->query($top__s);
                for($hh = 0; $hh < $top__q->rowCount(); $hh++) {
                    $top__d = $top__q->fetchObject();
                    if(isset($_SESSION['user_id'])) {
                        $img = '<a href="charity/' . $top__d->id . '/">';
                    } else {
                        $img = '<a href="charity/browse/">';
                    }
                    $top__d->image_name = 'uploads/charity-logos/' . $top__d->image_name;
                    if(is_file($top__d->image_name)) {
                        $img .= "<img src='$top__d->image_name' /></a>";
                    } else {
                        $img .= "<img src='sketch/logo--coming--soon.png' /></a>";
                    }
                    print "<dl class='front__favs'><dd><strong>&pound;$top__d->amount</strong></dd><dd>$img</dd></dl>";
                }
            print '</div>';
			?>
		</div>
	</div>
</div>
