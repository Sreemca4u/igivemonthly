<?php

if (session_id() == null) {
    session_start();
}

require_once '../../includes/config.php';
require_once '../../config/config.php';

require_once '../../vendor/PHPMailer/PHPMailerAutoload.php';

$_SESSION['mailSuccesss'] = false;
$_SESSION['mailMessage'] = 'Unknown error';

if (isset($_POST['mailName']) && isset($_POST['mailEmail']) && isset($_POST['mailTelephone']) && isset($_POST['mailQuery'])) {
    $_SESSION['mailName'] = strip_tags(htmlspecialchars($_POST['mailName'], ENT_QUOTES));
    $_SESSION['mailEmail'] = strip_tags(htmlspecialchars($_POST['mailEmail'], ENT_QUOTES));
    $_SESSION['mailTelephone'] = strip_tags(htmlspecialchars($_POST['mailTelephone'], ENT_QUOTES), '<br />');
    $_SESSION['mailQuery'] = nl2br(strip_tags(htmlspecialchars($_POST['mailQuery'], ENT_QUOTES)));
    
    $mail = new PHPMailer;
    
    $mail->IsSMTP();
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'tls';
    $mail->Host = Config::contactUsEmailHost();
    $mail->Port = 587;
    $mail->Username = Config::contactUsEmailFrom();
    $mail->Password = Config::contactUsEmailFromPassword();

    $mail->setFrom(Config::contactUsEmailFrom(), 'iGiveMonthly');
    $mail->addAddress(Config::contactUsEmailTo());

    $mail->isHTML(true);

    $mail->Subject = 'iGiveMonthly Contact Us Form';
    $html = '<table>';
    $html .= '<tr><th style="text-align:left;">Name</th><td>' . $_SESSION['mailName'] . '</td><tr>';
    $html .= '<tr><th style="text-align:left;">Email</th><td>' . $_SESSION['mailEmail'] . '</td><tr>';
    $html .= '<tr><th style="text-align:left;">Telephone</th><td>' . $_SESSION['mailTelephone'] . '</td><tr>';
    $html .= '<tr><th style="text-align:left;vertical-align:top;">Query</th><td>' . $_SESSION['mailQuery'] . '</td><tr>';
    $html .= '</table>';
    $mail->msgHTML($html);

    if(!$mail->send()) {
        $_SESSION['mailMessage'] = $mail->ErrorInfo;
    } else {
        $_SESSION['mailSuccesss'] = true;
    }
    
} else {
    $_SESSION['mailMessage'] = 'No form post data';
}

header('location: contact/');
exit;