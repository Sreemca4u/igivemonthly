<div id="crumbs">
	<div class="inner">
		<a href="#" class="activate_crumbs_dropdown">Admin menu</a>
		<p>
			<strong><a href="pages/home/">Home</a> / <a href="member/<?php print $_SESSION['user_id']; ?>/">Profile</a></strong>
			<a href="pages/members/add-donation/" class="green__button fright">Add Donation</a>
			<a href="pages/charities/browse/" class="green__button fright">Sponsor A Charity</a>
			<a href="pages/charities/browse/" class='green__button fright'>Follow A Charity</a>
			<a href="pages/members/update-member/" class="green__button fright">Update Profile</a>
			<a href="pages/members/link/" class="green__button fright">Link Accounts</a>
			<a href="pages/members/forwarded-emails/" class="green__button fright">Forwarded Emails<?php echo num_forwarded_emails(); ?></a>
			<?php /*<a href="#" class="green__button fright notification">Notifications <?php notice__num(); notifications(); ?></ul></a>*/ ?>
		</p>
	</div>
</div>
