<!--<div id="crumbs"><div class="inner"><p><strong><a href="pages/home/">Home</a></strong> / Browse</p></div></div>-->


<div id="content">
	<div class="inner">
		<div class="left wide">
			
			
			<h3>Members not found</h3>
			
			<p>I'm sorry, member not found, please try again.</p>


		</div>
	</div>
</div>


<script>
	
	$(function() {
	
		$("#filter").keyup(function(){
	        var filter = $(this).val(), count = 0;
	        $("div.members--pics").each(function(){
	            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
	                $(this).fadeOut(100);
	            } else {
	                $(this).show(100);
	                count++;
	            }
	        });
	    });
	    
	    
	    $('p.alpha a').click(function() {
	    	var letter = $(this).text();
	    	//alert(letter);
			$("div.members--pics").each(function(){
				if ($(this).text()[0] == letter) {
					$(this).show();
				} else {
					$(this).hide();
				}
			});
			return false;
	    });	
	    
	    
	});

</script>