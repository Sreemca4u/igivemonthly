<?php



# Get the number of notifications...


function num_forwarded_emails() {
    global $DB;

    $result = '';
    $scan__s = "SELECT notify_emails FROM users WHERE id = ? limit 1";
    $scan__q = $DB->prepare($scan__s);
    $scan__q->execute([$_SESSION['user_id']]);
    if ($scan__q->rowCount() > 0) {
        $scan__d = $scan__q->fetchObject();
        if ($scan__d->notify_emails > 0) {
            $result = ' <span style="color:#ff8a80;">(' . $scan__d->notify_emails . ')</span>';
        }
    }
    return $result;
}

	function notice__num() {


		global $DB;

		$scan__s = "SELECT * FROM live__node__data WHERE bv = ? AND aim = 32";
		$scan__q = $DB->prepare($scan__s);
		$scan__q->execute([$_SESSION['user_id']]);
		$scan__n = $scan__q->rowCount();

		if($scan__n > 0) { print $scan__n; };
	}

	function notifications() {


		global $DB;


		$notify__s = "SELECT id, ak, bk FROM live__node__data WHERE aim = 31 AND bk = ? AND ai = 1";
		$notify__q = $DB->prepare($notify__s);
		$notify__q->execute(array($_SESSION['user_id']));
		$notify__n = $notify__q->rowCount();


		$scan__s = "SELECT * FROM live__node__data WHERE bv in (?) AND aim = 32";
		$scan__q = $DB->prepare($scan__s);
		$scan__q->execute([$_SESSION['user_id']]);
		$scan__n = $scan__q->rowCount();

		//print "<h1>$scan__s</h1>";


		if($notify__n > 0 xor $scan__n > 0) {
			print '<ul class="notifications">';
			for($u = 0; $u < $notify__n; $u++) {
				$notify__d = $notify__q->fetchObject();
				$charity__s = "SELECT * FROM live__node__data WHERE aim = 14 AND id = ? LIMIT 1";
				$charity__q = $DB->prepare($charity__s);
				$charity__q->execute(array($notify__d->ak));
				$charity__d = $charity__q->fetchObject();
				print "<li><a href='charity/$charity__d->id/'><strong>$charity__d->av</strong> posted a news article</a></li>";
			}
			if($scan__n > 0) { print "<li><a href='pages/members/forwarded-emails/'><strong>$scan__n Forwarded emails detected</strong></a></li>"; }
			print "</ul>";
		}
	}

function fetchUser($user_id) {
	global $DB;
	$stmt = $DB->prepare('select * from users where id = ?');
	$stmt->execute([$user_id]);
	return $stmt->fetchObject();
}