<?php include('include__member__functions.php'); if($_SESSION['mine'] == true) { include('include__member__nav.php'); }; ?>

<script>

	$(function() {

		$('a.notification').click(function() {

			$('ul.notifications').slideToggle();
			event.preventDefault();
			return false;

		});

		$('body').addClass('crumbs');

	});


</script>



<div id="content">
	<div class="inner">
		<div class="left wide">




			<h3>Stats</h3>

			<br />

<!--			<ul class='tabs'>-->
<!--				<li><a href='#tab1'>Main Stats</a></li>-->
<!--				<li><a href='#tab2'>By Charity</a></li>-->
<!--				<li><a href='#tab3'>By Type</a></li>-->
<!--			</ul>-->

<!--			<br />-->

			<div id="tab1">


			<?php


			$uid = $_SESSION['user_id'];




			# Range, will need to be clever


			$filter = isset($_REQUEST['filter'])?$_REQUEST['filter']:'';
// <= ad <= CURRENT_DATE()
			if($filter == 'month') {
				$S = "SELECT * FROM donations inner join charities on donations.charity_id=charities.id WHERE donations.when_donated BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW() and donations.user_id = ? ORDER BY donations.when_donated DESC";
				$params = [$uid];
				$header__text = '<h3>Last month</h3>';
				// BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW()
			} elseif($filter == 'year') {
				$S = "SELECT * FROM donations inner join charities on donations.charity_id=charities.id WHERE donations.when_donated BETWEEN DATE_SUB(NOW(), INTERVAL 365 DAY) AND NOW() AND donations.user_id = ? ORDER BY donations.when_donated DESC";
				$params = [$uid];
				$header__text = '<h3>Last 12 months</h3>';
			} elseif($filter == 'quarter') {
				$S = "SELECT * FROM donations inner join charities on donations.charity_id=charities.id WHERE donations.when_donated BETWEEN DATE_SUB(NOW(), INTERVAL 90 DAY) AND NOW() AND donations.user_id = ? ORDER BY donations.when_donated DESC";
				$params = [$uid];
				$header__text = '<h3>Last 3 months</h3>';
			} elseif($filter == 'custom') {
				$explode__from = explode("/", $_POST['from']);
				$explode__to = explode("/", $_POST['to']);
				$makedate__from = date('Y-m-d', mktime(0, 0, 0, $explode__from[1], $explode__from[0], $explode__from[2]));
				$makedate__to = date('Y-m-d', mktime(0, 0, 0, $explode__to[1], $explode__to[0], $explode__to[2]));

				$S = "SELECT * FROM donations inner join charities on donations.charity_id=charities.id WHERE donations.when_donated BETWEEN ? AND ? AND donations.user_id = ? ORDER BY donations.when_donated DESC";
				$params = [$makedate__from, $makedate__to, $uid];
				$header__text = "<h3>From: $_POST[from] To: $_POST[to]</h3>";
			} else {
				$S = "SELECT * FROM donations inner join charities on donations.charity_id=charities.id WHERE donations.when_donated <= CURRENT_DATE() AND donations.user_id = ? ORDER BY donations.when_donated DESC";
				$params = [$uid];
				$header__text = '<h3>All</h3>';
			}

			// print "<p>--- $S ---</p>";


			$Q = $DB->prepare($S);
			$Q->execute($params);




			print '<ul class="filter">';
			print "<li><a href='stats/$uid/month/'>Last 30 days</a></li>";
			print "<li><a href='stats/$uid/quarter/'>Last 90 days</a></li>";
			print "<li><a href='stats/$uid/year/'>Yearly</a></li>";
			print "<li><a href='stats/$uid/all/'>All</a></li>";
			print "<li><form action='stats/$uid/custom/' method='post' class='filter'>Custom filter: ";
			print "<input type='text' name='from' placeholder='From' class='datetimepicker' value='".(isset($_POST['from'])?$_POST['from']:'')."' />";
			print "<input type='text' name='to' placeholder='To' class='datetimepicker' value='".(isset($_POST['to'])?$_POST['to']:'')."' />";
			print '<input type="submit" value="Filter Range" style="position: absolute;"/>';
			print "</form></li>";
			print '</ul>';


			print $header__text;

			if($Q->rowCount() != NULL) {
				for($t = 0; $t < $Q->rowCount(); $t++) {

					$D = $Q->fetchObject();
					$D->ad = date('d F Y', strtotime($D->when_donated));
                    #var_dump($D);
                    #exit;


					print '<dl class="strip stats">';
					print "<dd>$D->ad</dd>";
					print "<dd><strong>$D->name</strong></dd>";
					print "<dd>".CURRENCY_SYMBOL()."$D->amount</dd>";
					#print "<dd class='wide'><em>$D->email</em></dd>";
//					print "<dd class='edit'><a href='process.php?a=delete__donation.php&did=$D->id' class='confirm'><img src='sketch/delete.png' style='width: 16px;' /></a></dd>";
//					print "<dd class='edit'><a href='pages/members/edit-donation/$D->id/'><img src='sketch/pencil_go.png' style='width: 16px;' /></a></dd>";
					print '</dl>';


				}


			} else {

				print "<p>No donations found.</p>";

			}


			print '</div>';
			print '<div id="tab2">';

//
//#	For each charity you have donated too
//
//			print "<h2>Each Charity Summary</h2>";
//
//			$donated__to__charity__s = "SELECT bk, sum(ap) AS total FROM live__node__data WHERE aim = 23 AND ak = ? GROUP BY bk";
//			$donated__to__charity__q = $DB->prepare($donated__to__charity__s);
//			$donated__to__charity__q->execute(array($_SESSION['user_id']));
//
//
//			// print "<p>$donated__to__charity__s</p>";
//
//
//			for($k = 0; $k < $donated__to__charity__q->rowCount(); $k++) {
//
//				$donated__to__charity__d = $donated__to__charity__q->fetchObject();
//
//
//				# Charity
//
//				$charity__s = "SELECT * FROM live__node__data WHERE aim = '14' AND id = ? ORDER BY av ASC";
//				$charity__q = $DB->prepare($charity__s);
//				$charity__q->execute(array($donated__to__charity__d->bk));
//				$charity__d = $charity__q->fetchObject();
//
//
//				if($_SESSION['country'] == 806) { setlocale(LC_MONETARY, 'en_GB'); };
//				if($_SESSION['country'] == 807) { setlocale(LC_MONETARY, 'en_US'); };
//				if($_SESSION['country'] == 808) { setlocale(LC_MONETARY, 'en_US'); };
//
////				$donated__to__charity__d->total = utf8_encode(money_format('%n', $donated__to__charity__d->total));
//
//
//				if($charity__d->av != NULL) {
//
//					print "<dl class='strip nudge'><dd><strong>$charity__d->av</strong> </dd><dd> $donated__to__charity__d->total</dd></dl>";
//
//				}
//
//
//
//
//			}
//
//
//			print '</div>';
//			print '<div id="tab3">';
//
//
//			#	For each charity you have donated too
//
//						print "<h2>Each Activity Summary</h2>";
//
//						$donated__to__charity__s = "SELECT ck, sum(ap) AS total FROM live__node__data WHERE aim = 23 AND ak = ? GROUP BY ck";
//						$donated__to__charity__q = $DB->prepare($donated__to__charity__s);
//						$donated__to__charity__q->execute(array($_SESSION['user_id']));
//
//
//						// print "<p>$donated__to__charity__s</p>";
//
//
//						for($k = 0; $k < $donated__to__charity__q->rowCount(); $k++) {
//
//							$donated__to__charity__d = $donated__to__charity__q->fetchObject();
//
//
//							if($_SESSION['country'] == 806) { setlocale(LC_MONETARY, 'en_GB'); };
//							if($_SESSION['country'] == 807) { setlocale(LC_MONETARY, 'en_US'); };
//							if($_SESSION['country'] == 808) { setlocale(LC_MONETARY, 'en_US'); };
//
////							$donated__to__charity__d->total = utf8_encode(money_format('%n', $donated__to__charity__d->total));
//
//
//							# Activity
//
//							$charity__s = "SELECT * FROM live__node__data WHERE aim = '15' AND id = ? ORDER BY av ASC";
//							$charity__q = $DB->prepare($charity__s);
//							$charity__q->execute(array($donated__to__charity__d->ck));
//							$charity__d = $charity__q->fetchObject();
//
//							if($charity__d->av != NULL) {
//
//								print "<dl class='strip nudge'><dd><strong>$charity__d->av</strong> </dd><dd> $donated__to__charity__d->total</dd></dl>";
//
//							}
//
//
//
//
//						}



			print '</div>';




			function isFav($cid) {
				global $DB;
				$join__s = "SELECT * FROM live__node__data WHERE aim = '20' AND bk = ? AND ak = ? LIMIT 1";
				$join__q = $DB->prepare($join__s);
				$join__q->execute(array($cid, $_SESSION['user_id']));
				if($join__q->rowCount() != NULL) {
					print '<dd><img src="sketch/star.png" alt="star" width="23" height="20" class="auto" /></dd>';
				};
			}


			?>







		</div>
	</div>
</div>


<script>

$(function() {
$('ul.tabs').each(function(){
    // For each set of tabs, we want to keep track of
    // which tab is active and it's associated content
    var $active, $content, $links = $(this).find('a');

    // If the location.hash matches one of the links, use that as the active tab.
    // If no match is found, use the first link as the initial active tab.
    $active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
    $active.addClass('active');

    $content = $($active[0].hash);

    // Hide the remaining content
    $links.not($active).each(function () {
      $(this.hash).hide();
    });

    // Bind the click event handler
    $(this).on('click', 'a', function(e){
      // Make the old tab inactive.
      $active.removeClass('active');
      $content.hide();

      // Update the variables with the new link and content
      $active = $(this);
      $content = $(this.hash);

      // Make the tab active.
      $active.addClass('active');
      $content.show();

      // Prevent the anchor's default click action
      e.preventDefault();
    });
  });
});

</script>
