<!--<div id="crumbs"><div class="inner"><p><strong><a href="pages/home/">Home</a></strong> / Browse</p></div></div>-->
<div id="content">
	<div class="inner">
		<div class="left wide">
			<h3>Browse Members</h3>
			<p>More and more members growing daily, lookup members using the search filter below.</p>
			<?php
			
				if(isset($_SESSION['user_id'])) { 
				
				print '<p><input type="text" class="text-input" id="filter" value="" placeholder="Type here to filter..." /></p>';
				
				print '<p class="alpha">';
				print '<a href="#">A</a> <a href="#">B</a> <a href="#">C</a> <a href="#">D</a> <a href="#">E</a> ';
				print '<a href="#">F</a> <a href="#">G</a> <a href="#">H</a> <a href="#">I</a> <a href="#">J</a> ';
				print '<a href="#">K</a> <a href="#">L</a> <a href="#">M</a> <a href="#">N</a> <a href="#">O</a> ';
				print '<a href="#">P</a> <a href="#">Q</a> <a href="#">R</a> <a href="#">S</a> <a href="#">T</a> ';
				print '<a href="#">U</a> <a href="#">V</a> <a href="#">W</a> <a href="#">X</a> <a href="#">Y</a> <a href="#">Z</a>';
				print '</p>';
				print '<hr />';

					$S = "SELECT * FROM users ORDER BY first_name, last_name ASC";
					$Q = $DB->query($S);
					
					for($cc = 0; $cc < $Q->rowCount(); $cc++) {
						
						$D = $Q->fetchObject();
						print "<div class='members--pics'><a href='member/$D->id/'>";
						if (strlen($D->image_url)) {
							print "<img src='$D->image_url' />";
						} else {
                            print "<img src='images/default-avatar.jpg' />";
                        }
						print "$D->first_name $D->last_name</div>";
						
					}

				} else {
					
					print '<p>Sorry, but you need to be signed in to access this feature. Please <a href="pages/admin/register/" class="green__button">Register</a> or <a href="pages/admin/login/" class="green__button">Login</a></p>';				
				}

			?>
		</div>
	</div>
</div>
<script>
	$(function() {
		$("#filter").keyup(function(){
	        var filter = $(this).val(), count = 0;
	        $("div.members--pics").each(function(){
	            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
	                $(this).fadeOut(100);
	            } else {
	                $(this).show(100);
	                count++;
	            }
	        });
	    });
	    $('p.alpha a').click(function() {
	    	var letter = $(this).text();
	    	//alert(letter);
			$("div.members--pics").each(function(){
				if ($(this).text()[0] == letter) {
					$(this).show();
				} else {
					$(this).hide();
				}
			});
			return false;
	    });	
	});
</script>