<?php include('include__member__functions.php'); if($_SESSION['mine'] == true) { include('include__member__nav.php'); }; ?>


<div id="content">
	<div class="inner">
		<div class="left">


			<h3>Edit donation</h3>


			<?php


			$did = $_REQUEST['d'];


			$donation__s = "SELECT * FROM live__node__data WHERE aim = 23 AND id = ?";
			$donation__q = $DB->prepare($donation__s);
			$donation__q->execute(array($did));
			$donation__d = $donation__q->fetchObject();


			$charity__s = "SELECT av FROM live__node__data WHERE aim = '14' AND id = ? LIMIT 1";
			$charity__q = $DB->prepare($charity__s);
			$charity__q->execute(array($donation__d->bk));
			$charity__d = $charity__q->fetchObject();


			//print "<p>@$donation__s@</p>";


			?>


			<form action="process.php" method="get">
				<fieldset class="side_left">
					<dl>
						<dt>Select Main Charity</dt>
						<dd>
							<select name="charity" class="turn-to-ac">
								<option></option>
								<?php
									$S = "SELECT * FROM live__node__data WHERE aim = '14' ORDER BY av ASC";
									$Q = $DB->query($S);
									if($Q->rowCount() != NULL) {
										for($t = 0; $t < $Q->rowCount(); $t++) {
											$D = $Q->fetchObject();
											if($D->id == $donation__d->bk) { print "<option value='$D->id' selected='selected'>$D->av</option>"; } else { print "<option value='$D->id'>$D->av</option>"; }
										}
									}
								?>
							</select>
							<!-- <input type="text" id="default" list="charaties" name="charity" placeholder="e.g. Age UK" value="<?php print $charity__passed__direct__d->av; ?>"> -->
						</dd>
					</dl>
					<dl>
						<dt>Select/Type Activity</dt>
						<dd>
							<select name="fundraising__method">
								<option value="0A">NA</option>
								<?php
								$fundraising__method__s = "SELECT id, av FROM live__node__data WHERE aim = 15";
								$fundraising__method__q = $DB->query($fundraising__method__s);
								for($cu = 0; $cu < $fundraising__method__q->rowCount(); $cu++) {
									$fundraising__method__d = $fundraising__method__q->fetchObject();
									if($fundraising__method__d->id == $donation__d->ck) { $ischecked = 'selected="selected"'; } else { $ischecked = ''; }
									print "<option value='$fundraising__method__d->id' $ischecked>$fundraising__method__d->av</option>";
								}
								?>
							</select>
						</dd>
					</dl>
					<dl>
						<dt>Donation via</dt>
						<dd>
							<select name="donation__via">
								<option value="0">NA</option>
								<?php
								$via__s = "SELECT id, av FROM live__node__data WHERE aim = 24 ORDER BY av ASC";
								$via__q = $DB->query($via__s);
								for($v = 0; $v < $via__q->rowCount(); $v++) {
									$via__d = $via__q->fetchObject();
									if($via__d->id == $donation__d->dk) { $ischecked = 'selected="selected"'; } else { $ischecked = ''; }
									print "<option value='$via__d->id' $ischecked>$via__d->av</option>";
								}
								?>
							</select>
						</dd>
					</dl>
					
					<script>
					
						$(function() { $('.datetimepickeredit').datetimepicker({ timepicker: false, format:'Y-m-d', yearStart: '1920' }); });
					
					</script>
					
					<dl><dt>Relevant Date</dt><dd><input type="text" name="relevant__date" value="<?php print date('Y-m-d', strtotime($donation__d->ad)); ?>" class="datetimepickeredit" /></dd></dl>
				</fieldset>
				<fieldset class="side_right">
					<dl>
						<dt>Amount (&pound; or Hours)</dt>
						<dd><input type="text" name="amount" value="<?php print $donation__d->ap; ?>" /></dd>
						<dd style='width: 120px;'>
							<input type="checkbox" name="time__ticker" style='width: 10px;' value="y" <?php if($donation__d->av == 'TIME') { print 'checked="checked"'; }; ?> /><span style='font-size: 0.7em; '> (Time)</span>
						</dd>
					</dl>
					<dl>
						<dt>Frequency</dt>
						<dd>
							<select name="frequency">
								<option value="single" <?php if($donation__d->bv == 'single') { print 'selected="selected"'; }; ?>>Single donation</option>
								<option value="monthly" <?php if($donation__d->bv == 'monthly') { print 'selected="selected"'; }; ?>>Monthly donation</option>
								<option value="annually" <?php if($donation__d->bv == 'annually') { print 'selected="selected"'; }; ?>>Yearly donation</option>
							</select>
						</dd>
					</dl>
					<dl><dt>Information</dt><dd><textarea name="info"><?php print $donation__d->ax; ?></textarea></dd></dl>
					<input type="hidden" name="a" value="edit-donation.php" />
					<input type="hidden" name="did" value="<?php print $did; ?>" />
				</fieldset>
				<p><input type="submit" value="Update Donation" /></p>


			</form>




			<?php
				$S = "SELECT * FROM live__node__data WHERE aim = '14' ORDER BY av ASC";
				$Q = $DB->query($S);
				if($Q->rowCount() != NULL) {
					print '<datalist id="charaties">';
					for($t = 0; $t < $Q->rowCount(); $t++) {
						$D = $Q->fetchObject();
						print '<option value="'. $D->av .'">';
					}
					print '</datalist>';
				}
			?>




		</div>
	</div>
</div>
