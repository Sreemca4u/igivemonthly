<?php include('include__member__functions.php'); if($_SESSION['mine'] == true) { include('include__member__nav.php'); }; ?>


<?php

$d = $_REQUEST['d'];

$charity__passed__direct__s = "SELECT id, av FROM live__node__data WHERE aim = 14 AND id = ?";
$charity__passed__direct__q = $DB->prepare($charity__passed__direct__s);
$charity__passed__direct__q->execute(array($d));
$charity__passed__direct__d = $charity__passed__direct__q->fetchObject();



?>



<div id="content">
	<div class="inner">
		<div class="left wide">


			<h3>Import donation</h3>


			<?php

			$scan__s = "SELECT * FROM emails WHERE id = ?";
			$scan__q = $DB->prepare($scan__s);
			$scan__q->execute(array($d));
			$scan__d = $scan__q->fetchObject();

			// print "<p>$scan__s</p>";
			
			// print "<p>$d and $charity__passed__direct__s</p>";
 

			?>


			<form action="process.php" method="get">
				<fieldset class="side_left">
					<dl>
						<dt>Select Main Charity</dt>
						<dd>
							 <input type="text" id="default" name="charity" placeholder="e.g. Age UK" value="<?php print $scan__d->charity_name; ?>">
						</dd>
					</dl>					
					<dl>
						<dt>Donation via</dt>
						<dd>
							<select name="donation__via">
								<option>Direct</option>
								<option <?php if ($scan__d->via == "Just Giving"): ?>selected<?php endif; ?>>Just Giving</option>
							</select>
						</dd>
					</dl>
					<dl><dt>Relevant Date</dt><dd><input type="text" name="relevant__date" class="datetimepicker" value="<?php print date('d/m/Y', strtotime($scan__d->when_donated)); ?>" /></dd></dl>
				</fieldset>
				<fieldset class="side_right">
					<dl>
						<dt>Amount (&pound; or Hours)</dt>
						<dd><input type="text" name="amount" value="<?php print $scan__d->amount; ?>"/></dd>
						<dd style='width: 120px;'>
							<input type="checkbox" name="time__ticker" style='width: 10px;' value="y" /><span style='font-size: 0.7em; '> (Time)</span>
						</dd>
					</dl>
					<dl><dt>Information</dt><dd><textarea name="info"><?php print $scan__d->reference; ?></textarea></dd></dl>
					<input type="hidden" name="a" value="add-donation.php" />
				</fieldset>
				<input type="hidden" name="imported__email__id" value="<?php print $scan__d->id; ?>" />
				<p><input type="submit" value="Add Donation" /></p>


			</form>




		</div>
	</div>
</div>
