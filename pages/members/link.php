<script>

	$(function() {



		$('body').addClass('crumbs');

	});


</script>


<?php include('include__member__functions.php'); if($_SESSION['mine'] == true) { include('include__member__nav.php'); }; ?>
<?php $user = fetchUser($_SESSION['user_id']); ?>



<div id="content">
	<div class="inner">
		<div class="left wide">


			<h3>Link Accounts</h3>


			<p>Here you can link more social media accounts.</p>


			<br />



			<meta name="google-signin-scope" content="profile email">
			<meta name="google-signin-client_id" content="<? echo Config::googleClientId(); /* NOTE: Config Google Sign On Client Id */ ?>">
			<script src="https://apis.google.com/js/platform.js?onload=startApp" async defer></script>

			<script>
				// This is called with the results from from FB.getLoginStatus().
				function statusChangeCallback(response) {
					if (response.status === 'connected') {
						// Logged into your app and Facebook.
						fetchAPI(response);
					}
				}

				// This function is called when someone finishes with the Login
				// Button.  See the onlogin handler attached to it in the sample
				// code below.
				function checkLoginState() {
					FB.getLoginStatus(function(response) {
						statusChangeCallback(response);
					});
				}

				window.fbAsyncInit = function() {
					FB.init({
						appId      : '<? echo Config::facebookAppId(); /* NOTE: Config Facebook App ID */ ?>',
						cookie     : true,  // enable cookies to allow the server to access
											// the session
						xfbml      : true,  // parse social plugins on this page
						version    : '<? echo Config::facebookVersion(); /* NOTE: Config Facebook Version */ ?>'
					});

				};

				// Load the SDK asynchronously
				(function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) return;
					js = d.createElement(s); js.id = id;
					js.src = "//connect.facebook.net/en_US/sdk.js";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));

				function fetchAPI(response) {
					window.open('link-fb.php?token='+response.authResponse.accessToken, '_self');
				}

				function doFbLogin() {
					<?php if (empty($user->facebook_id)): ?>
					FB.login(checkLoginState, 'public_profile,email');
					<?php endif; ?>
				}
			</script>
			<script>
				function onSignIn(googleUser) {
					window.open('link-gplus.php?token='+btoa(JSON.stringify(googleUser.getAuthResponse())), '_self');
				};
				var startApp = function() {
					gapi.load('auth2', function(){
						// Retrieve the singleton for the GoogleAuth library and set up the client.
						auth2 = gapi.auth2.init({
							client_id: '<? echo Config::googleClientId(); /* NOTE: Config Google Sign On Client Id (Again!)*/ ?>',
							cookiepolicy: 'single_host_origin',
							// Request scopes in addition to 'profile' and 'email'
							//scope: 'additional_scope'
						});
						attachSignin(document.getElementById('gplus_login'));
					});
				};
				function attachSignin(element) {
					<?php if (empty($user->google_id)): ?>
					auth2.attachClickHandler(element, {},
							onSignIn, function(error) {
								alert(JSON.stringify(error, undefined, 2));
							});
					<?php endif; ?>
				}
			</script>

			<!-- The plugin will be embedded into this div //-->
			<div>
				<div style="display: inline-block; width: 300px;">
					<img src="images/facebook.png" style="vertical-align: middle;margin-right: 5px;<?php if (!empty($user->facebook_id)): ?>filter:grayscale(100%);-webkit-filter: grayscale(100%);<?php endif; ?>cursor: pointer" onclick="doFbLogin()" />
					<?php if (!empty($user->facebook_id)): ?>
						Already linked
					<?php else: ?>
						Click to link
					<?php endif; ?>
				</div>
				<div style="display: inline-block; width: 300px;">
					<img src="images/gplus.png" style="vertical-align: middle;margin-right: 5px;<?php if (!empty($user->google_id)): ?>filter:grayscale(100%);-webkit-filter: grayscale(100%);<?php endif; ?>cursor: pointer" id="gplus_login" />
					<?php if (!empty($user->google_id)): ?>
						Already linked
					<?php else: ?>
						Click to link
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
