<script>
	$(function() {

		$('a.notification').click(function() {
			$('ul.notifications').slideToggle();
			event.preventDefault();
			return false;

		});


		$('body').addClass('crumbs');

	});

</script>
<?php

# $_SESSION[user_id] is the id from the actual user who has signed in with Facebook

# $uid is the id number taken from the url ($uid = $d;)

# Capture the user id from the url

$uid = $d;

$member__s = "SELECT * FROM users WHERE id = ? LIMIT 1";
$member__q = $DB->prepare($member__s);
$member__q->execute(array($uid));
$member__d = $member__q->fetchObject();

//$member__d->bx = preg_replace('/\n/', '<br />', $member__d->bx);

$mine = false;
if($uid == $_SESSION['user_id']) {
    $mine = true;
    if(!isset($_SESSION['mine'])) {
        $_SESSION['mine'] = true;
    }
    include('include__member__functions.php'); # NOTE: Notification functions LIVE_DATA_NODE stuff! :-(
    include('include__member__nav.php');
}

?>

<div id="content">
	<div class="inner">
		<div class="left">
		<?php

			print "<h3>$member__d->first_name $member__d->last_name Profile</h3>";
			print "<div class='fleft'>";
			if (strlen($member__d->image_url)) {
				print "<img src='$member__d->image_url' />";
			}

			if($mine == true) { print "<a href='pages/members/update-member/' class='green__button'>Update profile</a>"; }

			print "</div><p>".htmlentities($member__d->profile)."</p>";

			?>
			<br clear="all" />
            <hr />
            <h3>Features</h3>
			<p>You can forward your email receipts to us and we will automatically add the data to your account.</p>
			<h4>How it works</h4>
			<p>First, forward your email receipt to <strong>donations@igivemonthly.com</strong>. </p>
			<p>Next, try emailing something into it, and hey presto! login to your account and you can see the results in your account.</p>
		</div><div class="right"><div class="padd">
			<div class="boxy">

				<h5>Charities I follow: <a class='toggle__p' href='#'>Help</a></h5>

				<p class='hide'>Add a charity you are interested in to your watch-list, see latest news and updates.</p>

				<?php

				$join__s = "SELECT * FROM charity_user WHERE user_id = ? LIMIT 10";

				$join__q = $DB->prepare($join__s);

				$join__q->execute(array($member__d->id));

				//print "<p>@$join__s@</p>";

				if($join__q->rowCount() != NULL) {

					for($n = 0; $n < $join__q->rowCount(); $n++) {

						$join__d = 	$join__q->fetchObject();
						$charity__s = "SELECT * FROM charities WHERE id = ? LIMIT 1";

						//print "<p>@$charity__s@</p>";

						$charity__q = $DB->prepare($charity__s);
						$charity__q->execute(array($join__d->charity_id));
						$charity__d = $charity__q->fetchObject();

						if(file_exists("uploads/charity-logos/$charity__d->image_name")) {
							print "<div class='charity--logos'><a href='charity/$charity__d->id/'><img src='uploads/charity-logos/$charity__d->image_name' /></a>$charity__d->name</div>";
						} else {
							print "<div class='charity--logos'><a href='charity/$charity__d->id/'><img src='sketch/logo--coming--soon.png' /></a>$charity__d->name</div>";
						}
					}
				} else {
					print "<p><b>No charities followed.</b></p>";
				}

				print "<p><a href='pages/charities/browse/' class='green__button'>Follow A Charity</a></p>";

				?>
			</div>
			<div class="boxy">
				<h5>Charities I sponsor through IGM: <a class='toggle__p' href='#'>Help</a></h5>
				<p class="hide">If it is available, you can sponsor a charity and reserve the enhanced profile page which the charity is then able to customise. As a sponsor your profile will be featured on the page.</p>
				<?php

				
				$sponsored__s = "SELECT * FROM sponsored_charities WHERE user_id = ? ORDER BY expires_time DESC limit 5";
				$sponsored__q = $DB->prepare($sponsored__s);
				$sponsored__q->execute(array($member__d->id));
				if($sponsored__q->rowCount() != NULL) {
					for($ss = 0; $ss < $sponsored__q->rowCount(); $ss++) {
						$sponsored__d = $sponsored__q->fetchObject();
                        if (is_file("uploads/charity-logos/$sponsored__d->image_name")) {
							print "<div class='charity--logos'><a href='charity/$sponsored__d->id/'><img src='uploads/charity-logos/$sponsored__d->image_name' /></a>$sponsored__d->name</div>";
						} else {
							print "<div class='charity--logos'><a href='charity/$sponsored__d->id/'><img src='sketch/logo--coming--soon.png' /></a>$sponsored__d->name</div>";
						}
					}
				} else {
					print "<p><b>No charities sponsored.</b></p>";
				}

				print "<p><a href='pages/charities/browse/' class='green__button'>Sponsor A Charity</a></p>";

				?>
			</div>
			<div class="boxy">
				<h5>Recent Donations:</h5>
				<p>Last 10 donations</p>
				<?php

				$donations__s = "SELECT * FROM charity_donations WHERE user_id = ? ORDER BY when_donated DESC LIMIT 10";
				$donations__q = $DB->prepare($donations__s);
				$donations__q->execute(array($member__d->id));

				for($t = 0; $t < $donations__q->rowCount(); $t++) {
					$donations__d = $donations__q->fetchObject();
					$donations__d->when_donated = date('d/M/Y', strtotime($donations__d->when_donated));

					print '<dl class="strip fixed">';
					print "<dd><strong>$donations__d->name</strong> | ". $donations__d->when_donated . " | ".CURRENCY_SYMBOL()."$donations__d->amount</dd>";
					print '</dl>';
				}

				if($mine == true) {
					print "<p><a href='pages/members/add-donation/' class='green__button'>Add Donation</a><a href='stats/$_SESSION[user_id]/' class='green__button'>See All Stats</a></p>";
				}

				//print "<p>$donations__s</p>";

				?>

			</div>
		</div></div>
	</div>
</div>