<script>

	$(function() {

		$('body').addClass('crumbs');

	});


</script>

<?php include('include__member__functions.php'); if($_SESSION['mine'] == true) { include('include__member__nav.php'); }; ?>


<div id="content">
	<div class="inner">
		<div class="left wide">
			<h3>Update Profile</h3>
			<p>Update your profile.</p>
			<?php

			$member__s = "SELECT * FROM users WHERE id = ? LIMIT 1";
			$member__q = $DB->prepare($member__s);
			$member__q->execute(array($_SESSION['user_id']));
			$member__d = $member__q->fetchObject();
            
			?>
			<hr />
			<form action="process.php" method="post" enctype="multipart/form-data">
				<fieldset>
                    <dl><dt>Avatar:</dt><dd>
                        <?php
                            echo '<div class="avatars">';
                            if (!empty($member__d->image_url)) {
                                echo '<div><input type="radio" name="image_url" id="avatar-current" value="' . $member__d->image_url . '" checked="checked" />';
                                echo '<label class="avatar" for="avatar-current">';
                                echo '<img src="' . $member__d->image_url . '" /><span>';
                                if (!empty($member__d->facebook_id) && strpos($member__d->image_url, 'facebook.com')) {
                                    echo 'From Facebook';
                                } elseif (!empty($member__d->google_id) && strpos($member__d->image_url, 'googleusercontent.com')) {
                                    echo 'From Google';
                                } elseif (strpos($member__d->image_url, 'gravatar.com')) {
                                    echo 'From Gravatar';
                                } else {
                                    echo 'Uploaded';
                                }
                                echo '</span></label></div>';
                            }
                            if (!empty($member__d->social_url) && $member__d->social_url != $member__d->image_url) {
                                echo '<div><input type="radio" name="image_url" id="avatar-social" value="' . $member__d->social_url . '" />';
                                echo '<label class="avatar" for="avatar-social">';
                                echo '<img src="' . $member__d->social_url . '" /><span>';
                                if (!empty($member__d->facebook_id)) {
                                    echo 'From Facebook';
                                } else {
                                    echo 'From Google';
                                }
                                echo '</span></label></div>';
                            }
                            if (!strpos($member__d->image_url, 'gravatar.com')) {
                                $image_gravatar = 'https://www.gravatar.com/avatar/' . md5(strtolower(trim($member__d->email))) . '.jpg?s=150?d=404';
                                echo '<div><input type="radio" name="image_url" id="avatar-gravatar" value="' . $image_gravatar . '" />';
                                echo '<label class="avatar" for="avatar-gravatar">';
                                echo '<img src="' . $image_gravatar . '" />';
                                echo '<span>From Gravatar</span>';
                                echo '</label></div>';
                            }
                            $found = false;
                            $image_file = sprintf('images/avatars/%s.', $_SESSION['user_id']);
                            if (is_file(Env::basePath() . '/' . $image_file . 'gif')) {
                                $image_file .= 'gif';
                                $found = true;
                            } elseif (is_file(Env::basePath() . '/' . $image_file . 'jpg')) {
                                $image_file .= 'jpg';
                                $found = true;
                            } elseif (is_file(Env::basePath() . '/' . $image_file . 'png')) {
                                $image_file .= 'png';
                                $found = true;
                            }
                            if ($found && $member__d->image_url != $image_file) {
                                echo '<div><input type="radio" name="image_url" id="avatar-uploaded" value="' . $image_file . '" />';
                                echo '<label class="avatar" for="avatar-uploaded">';
                                echo '<img src="' . $image_file . '" />';
                                echo '<span>Uploaded</span>';
                                echo '</label></div>';
                            }
                            echo '<div><label class="avatar" for="file-avatar">';
                            echo '<img src="holder.js/150x150" id="file-preview" /><input type="file" name="file-avatar" id="file-avatar" accept="image/gif, image/jpeg, image/png" />';
                            echo '<span id="file-name">Upload new image</span>';
                            echo '<script>document.getElementById("file-avatar").onchange = function () {
                                var reader = new FileReader();
                                reader.fileName = this.files[0].name;
                                reader.onload = function (e) {
                                    // get loaded data and render thumbnail.
                                    document.getElementById("file-preview").src = e.target.result;
                                    document.getElementById("file-name").innerHTML = e.target.fileName;
                                };

                                // read the image file as a data URL.
                                reader.readAsDataURL(this.files[0]);
                                };</script>';
                            echo '</label></div>';
                            echo '</div>';
                        ?>
                    </dd></dl>
                </fieldset>
				<p><input type="submit" value="Update Avatar" /></p>
				<input type="hidden" name="a" value="update-avatar.php" />
			</form>
                        
            <form action="process.php" method="post">
				<fieldset>
                    <dl><dt>Gender:</dt><dd><label for="male"><input type="radio" name="gender" id="male" value="male" <?php if ($member__d->gender == 'male') { echo 'checked="checked" '; } ?>/> Male</label> <label for="female"><input type="radio" name="gender" id="female" value="female" <?php if ($member__d->gender == 'female') { echo 'checked="checked" '; } ?>/> Female</label></dd></dl>
					<dl><dt>Profile:</dt><dd><textarea name="info" rows="10" id="descTextArea"><?php print $member__d->profile; ?></textarea><br /><span id="stat"></span></dd></dl>
				</fieldset>
				<p><input type="submit" value="Update Profile" /></p>
				<input type="hidden" name="a" value="update-profile.php" />
			</form>

<!--			<hr />-->

<!--			<h4>Emails</h4>-->
<!---->
<!--			<p>You can add more email addresses (up to 3) to your account for use when forwarding emails to donations@igivemonthly.com</p>-->
<!---->
<!--			<p><strong>Validation will be required</strong></p>-->

			<?php
//			$es = "SELECT * FROM live__node__data WHERE aim = '27' AND ak = ?";
//			$eq = $DB->prepare($es);
//			$eq->execute(array($_SESSION['user_id']));
//			if($eq->rowCount() != 0) {
//				print "<hr /><p><strong>Email addresses found:</strong></p>";
//				for($x = 0; $x < $eq->rowCount(); $x++) {
//					$ed = $eq->fetchObject();
//					print '<dl class="strip stats" style="border-bottom: 0; margin: 0px; padding: 0;">';
//					print "<dd>$ed->av</dd>";
//					print "<dd>$ed->cv</dd>";
//					print "<dd><a href='process.php?a=delete__email.php&id=$ed->id'>Delete</a></dd>";
//					print '</dl>';
//
//				}
//
//			}
//			if($eq->rowCount() < 3) {
//				print '<hr />';
//				print '<h4>Add Email Address</h4>';
//				print '<form action="process.php" method="post">';
//				print '<fieldset>';
//				print '<dl><dt>Email Address</dt><dd><input type="text" name="email" /></dd></dl>';
//				print '</fieldset>';
//				print '<p><input type="submit" value="Add Email Address To My Account" /></p>';
//				print '<input type="hidden" name="a" value="add-email-address.php" />';
//				print '</form>';
//			} else {
//				print "<p><strong>Maximum reached</strong></p>";
//			}
			?>



		</div>
	</div>
</div>



<script>

	function countChar(val) {
	    var len = val.value.length;
	    if (len >= 500) {
	        val.value = val.value.substring(0, 500);
	        $('#stat').text(0);
	    } else {
	        $('#stat').text(500 - len + ' remaining');
	    }
	}
	countChar($('#descTextArea').get(0));
	$('#descTextArea').keyup(function() {
	    countChar(this);
	});

</script>


<?php
