<script>

	$(function() {

		$('body').addClass('crumbs');

	});


</script>

<?php

$member__s = "UPDATE users SET notify_emails = 0 WHERE id = ? LIMIT 1";
$member__q = $DB->prepare($member__s);
$member__q->execute(array($_SESSION['user_id']));

include('include__member__functions.php'); if($_SESSION['mine'] == true) { include('include__member__nav.php'); };

?>

<div id="content">
	<div class="inner">
		<div class="left wide">


			<h3>Forwarded Emails</h3>


			<p>Here you can see any of the emails forwarded matching your email address in your profile.</p>

			<p><strong>1. Donate</strong><br />
			Donate to your favourite charity online like Just Giving, Virgin money etc. </p>

			<p><strong>2. Forward Email</strong><br />
			Once you have received your confirmation email from the charity forward to <a href="mailto:donations@igivemonthly.com">donations@igivemonthly.com</a></p>

			<p><strong>3. Login to I Give Monthly</strong><br />
			Login to I Give Monthly and click on 'Forwarded Emails' and you should see your email in your list, if not (i.e. you have used an unregistered address) goto update profile, scroll down and add your email address you sent from. Other wise, click import and all the data decoded will populate the form and hit Add! It's that easy.</p>


			<?php

			$scan__s = "SELECT * FROM emails WHERE user_id = ? and processed = 0";
			$scan__q = $DB->prepare($scan__s);
			$scan__q->execute(array($_SESSION['user_id']));

			if($scan__q->rowCount() != NULL) {

				print "<h3>Emails found</h3>";

				for($xb = 0; $xb < $scan__q->rowCount(); $xb++) {
					$scan__d = $scan__q->fetchObject();

					print '<dl class="strip stats" style="border-bottom: 0; margin: 0px; padding: 0;">';
					print "<dd>$scan__d->email</dd>";
					print "<dd>$scan__d->amount</dd>";
					print "<dd>$scan__d->when_donated</dd>";
					print "<dd>$scan__d->charity_name</dd>";
					print "<dd><a href='pages/members/import-donation/$scan__d->id/'>Import</a></dd>";
					print "<dd><a href='process.php?a=delete__forwarded__email.php&id=$scan__d->id'>Delete</a></dd>";
					print '</dl>';

				}


			} else {

				print "<p><strong><ins>No emails found.</ins></strong></p>";

			}



			?>


		</div>
	</div>
</div>
