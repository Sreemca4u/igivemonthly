<script>
	$(function() {
		$('body').addClass('crumbs');
	});
</script>
<?php

if (array_key_exists('d', $_REQUEST)) {
	$d = $_REQUEST['d'];
} else {
	$d = '';
}

$charity__passed__direct__s = "SELECT id, name FROM charities WHERE id = ?";
$charity__passed__direct__q = $DB->prepare($charity__passed__direct__s);
$charity__passed__direct__q->execute(array($d));
$charity__passed__direct__d = $charity__passed__direct__q->fetchObject();

// print "<p>$d and $charity__passed__direct__s</p>";

include('include__member__functions.php'); if($_SESSION['mine'] == true) { include('include__member__nav.php'); }; ?>

<div id="content">
	<div class="inner">
		<div class="left wide">
			<h3>Add donation</h3>
			<form action="process.php" method="get">
				<fieldset class="side_left">
					<dl>
						<dt>Select Main Charity</dt>
						<dd>
							<select name="charity" class="turn-to-ac" required>
								<option disabled selected>Select main charity...</option>
								<?php
									$S = "SELECT * FROM charities ORDER BY name ASC";
									$Q = $DB->query($S);
									if($Q->rowCount() != NULL) {
										for($t = 0; $t < $Q->rowCount(); $t++) {
											$D = $Q->fetchObject();
											if($D->id == $d) {
												print "<option value='$D->id' selected='selected'>$D->name</option>";
											} else {
												print "<option value='$D->id'>$D->name</option>";
											}
										}
									}
								?>
							</select>

							<!-- <input type="text" id="default" list="charaties" name="charity" placeholder="e.g. Age UK" value="<?php print $charity__passed__direct__d->av; ?>"> -->
						</dd>
					</dl>
					<dl>
						<dt>Frequency</dt>
						<dd>
							<select name="frequency" id="frequency">
								<option value="single">Single donation</option>
								<option value="monthly">Monthly donation</option>
							</select>
						</dd>
					</dl>
					<dl id="select__type__activity">
						<dt>Select/Type Activity</dt>
						<dd>
							<select name="fundraising__method">
								<option value="0A">NA</option>
								<?php
								$fundraising__method__s = "SELECT id, av FROM live__node__data WHERE aim = 15";# NOTE: LIVE_NODE_DATA aim=15 for Activities
								$fundraising__method__q = $DB->query($fundraising__method__s);
								for($cu = 0; $cu < $fundraising__method__q->rowCount(); $cu++) {
									$fundraising__method__d = $fundraising__method__q->fetchObject();
									print "<option value='$fundraising__method__d->id'>$fundraising__method__d->av</option>";
								}
								?>
							</select>
						</dd>
					</dl>
					<dl>
						<dt>Donation via</dt>
						<dd>
							<select id="donation__via__select" name="donation__via">
								<option value="0">NA</option>
								<?php
								$via__s = "SELECT id, av FROM live__node__data WHERE aim = 24 ORDER BY av ASC";# NOTE: LIVE_NODE_DATA aim=24 for Donation Via
								$via__q = $DB->query($via__s);
								for($v = 0; $v < $via__q->rowCount(); $v++) {
									$via__d = $via__q->fetchObject();
									print "<option value='$via__d->id'>$via__d->av</option>";
								}
								?>
							</select><br /><br /><input type="text" name="donation__via__other" autocomplete="off" id="donation__via__input" placeholder="other" />
						</dd>
					</dl>
				</fieldset>
				<fieldset class="side_right">
					<dl><dt>Relevant Date (dd/mm/yyyy)</dt><dd><input type="text" name="relevant__date" class="datetimepicker" value="<?php echo date('d/m/Y'); ?>" /></dd></dl>
					<dl id="end__date__dl"><dt>End Date (dd/mm/yyyy)</dt><dd><input type="text" name="end__date" class="datetimepicker" /></dd></dl>
					<dl>
						<dt>Amount (&pound; or Hours)</dt>
						<dd><input type="text" name="amount" autocomplete="off" /></dd>
						<dd style='width: 120px;'>
							<input type="checkbox" name="time__ticker" style='width: 10px;' value="y" autocomplete="off" /><span style='font-size: 0.7em; '> (Time)</span>
						</dd>
						<dd><?php monies(); ?></dd>
					</dl>

					<dl><dt>Information</dt><dd><textarea name="info"></textarea></dd></dl>
					<input type="hidden" name="a" value="add-donation.php" />
				</fieldset>
				<center><p><input type="submit" value="Add Donation" /></p></center>


			</form>
		</div>
	</div>
</div>


<script>

	$(function() {


		$('#end__date__dl, #donation__via__input').hide();

		$('#donation__via__select').on('change', function() {
			thevalue = this.value;
			if(thevalue == 328) { $('#donation__via__input').slideDown(300); } else { $('#donation__via__input').slideUp(300); } // donation__via__input
		});


		$('#frequency').on('change', function() {
			//alert( this.value ); // or $(this).val()
			thevalue = this.value;
			if(thevalue == 'monthly') {
				$('#select__type__activity').slideUp(300);
				$('#end__date__dl').slideDown(300);
			} else {
				$('#select__type__activity').slideDown(300);
				$('#end__date__dl').slideUp(300);
			}
		});

	});

</script>
