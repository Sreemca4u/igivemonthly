<?php

# Recommended editor 'Brackets' by Adobe: http://brackets.io
# with extension 'Todo' by Mikael Jorhult (search and install within Extension Manager): https://github.com/mikaeljorhult/brackets-todo

# Configs for site
# ----------------

date_default_timezone_set('Europe/London');

error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

# NOTE: Config::addEnv(string $env, string|array $host) Adds environments. $host is searched for in the $_SERVER['HTTP_HOST'].
Config::addEnv('local', ['localhost', 'igivemonthly.dev']);
#Config::addEnv('staging', 'kloc');
Config::addEnv('staging', 'igmdev1');
Config::addEnv('live', 'igivemonthly');

# NOTE: Config::db(string $env, string $server, string $database, string $user, string $env, string $password = '') Set database connections for each environment
Config::db('local', '127.0.0.1', 'igivemon_w6wr628', 'root', 'Qw3rty');
#Config::db('staging', '127.0.0.1', 'igivemon_w6wr628', 'root', '9T9y8u8i');
Config::db('staging', '10.169.0.110', 'igivemon_igmdev1', 'igivemon_igmdev1', '~#Ju5tG1v1nG~#');
Config::db('live', '10.169.0.12', 'igivemon_w6wr628', 'igivemon_w6wr628', 'd5tNbRan');

# NOTE: Config::[config](string $env, string $data) Set configs

# NOTE: Config::https
Config::https('local', false);
#Config::https('staging', false);
Config::https('staging', false);
Config::https('live', true);

# Note: Contact Us Email

Config::contactUsEmailHost('local', 'smtp.gmail.com');
Config::contactUsEmailFrom('local', 'kevatkloc@gmail.com');
Config::contactUsEmailFromPassword('local', '~#Qw3rty~#');
Config::contactUsEmailTo('local', 'kevin.warren@kloc.co.uk');

Config::contactUsEmailHost('staging', 'smtp.gmail.com');
Config::contactUsEmailFrom('staging', 'kevatkloc@gmail.com');
Config::contactUsEmailFromPassword('staging', '~#Qw3rty~#');
Config::contactUsEmailTo('staging', 'kevin.warren@kloc.co.uk');

Config::contactUsEmailHost('local', 'smtp.igivemonthly.com');
Config::contactUsEmailFrom('live', 'contact-us@igivemonthly.com');
Config::contactUsEmailFromPassword('live', 'Password');
Config::contactUsEmailTo('live', 'info@igivemonthly.com');
    
# Note: Stripe
Config::stripeApiKey('local', 'sk_test_XhjvukDoM28NpEQgNwbAFGtu');
Config::stripeDataKey('local', 'pk_test_lEP2n7yVD0OGZAp8YXFy0A9p');

Config::stripeApiKey('staging', 'sk_test_XhjvukDoM28NpEQgNwbAFGtu');
Config::stripeDataKey('staging', 'pk_test_lEP2n7yVD0OGZAp8YXFy0A9p');

Config::stripeApiKey('live', 'sk_live_zk3oOJJphb3u5sMVnUrRxw6P');
Config::stripeDataKey('live', 'pk_live_93j8deUN3Ip4EI00eACOgo5V');

# Note: Facebook
Config::facebookAppId('local', '2053259788231953');
Config::facebookAppSecret('local', '17776f1e1dfaf93460313b5aa26de20b');
Config::facebookVersion('local', 'v2.6');

Config::facebookAppId('staging', '2053259788231953');
Config::facebookAppSecret('staging', '17776f1e1dfaf93460313b5aa26de20b');
Config::facebookVersion('staging', 'v2.6');

Config::facebookAppId('live', '319035028445663');
Config::facebookAppSecret('live', '736b7590d6cd23012edbdc2dfe443282');
Config::facebookVersion('live', 'v2.7');

# Note: Google
Config::googleKey('local', 'AIzaSyADOq_osvMLrXcTay-zP3arACU7-WiRwCk');
Config::googleClientId('local', '993271420727-mcqla503gippgjr676o9o9etqnife23s.apps.googleusercontent.com');
Config::googleClientSecret('local', 'gb0R6qzoa61DzwZkqqIqFYNy');
Config::googleRedirectUri('local', 'http://igivemonthly.dev/login-gplus.php');

Config::googleKey('staging', 'AIzaSyADOq_osvMLrXcTay-zP3arACU7-WiRwCk');
Config::googleClientId('staging', '993271420727-mcqla503gippgjr676o9o9etqnife23s.apps.googleusercontent.com');
Config::googleClientSecret('staging', 'gb0R6qzoa61DzwZkqqIqFYNy');
Config::googleRedirectUri('staging', 'http://igmdev1.igivemonthly.com/login-gplus.php');

Config::googleKey('live', 'AIzaSyADOq_osvMLrXcTay-zP3arACU7-WiRwCk');
Config::googleClientId('live', '993271420727-mcqla503gippgjr676o9o9etqnife23s.apps.googleusercontent.com');
Config::googleClientSecret('live', 'gb0R6qzoa61DzwZkqqIqFYNy');
Config::googleRedirectUri('live', 'https://www.igivemonthly.com/login-gplus.php');

# Note: Twitter
Config::twitterWidgetId('local', '542194953745874944');
Config::twitterConsumerKey('local', 'apAPY3nR3ePlRr4H3bO9cAaFp');# NOTE: KLOC Test Twitter Account App
Config::twitterConsumerSecret('local', 'k2KhYOzyMj1HVYfNpJuL6DoOFWyGPdMpG4OWVRUWqG1q9MbMgX');# NOTE: KLOC Test Twitter Account App
Config::twitterCallbackUrl('local', 'http://igivemonthly.dev/scripts/login-with-twitter/oauth.php');

Config::twitterWidgetId('staging', '542194953745874944');
Config::twitterConsumerKey('staging', 'apAPY3nR3ePlRr4H3bO9cAaFp');# NOTE: KLOC Test Twitter Account App
Config::twitterConsumerSecret('staging', 'k2KhYOzyMj1HVYfNpJuL6DoOFWyGPdMpG4OWVRUWqG1q9MbMgX');# NOTE: KLOC Test Twitter Account App
Config::twitterCallbackUrl('staging', 'http://igmdev1.igivemonthly.com/scripts/login-with-twitter/oauth.php');

Config::twitterWidgetId('live', '542194953745874944');
Config::twitterConsumerKey('live', '9FHsueAVM7EqnSs0MAyuqMjvw');#siKE4DwojlAAWhfVjU9T1iVhy
Config::twitterConsumerSecret('live', 's7bqsnalzkon9ZjU8HOLZXhIFS6pJWUej790pwG8TIBlau9mAx');#ZWdcHntwajQVKvgKOeSPNsBMNtjM5lBFBKvPWrpFzetOGesMwn
Config::twitterCallbackUrl('live', 'https://www.igivemonthly.com/scripts/login-with-twitter/oauth.php');

# Note: OneAll
Config::oneallSubdomain('local', 'igivemonthly');
Config::oneallPublicKey('local', 'b21fa8c6-e5ee-48da-821d-a1333c143096');
Config::oneallPrivateKey('local', 'ec4e633e-ec13-4f35-8c58-7a5d6d777e21');

Config::oneallSubdomain('staging', 'igivemonthly');
Config::oneallPublicKey('staging', 'b21fa8c6-e5ee-48da-821d-a1333c143096');
Config::oneallPrivateKey('staging', 'ec4e633e-ec13-4f35-8c58-7a5d6d777e21');

Config::oneallSubdomain('live', 'igivemonthly');
Config::oneallPublicKey('live', 'b21fa8c6-e5ee-48da-821d-a1333c143096');
Config::oneallPrivateKey('live', 'ec4e633e-ec13-4f35-8c58-7a5d6d777e21');