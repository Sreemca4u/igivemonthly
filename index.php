<?php

require_once('includes/includes.php');

?>
<!DOCTYPE html>

<html class="no-js">

<head>

	<title>IGM</title>
	<meta charset="UTF-8" />
	<meta name="viewport" id="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<base href="<?php echo Env::baseUrl() ?>" />

	<link rel="apple-touch-icon" sizes="57x57" href="apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">

    <link rel="manifest" href="manifest.json">

	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
	<script src="scripts/bxslider/jquery.bxslider.min.js"></script>
	<script src="scripts/datetimepicker/jquery.datetimepicker.js"></script>
	<script src="scripts/selectToAutocomplete/jquery-ui.min.js"></script>
    <script src="vendor/holder.min.js"></script>

	<script src="scripts/selectToAutocomplete/jquery.select-to-autocomplete.js"></script>
	<script src="scripts/magnific__popup/dist/jquery.magnific-popup.min.js"></script>

	<link rel="stylesheet" href="scripts/magnific__popup/dist/magnific-popup.css">

	<script type="text/javascript" src="scripts/validation/dist/jquery.validate.min.js"></script>

	<script>
		$(window).load(function() {

            $('p.hide').hide();

            $('a.toggle__p').click(function() {

				$('p.hide').slideToggle('slow');

				return false;

			});

			$('ul.slides').bxSlider({ mode: 'fade', hideControlOnEnd: false, pager: true, controls: true, auto: true, pause: 6000, speed: 2000 });


			// Validate

			$('#validate').validate();

//			$('#validate :input').blur(function() {

//				$('#validate').validate().form();

//			});


			// popup

			$('.popup').magnificPopup({ type:'iframe', src: '<div>Content...</div>' });


			$('a.delete__card').click(function() {


				$(this).parent('p').parent('div').hide('slow');

				var thehref = $(this).attr("href");

				$.post(thehref);

				return false;

			});


			$('.confirm').click(function() { // confirm delete or logout

				var answer = confirm('Are you sure?');

				return answer;

			});


            // http://xdsoft.net/jqplugins/datetimepicker/

			$('.datetimepicker').datetimepicker({ timepicker: false, format:'d/m/Y', yearStart: '1920', mask:true });



			var oa = document.createElement('script');

			oa.type = 'text/javascript'; oa.async = true;

			oa.src = '//igivemonthly.api.oneall.com/socialize/library.js'

			var s = document.getElementsByTagName('script')[0];

			s.parentNode.insertBefore(oa, s)


			// Sizer

			/*$('body').append("<div id='sizer' style='background: black; color: white; font-weight: bold; padding: .4em; position: fixed; left: 20px; top: 20px; z-index: 9000;'>START</div>");

			$('#sizer').text($(window).width());*/

			$(window).resize(function() {

				width = $(window).width();

				//$('#sizer').text(width);

				if(width > 750) { $('#banner div.inner div > ul').show(); }

			});


			// Active nav for mobile

			$('a.activate_dropdown').click(function() {

				$('#nav ul').slideToggle('slow');

				return false;

			});



			// Active crumbs for mobile

			$('a.activate_crumbs_dropdown').click(function() {

				$('#crumbs div p').slideToggle('slow');

				return false;

			});


			$('a.notification').click(function() {

				$('ul.notifications').slideToggle();

				event.preventDefault();

				return false;

			});

		});


		// jQuery(function(){ jQuery('select.turn-to-ac').selectToAutocomplete(); });

	</script>

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="scripts/selectToAutocomplete/jquery-ui.css" media="screen">
	<link rel="stylesheet" type="text/css" href="scripts/datetimepicker/jquery.datetimepicker.css"/ >
	<link rel="stylesheet" href="css.css" media="screen">

</head>
<body>

<!-- Top bar, login and links to apps -->

	<div id="fixed">

		<div id="admin">

			<div class="inner">

				<p>

				<?php

				# User

				if(isset($_SESSION['user_id'])) {


					print '<a href="member/' . $_SESSION['user_id'] . '/" class="admin green__button">My Profile</a><a href="process.php?a=logout.php" title="Logout" class="green__button">Logout</a>';


				} else {


					print '<a href="pages/admin/login/" title="Sign-in to igivemonthly.com" class="sign-in green__button">Login</a>';

					print '<a href="pages/admin/register/" title="Sign-in to igivemonthly.com" class="sign-in green__button">Register</a> ';

					// print '<a href="pages/admin/charity-sign-in/" title="Sign-in to igivemonthly.com" class="sign-in green__button">Charity Sign-in</a>';


				}


				//print "<a class='sign-in green__button' href='RESET.php' style='background: #cc0000;'>RESET</a>";

				//print "<a class='sign-in green__button' href='member/$_SESSION[user_id]/' style='background: #cc0000;'>UID: $_SESSION[user_id]</a>";

				//print "<a class='sign-in green__button' href='charity/$_SESSION[charity_id]/' style='background: #B8E986;'>CID: $_SESSION[charity_id]</a>";


				# Charity


				if(isset($_SESSION['charity_id'])) {

					print "<a href='charity/$_SESSION[charity_id]/' title='Logout' class='green__button'>Charity Page</a>";

				}

/* Please leave alone */


	
function getIp() {
    $ip = getenv('HTTP_CLIENT_IP')?:getenv('HTTP_X_FORWARDED_FOR')?:getenv('HTTP_X_FORWARDED')?:getenv('HTTP_FORWARDED_FOR')?:getenv('HTTP_FORWARDED')?:getenv('REMOTE_ADDR');
    return $ip;
}


	if (!array_key_exists('country', $_SESSION)) {

		try {
            $locale_info = @file_get_contents('http://ipinfo.io/' . getIp() . '/json');
            $locale_info = json_decode($locale_info);
        } catch (Exception $e) {
            $locale_info = null;
        }

		if (!isset($locale_info->country)) {

			$_SESSION['country'] = 806;

			$_SESSION['country__flag'] = 'united__kingdom';

		} else {

			switch ($locale_info->country) {

				case 'AU':

					$_SESSION['country'] = 807;

					$_SESSION['country__flag'] = 'australia';

					break;

				case 'US':

					$_SESSION['country'] = 808;

					$_SESSION['country__flag'] = 'united__states';

					break;

				default:

					$_SESSION['country'] = 806;

					$_SESSION['country__flag'] = 'united__kingdom';

			}

		}

	}

	$js__country = $_SESSION['country'];

	//print "<p>Session country: $js__country;</p>";

//	print '<script>';

//	print '$(function() {';

//	//print "alert('one');";

//	print "var session__country__id = " . $js__country . "";

//	//print "alert('two');";

//	//print "alert(session__country__id);";

//	print '$.get("http://ipinfo.io", function (response) {';

//	print 'current__location = response.country;';

//	//print 'alert(current__location);';

//	# Now if $js__country is empty and we got the current__location, register correctly

//	// 806 uk, 807 aus, 808 us // GB // US // AU

//	if($_SESSION['country'] == '') {

//	print 'if(current__location == "GB") { window.location.href = "process.php?a=change__country.php&country=806"; }';

//	print 'if(current__location == "AU") { window.location.href = "process.php?a=change__country.php&country=807"; }';

//	print 'if(current__location == "US") { window.location.href = "process.php?a=change__country.php&country=808"; }';

//	};

//	print '}, "jsonp");';

//	print '});';

//	print '</script>';



/* Please leave alone */


//				if($_SESSION['country'] == '') {

//					$_SESSION['country'] = '806';

//					$_SESSION['country__flag'] = 'united__kingdom';

//				};


				$img__icon = '<img src="images/countries/' . $_SESSION['country__flag'] . '.png" style="margin-left: 10px;"/>';


				print "<a href='pages/members/country/' title='Change currency...'>$img__icon</a>";

				?>

				</p>

			</div>

		</div>

<!-- Banner/logo -->

		<div id="banner">

			<div class="inner">

				<h1><a href='./'><img src="sketch/logo.png" alt="IGiveMonthly"></a></h1>

				<?php

				//if($heart__d->thetotal >= 1000) {

					// $heart__d->thetotal = $heart__d->thetotal / 1000 . "k";   // NB: you will want to round this

					if($_SESSION['country'] == 806) { setlocale(LC_MONETARY, 'en_GB'); };

					if($_SESSION['country'] == 807) { setlocale(LC_MONETARY, 'en_US'); };

					if($_SESSION['country'] == 808) { setlocale(LC_MONETARY, 'en_US'); };



//					$heart__d->thetotal = utf8_encode(number_format($heart__d->thetotal, 2));


				//};


				?>


				<h3><span>Tracked: <?php print CURRENCY_SYMBOL() . trackedTotal($_SESSION['country']); ?></span></h3>

			</div>

		</div>

		<!-- Nav -->

		<div id="nav">

			<div class="inner">

				<a href="#" class="activate_dropdown">MENU</a>

				<ul>

					<li class="one"><a href="pages/content/home/">home</a></li

					><li class="two"><a href="pages/charities/browse/">browse charities</a></li

					><li class="three"><a href="pages/members/browse/">browse members</a></li

					><li class="four"><a href="pages/news/main/">news</a></li

					><li class="five"><a href="pages/content/features/">how it works</a></li>

				</ul>

			</div>

		</div>

	</div>

	<?php

	if (array_key_exists('a', $_GET)) {

		$a = $_GET['a'];

	} else {

		$a = null;

	}

	if (array_key_exists('b', $_GET)) {

		$b = $_GET['b'];

	} else {

		$b = null;

	}

	if (array_key_exists('c', $_GET)) {

		$c = $_GET['c'];

	} else {

		$c = null;

	}

	if (array_key_exists('d', $_GET)) {

		$d = $_GET['d'];

	} else {

		$d = null;

	}


	if($c == '') { include("pages/content/home.php"); } else { include("$a/$b/$c.php"); };# NOTE: Included files


	?>
	<div id="footer">

		<div class="inner">

			<div id="links">

				<div>

					<h5>Members</h5>

					<ul>

						<li><a href='pages/admin/sign-in/'>sign-up</a></li>

						<li><a href='pages/members/browse/'>browse members</a></li>

						<li><a href='pages/content/features/'>features</a></li>

						<li><a href='pages/content/terms/'>Terms &amp; Conditions</a></li>

						<!-- <li><a href='pages/content/privacy/'>Privacy Policy</a></li> -->

					</ul>

					<!--<a href="https://itunes.apple.com/gb/app/igivemonthly/id648267374?ls=1&mt=8" target="_new"><img src="sketch/footer/download__apple.png" alt="download__apple" /></a>

					<a href="https://play.google.com/store/apps/details?id=iGiveMonthly.Android&hl=en_GB" target="_new"><img src="sketch/footer/download__andriod.png" alt="download__andriod" /></a>-->

				</div>

				<div>

					<h5>Charities</h5>

					<ul>

						<li><a href='pages/charities/browse/'>secure your charity profile page</a></li>

						<li><a href='pages/admin/charity-sign-in/'>login</a></li>

						<li><a href='pages/content/fees/'>fees</a></li>

						<li><a href='pages/content/charity-terms/'>terms &amp; conditions</a></li>

					</ul>

				</div>

				<div>

					<h5>About Us</h5>

					<ul>

						<li><a href='pages/content/who-are-we/'>who are igivemonthly</a></li>

						<li><a href='pages/content/what-we-do/'>what do we do</a></li>

						<li><a href='pages/content/our-mission/'>our mission</a></li>

						<li><a href='pages/content/contact/'>contact us</a></li>

					</ul>

					<a href="https://twitter.com/@igivemonthly" target="_new"><img src="sketch/social/twitter.png" alt="twitter"></a>

					<a href="https://www.facebook.com/Igivemonthly?ref=hl" target="_new"><img src="sketch/social/facebook.png" alt="facebook"></a>

					<a href="https://plus.google.com/u/0/b/111308577641125655747/111308577641125655747/posts" target="_new"><img src="sketch/social/google-plus.png" alt="google-plus"></a>

				</div>

			</div>

			<div id="terms">

				<img src="sketch/footer/strapline.png" alt="strapline__footer">

				<p>&copy; <?php echo date('Y'); ?> I Give Monthly | Registration number: 08324251</p>

			</div>

		</div>

	</div>

</body>
</html>