<?php

/* Config class to set & get config settings for each environment
 *
 * 
 *
 */

# CLASS: Config
final class Config {
    private static $initialised = false;
    private static $environments = array();
    private static $environment = null;
    
    
    # FUNCTION: addEnv(string $env, string|array $host) Add a new environment
    public static function addEnv($env, $host) {
        if (!empty($env) && is_string($env) && !empty($host) && (is_string($host) || is_array($host))) {
            if (!array_key_exists($env, self::$environments)) {
                self::$environments = array_merge(self::$environments, [$env=>(object)array('hosts'=>'', 'db'=>null, 'https'=>null,
                    'contactUsEmail'=>(object)array('host'=>null, 'from'=>null, 'fromPassword'=>null, 'to'=>null),
                    'stripe'=>(object)array('apiKey'=>null, 'dataKey'=>null),
                    'facebook'=>(object)array('appId'=>null, 'appSecret'=>null, 'version'=>null),
                    'google'=>(object)array('key'=>null, 'clientId'=>null, 'clientSecret'=>null, 'redirectUri'=>null),
                    'twitter'=>(object)array('widgetId'=>null, 'consumerKey'=>null, 'consumerSecret'=>null, 'callbackUrl'=>null),
                    'oneall'=>(object)array('subDomain'=>null, 'publicKey'=>null, 'privateKey'=>null))]);
                self::$environments[$env]->hosts = $host;
            } else {
                if (is_string(self::$environments[$env]->hosts)) {
                    self::$environments[$env]->hosts = array(self::$environments[$env]->hosts, $host);
                } else {
                    self::$environments[$env]->hosts[] = $host;
                }
            }
        } else {
            die('Config::addEnv(string $env, string|array $host) expects $env to be a string & $host to be either a string or an array');
        }
    }
    
    # FUNCTION: init() Initialise function called 1st by all functions to automatically detect environment
    private static function init() {
        if (!self::$initialised) {
            foreach (self::$environments as $env=>$data) {
                if (is_array($data->hosts)) {
                    foreach ($data->hosts as $h) {
                        if (strpos($_SERVER['HTTP_HOST'], $h) !== false) {
                            self::$environment = $env;
                            break 2;
                        }  
                    }
                } elseif (strpos($_SERVER['HTTP_HOST'], $data->hosts) !== false) {
                    self::$environment = $env;
                    break 1;
                }
            }
            self::$initialised = true;
        }
    }
    
    # FUNCTION: env(string $env = null) Set or get site environment
    public static function env($env = null) {
        self::init();
        if (is_null($env)) {
            return self::$environment;
        } elseif (array_key_exists($env, self::$environments)) {
            self::$environment = $env;
        }
    }
    
    # FUNCTION: db(string $env = null, string $server = null, string $database = null, string $user = null, string $password = '') Set database connections for each environment
    public static function db($env = null, $server = null, $database = null, $user = null, $password = '') {
        self::init();
        if (array_key_exists($env, self::$environments)) {
            if (!empty($server) && is_string($server) && !empty($database) && is_string($database) && !empty($user) && is_string($user) && is_string($password)) {
                self::$environments[$env]->db = (object)array('server'=>$server, 'database'=>$database, 'user'=>$user, 'password'=>$password);
            } else {
                die('Config::db(string $env, string $server, string $database, string $user, string $password = \'\') expects $server, $database, $user & $password to be strings');
            }
        } else {
            die('Config::db(string $env, string $server, string $database, string $user, string $password = \'\'), \'' . $env . '\' is not in current list of envoronments: ' . implode('|', array_keys(self::$environments)));
        }
    }
    
    # FUNCTION: dbConnect() Return new PDO connection for current environment
    public static function dbConnect() {
        return new PDO('mysql:host=' . self::$environments[self::$environment]->db->server . ';dbname=' . self::$environments[self::$environment]->db->database, self::$environments[self::$environment]->db->user, self::$environments[self::$environment]->db->password);
    }
    
    # FUNCTION: https(string $env, boolean $data) Set or get https requirement
    public static function https($env = null, $data = null) {
        self::init();
        if (is_null($env)) {
            return self::$environments[self::$environment]->https;
        } elseif (array_key_exists($env, self::$environments) && is_bool($data)) {
            self::$environments[$env]->https = $data;
        } else {
            die('Config::https(string $env, boolean $data) expects $env to be string &amp; $data to be boolean');
        }
    }
    
    # FUNCTION: contactUsEmailHost(string $env, string $data) Set or get Contact Us Email STMP Host
    public static function contactUsEmailHost($env = null, $data = null) {
        self::init();
        if (is_null($env)) {
            return self::$environments[self::$environment]->contactUsEmail->host;
        } elseif (array_key_exists($env, self::$environments) && !empty($data) && is_string($data)) {
            self::$environments[$env]->contactUsEmail->host = $data;
        } else {
            die('Config::contactUsEmailHost(string $env, string $data) expects $env, $data to be strings');
        }
    }
    
    # FUNCTION: contactUsEmailFrom(string $env, string $data) Set or get Contact Us From Email Address
    public static function contactUsEmailFrom($env = null, $data = null) {
        self::init();
        if (is_null($env)) {
            return self::$environments[self::$environment]->contactUsEmail->from;
        } elseif (array_key_exists($env, self::$environments) && !empty($data) && is_string($data)) {
            self::$environments[$env]->contactUsEmail->from = $data;
        } else {
            die('Config::contactUsEmailFrom(string $env, string $data) expects $env, $data to be strings');
        }
    }
    
    # FUNCTION: contactUsEmailFromPassword(string $env, string $data) Set or get Contact Us From Email Address Password
    public static function contactUsEmailFromPassword($env = null, $data = null) {
        self::init();
        if (is_null($env)) {
            return self::$environments[self::$environment]->contactUsEmail->fromPassword;
        } elseif (array_key_exists($env, self::$environments) && !empty($data) && is_string($data)) {
            self::$environments[$env]->contactUsEmail->fromPassword = $data;
        } else {
            die('Config::contactUsEmailFromPassword(string $env, string $data) expects $env, $data to be strings');
        }
    }
    
    # FUNCTION: contactUsEmailTo(string $env, string $data) Set or get Contact Us To Email Address
    public static function contactUsEmailTo($env = null, $data = null) {
        self::init();
        if (is_null($env)) {
            return self::$environments[self::$environment]->contactUsEmail->to;
        } elseif (array_key_exists($env, self::$environments) && !empty($data) && is_string($data)) {
            self::$environments[$env]->contactUsEmail->to = $data;
        } else {
            die('Config::contactUsEmailTo(string $env, string $data) expects $env, $data to be strings');
        }
    }
    
    # FUNCTION: stripeApiKey(string $env, string $data) Set or get Stripe API Key
    public static function stripeApiKey($env = null, $data = null) {
        self::init();
        if (is_null($env)) {
            return self::$environments[self::$environment]->stripe->apiKey;
        } elseif (array_key_exists($env, self::$environments) && !empty($data) && is_string($data)) {
            self::$environments[$env]->stripe->apiKey = $data;
        } else {
            die('Config::stripeApiKey(string $env, string $data) expects $env, $data to be strings');
        }
    }
    
    # FUNCTION: stripeDataKey(string $env, string $data) Set or get Stripe Data Key
    public static function stripeDataKey($env = null, $data = null) {
        self::init();
        if (is_null($env)) {
            return self::$environments[self::$environment]->stripe->dataKey;
        } elseif (array_key_exists($env, self::$environments) && !empty($data) && is_string($data)) {
            self::$environments[$env]->stripe->dataKey = $data;
        } else {
            die('Config::stripeDataKey(string $env, string $data) expects $env, $data to be strings');
        }
    }
    
    # FUNCTION: facebookAppId(string $env, string $data) Set or get Facebook App ID
    public static function facebookAppId($env = null, $data = null) {
        self::init();
        if (is_null($env)) {
            return self::$environments[self::$environment]->facebook->appId;
        } elseif (array_key_exists($env, self::$environments) && !empty($data) && is_string($data)) {
            self::$environments[$env]->facebook->appId = $data;
        } else {
            die('Config::facebookAppId(string $env, string $data) expects $env, $data to be strings');
        }
    }
    
    # FUNCTION: facebookAppSecret(string $env, string $data) Set or get Facebook App Secret
    public static function facebookAppSecret($env = null, $data = null) {
        self::init();
        if (is_null($env)) {
            return self::$environments[self::$environment]->facebook->appSecret;
        } elseif (array_key_exists($env, self::$environments) && !empty($data) && is_string($data)) {
            self::$environments[$env]->facebook->appSecret = $data;
        } else {
            die('Config::facebookAppSecret(string $env, string $data) expects $env, $data to be strings');
        }
    }
    
    # FUNCTION: facebookVersion(string $env, string $data) Set or get Facebook Version
    public static function facebookVersion($env = null, $data = null) {
        self::init();
        if (is_null($env)) {
            return self::$environments[self::$environment]->facebook->version;
        } elseif (array_key_exists($env, self::$environments) && !empty($data) && is_string($data)) {
            self::$environments[$env]->facebook->version = $data;
        } else {
            die('Config::facebookVersion(string $env, string $data) expects $env, $data to be strings');
        }
    }
    
    # FUNCTION: googleKey(string $env, string $data) Set or get Google Key to get profile image
    public static function googleKey($env = null, $data = null) {
        self::init();
        if (is_null($env)) {
            return self::$environments[self::$environment]->google->key;
        } elseif (array_key_exists($env, self::$environments) && !empty($data) && is_string($data)) {
            self::$environments[$env]->google->key = $data;
        } else {
            die('Config::googleKey(string $env, string $data) expects $env, $data to be strings');
        }
    }
    
    # FUNCTION: googleClientId(string $env, string $data) Set or get Google Client ID
    public static function googleClientId($env = null, $data = null) {
        self::init();
        if (is_null($env)) {
            return self::$environments[self::$environment]->google->clientId;
        } elseif (array_key_exists($env, self::$environments) && !empty($data) && is_string($data)) {
            self::$environments[$env]->google->clientId = $data;
        } else {
            die('Config::googleClientId(string $env, string $data) expects $env, $data to be strings');
        }
    }
    
    # FUNCTION: googleClientSecret(string $env, string $data) Set or get Google Client Secret
    public static function googleClientSecret($env = null, $data = null) {
        self::init();
        if (is_null($env)) {
            return self::$environments[self::$environment]->google->clientSecret;
        } elseif (array_key_exists($env, self::$environments) && !empty($data) && is_string($data)) {
            self::$environments[$env]->google->clientSecret = $data;
        } else {
            die('Config::googleClientSecret(string $env, string $data) expects $env, $data to be strings');
        }
    }
    
    # FUNCTION: googleRedirectUri(string $env, string $data) Set or get Google Redirect Uri
    public static function googleRedirectUri($env = null, $data = null) {
        self::init();
        if (is_null($env)) {
            return self::$environments[self::$environment]->google->redirectUri;
        } elseif (array_key_exists($env, self::$environments) && !empty($data) && is_string($data)) {
            self::$environments[$env]->google->redirectUri = $data;
        } else {
            die('Config::googleRedirectUri(string $env, string $data) expects $env, $data to be strings');
        }
    }
    
    # FUNCTION: twitterWidgetId(string $env, string $data) Set or get Twitter Widget Id
    public static function twitterWidgetId($env = null, $data = null) {
        self::init();
        if (is_null($env)) {
            return self::$environments[self::$environment]->twitter->widgetId;
        } elseif (array_key_exists($env, self::$environments) && !empty($data) && is_string($data)) {
            self::$environments[$env]->twitter->widgetId = $data;
        } else {
            die('Config::twitterWidgetId(string $env, string $data) expects $env, $data to be strings');
        }
    }
    
    # FUNCTION: twitterConsumerKey(string $env, string $data) Set or get Twitter Key
    public static function twitterConsumerKey($env = null, $data = null) {
        self::init();
        if (is_null($env)) {
            return self::$environments[self::$environment]->twitter->consumerKey;
        } elseif (array_key_exists($env, self::$environments) && !empty($data) && is_string($data)) {
            self::$environments[$env]->twitter->consumerKey = $data;
        } else {
            die('Config::twitterConsumerKey(string $env, string $data) expects $env, $data to be strings');
        }
    }
    
    # FUNCTION: twitterConsumerSecret(string $env, string $data) Set or get Twitter Secret
    public static function twitterConsumerSecret($env = null, $data = null) {
        self::init();
        if (is_null($env)) {
            return self::$environments[self::$environment]->twitter->consumerSecret;
        } elseif (array_key_exists($env, self::$environments) && !empty($data) && is_string($data)) {
            self::$environments[$env]->twitter->consumerSecret = $data;
        } else {
            die('Config::twitterConsumerSecret(string $env, string $data) expects $env, $data to be strings');
        }
    }
    
    # FUNCTION: twitterCallbackUrl(string $env, string $data) Set or get Twitter Callback
    public static function twitterCallbackUrl($env = null, $data = null) {
        self::init();
        if (is_null($env)) {
            return self::$environments[self::$environment]->twitter->callbackUrl;
        } elseif (array_key_exists($env, self::$environments) && !empty($data) && is_string($data)) {
            self::$environments[$env]->twitter->callbackUrl = $data;
        } else {
            die('Config::twitterCallbackUrl(string $env, string $data) expects $env, $data to be strings');
        }
    }
    
    # FUNCTION: oneallSubDomain(string $env, string $data) Set or get OneAll Sub-domain
    public static function oneallSubDomain($env = null, $data = null) {
        self::init();
        if (is_null($env)) {
            return self::$environments[self::$environment]->oneall->subDomain;
        } elseif (array_key_exists($env, self::$environments) && !empty($data) && is_string($data)) {
            self::$environments[$env]->oneall->subDomain = $data;
        } else {
            die('Config::oneallSubDomain(string $env, string $data) expects $env, $data to be strings');
        }
    }
    
    # FUNCTION: oneallPublicKey(string $env, string $data) Set or get OneAll Public Key
    public static function oneallPublicKey($env = null, $data = null) {
        self::init();
        if (is_null($env)) {
            return self::$environments[self::$environment]->oneall->publicKey;
        } elseif (array_key_exists($env, self::$environments) && !empty($data) && is_string($data)) {
            self::$environments[$env]->oneall->publicKey = $data;
        } else {
            die('Config::oneallPublicKey(string $env, string $data) expects $env, $data to be strings');
        }
    }
    
    # FUNCTION: oneallPrivateKey(string $env, string $data) Set or get OneAll Private Key
    public static function oneallPrivateKey($env = null, $data = null) {
        self::init();
        if (is_null($env)) {
            return self::$environments[self::$environment]->oneall->privateKey;
        } elseif (array_key_exists($env, self::$environments) && !empty($data) && is_string($data)) {
            self::$environments[$env]->oneall->privateKey = $data;
        } else {
            die('Config::oneallPrivateKey(string $env, string $data) expects $env, $data to be strings');
        }
    }
}