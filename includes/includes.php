<?php

if (session_id() == null) {
    session_start();
    
    require_once(__DIR__ . '/env.php');
    require_once(__DIR__ . '/config.php');
    require_once(Env::configPath() . 'config.php');
    
    if (!Env::ssl() && Config::https()) {
        header('location: ' . Env::urlHttps());
        exit;
    } elseif (Env::ssl() && !Config::https()) {
        header('location: ' . Env::urlHttp());
        exit;
    }
    
    require_once(__DIR__ . '/connect.php');

    #include('../bridge.php'); # TODO: Change all values in site this sets up then remove

    require_once(Env::vendorPath() . 'facebook-php-sdk-v4-5.0.0/src/Facebook/autoload.php');
    require_once(__DIR__ . '/functions.php');
}