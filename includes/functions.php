<?php

function CURRENCY_SYMBOL() {
	switch ($_SESSION['country']) {
		case 806:
			return '&pound;';
		default:
			return "$";
	}
}

# NOTE: Dropdown for countries/currency


function monies() {
    global $DB;

    $S = "SELECT id, av, bv FROM live__node__data WHERE aim = 50";
    $Q = $DB->query($S);

    print '<select name="monies">';
    for($n = 0; $n < $Q->rowCount(); $n++) {
        $D = $Q->fetchObject();
        print "<option value='$D->id'>$D->bv</option>";
    }
    print '</select>';

}

# NOTE: ??? Currency convertor API - http://altafphp.blogspot.in/2013/11/convert-currency-using-google-finance.html

function covert__currency($amount, $from, $to) {
//	$arrContextOptions=array(
//		"ssl"=>array(
//			"verify_peer"=> false
//		)
//	);
//
//	$url  = "https://www.google.com/finance/converter?a=$amount&from=$from&to=$to";
//	$data = file_get_contents($url, null, stream_context_create($arrContextOptions));
//	preg_match("/<span class=bld>(.*)<\/span>/",$data, $converted);
//	$converted = preg_replace("/[^0-9.]/", "", $converted[1]);
	return round($amount, 3);
}

// echo covert__currency(100, "USD", "GBP");

# NOTE: Track Total Donations
function trackedTotal($country__id) {
	global $DB;
	$heart__s = "SELECT id, sum(amount) AS thetotal FROM donations group by id";
    $heart__q = $DB->query($heart__s);
    //print "$heart__s || $country__id ";
    $uk__amount = 0;
    $us__amount = 0;
    $au__amount = 0;
    $converted__total = 0;
    for($n = 0; $n < $heart__q->rowCount(); $n++) {
        $heart__d = $heart__q->fetchObject();
        //print "|| EK - $heart__d->ek || ";
        $uk__amount = $heart__d->thetotal;
    }
    return number_format($uk__amount, 2);
}

# NOTE: Track Total Donations for a charity
function trackedCharityTotal($country__id, $cid) {
    global $DB;
    $heart__s = "SELECT id, sum(amount) AS thetotal, charity_id FROM donations WHERE charity_id = ? GROUP BY charity_id";
    $heart__q = $DB->prepare($heart__s);
    $heart__q->execute(array($cid));
    //print "$heart__s || $country__id ";
    $uk__amount = 0;
    $us__amount = 0;
    $au__amount = 0;
    $converted__total = 0;
    for($n = 0; $n < $heart__q->rowCount(); $n++) {
        $heart__d = $heart__q->fetchObject();
        //print "|| EK - $heart__d->ek || ";
        $uk__amount = $heart__d->thetotal;
    }
    //print "<p>UK $uk__amount || US $us__amount || AUS $au__amount</p>";
    // UK
    if($country__id == 806) {
        $c__us = covert__currency($us__amount, "USD", "GBP");
        $c__au = covert__currency($au__amount, "AUD", "GBP");
        $converted__total = $uk__amount + $c__us + $c__au;
        setlocale(LC_MONETARY, 'en_GB');
        $converted__total = utf8_encode("&pound;" . sprintf('%0.2f', $converted__total));
    }
    // aus
    if($country__id == 807) {
        $c__uk = covert__currency($uk__amount, "GBP", "AUD");
        $c__us = covert__currency($us__amount, "USD", "AUD");
        $converted__total = $au__amount + $c__uk + $c__us;
        setlocale(LC_MONETARY, 'en_US');
        $converted__total = utf8_encode("$" . sprintf('%0.2f', $converted__total));
    }
    // us
    if($country__id == 808) {
        $c__uk = covert__currency($uk__amount, "GBP", "USD");
        $c__au = covert__currency($us__amount, "AUD", "USD");
        $converted__total = $au__amount + $c__uk + $c__au;
        setlocale(LC_MONETARY, 'en_US');
        $converted__total = utf8_encode("$" . sprintf('%0.2f', $converted__total));
    }
    //print "<h1>$converted__total</h1>";
    return $converted__total;
}

# NOTE: Get user_id from Facebook ID
function GetUserIdForFBToken($user_token) {
	// Execute the query: SELECT user_id FROM user_social_link WHERE user_token = <user_token>
	// Return the user_id or null if none found.
	global $DB;
	$S = "SELECT * FROM users WHERE facebook_id = ?";
	$Q = $DB->prepare($S);
	$Q->execute(array($user_token));
	//print "<p>SQL: $S</p>";
	//print "<p>Token: $user_token</p>";
	if($Q->rowCount() != null) {
		$D = $Q->fetchObject();
		//print "<p>Returning My ID: $D->id</p>";
		return $D;
	} else {
		//print "<p>Returning NULL (first time)</p>";
		return null;
	}
}

# NOTE: Get user_id from Google ID
function GetUserIdForGoogleToken($user_token) {
	// Execute the query: SELECT user_id FROM user_social_link WHERE user_token = <user_token>
	// Return the user_id or null if none found.
	global $DB;
	$S = "SELECT * FROM users WHERE google_id = ?";
	$Q = $DB->prepare($S);
	$Q->execute(array($user_token));
	//print "<p>SQL: $S</p>";
	//print "<p>Token: $user_token</p>";
	if($Q->rowCount() != null) {
		$D = $Q->fetchObject();
		//print "<p>Returning My ID: $D->id</p>";
		return $D;
	} else {
		//print "<p>Returning NULL (first time)</p>";
		return null;
	}
}

# NOTE: Link Facebook ID to User
function LinkFBTokenToUserId($user_token, $user_id){
	// Execute the query: INSERT INTO user_social_link SET user_token = <user_token>, user_id = <user_id>
	global $DB;
	$S = "update users set facebook_id = ? where id = ?";
	$Q = $DB->prepare($S);
	$Q->execute(array($user_token, $user_id));
	//print "<p>$S</p>";
	// Nothing has to be returned
}

# NOTE: Link Google ID to User
function LinkGoogleTokenToUserId($user_token, $user_id){
	// Execute the query: INSERT INTO user_social_link SET user_token = <user_token>, user_id = <user_id>
	global $DB;
	$S = "update users set google_id = ? where id = ?";
	$Q = $DB->prepare($S);
	$Q->execute(array($user_token, $user_id));
	//print "<p>$S</p>";
	// Nothing has to be returned
}

/* Unrelated to social login */

# NOTE: Print/Echo page content from CMS feature in live__node__data
function print__page($pid) {
	global $DB;
	$S = "SELECT ax FROM live__node__data WHERE aim = 42 AND id = ?";
	$Q = $DB->prepare($S);
	$Q->execute(array($pid));
	if($Q->rowCount() != NULL) {
		$D = $Q->fetchObject();
		$D->ax = str_replace('ÃÃÃ', "'", $D->ax);
		$D->ax = str_replace('ï¿½ï¿½ï¿½', "'", $D->ax);
		print $D->ax;
	} else {
		print "TEST";
	}
}

# NOTE: Print/Echo (right) page content from CMS feature in live__node__data
function print__page__right($pid) {
	global $DB;
	$S = "SELECT * FROM live__node__data WHERE aim = 42 AND id = ?";
	$Q = $DB->prepare($S);
	$Q->execute(array($pid));
	if($Q->rowCount()!= NULL) {
		$D = $Q->fetchObject();
		$D->bx = stripslashes($D->bx);
		$D->bx = str_replace('ÃÃÃ', "'", $D->bx);
		print "$D->bx";
	} else {
		print "TEST";
	}
}

# NOTE: Get FB Profile Image from FB ID
function GetFBProfileImage($user_token, $user_id) {
	global $DB;
	$S = "update users set image_url = ?, social_url = ? where id = ?";

	$Q = $DB->prepare($S);
	$Q->execute(array("http://graph.facebook.com/".$user_token."/picture?width=150&height=150", "http://graph.facebook.com/".$user_token."/picture?width=150&height=150", $user_id));
}

# NOTE: Get Google Profile Image from Google ID (Don't think it works, image being found a different way already)
function GetGoogleProfileImage($user_token, $user_id) {
	$data = json_decode(file_get_contents("https://www.googleapis.com/plus/v1/people/". $user_token ."?fields=image&key=" . Config::googleKey()));# NOTE: Config Google Key
	$data->image->url = str_replace("?sz=50", "?sz=150", $data->image->url);

	global $DB;
	$S = "update users set image_url = ?, social_url = ? where id = ?";

	$Q = $DB->prepare($S);
	$Q->execute(array($data->image->url, $data->image->url, $user_id));
}