<?php

class Env {
    private static $basePath = null;
    private static $baseUrl = null;
    private static $ssl = null;
    private static $protocol = null;
    private static $configPath = null;
    private static $vendorPath = null;
    private static $urlHttp = null;
    private static $urlHttps = null;
 
    private static function getEnv() {
        self::$basePath = $_SERVER['DOCUMENT_ROOT'] . '/';
        self::$ssl = empty($_SERVER['HTTPS']) ? false : ($_SERVER['HTTPS'] == 'on') ? true : (isset($_SERVER['HTTP_X_HTTPS']) && strtolower($_SERVER['HTTP_X_HTTPS']) == 'on') ? true : false;
        $s = (self::$ssl) ? 's' : '';
        self::$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
        $protocol = substr(strtolower(self::$protocol), 0, strpos(strtolower(self::$protocol), '/')) . $s;
        $port = ($_SERVER['SERVER_PORT'] == '80') ? '' : (':' . $_SERVER['SERVER_PORT']);
        $uri = $protocol . '://' . $_SERVER['SERVER_NAME'] . $port . str_replace(array('\/', '//', '/'), '/', dirname($_SERVER['PHP_SELF']) . '/');
        $segments = explode('?', $uri, 2);
        self::$baseUrl = str_replace(['/control', '/scripts/login-with-twitter'] , '', $segments[0]);
        self::$urlHttp = str_replace('https://' , 'http://', $segments[0]) . ltrim($_SERVER['REQUEST_URI'] ,'/');
        self::$urlHttps = str_replace('http://' , 'https://', $segments[0]) . ltrim($_SERVER['REQUEST_URI'] ,'/');
        self::$configPath = self::$basePath . 'config/';
        self::$vendorPath = self::$basePath . 'vendor/';
    }
 
    public static function basePath() {
        if (is_null(self::$basePath)) { self::getEnv(); }
        return self::$basePath;
    }
 
    public static function baseUrl() {
        if (is_null(self::$baseUrl)) { self::getEnv(); }
        return self::$baseUrl;
    }
 
    public static function urlHttp() {
        if (is_null(self::$urlHttp)) { self::getEnv(); }
        return self::$urlHttp;
    }
 
    public static function urlHttps() {
        if (is_null(self::$urlHttps)) { self::getEnv(); }
        return self::$urlHttps;
    }
 
    public static function ssl() {
        if (is_null(self::$ssl)) { self::getEnv(); }
        return self::$ssl;
    }
 
    public static function protocol() {
        if (is_null(self::$protocol)) { self::getEnv(); }
        return self::$protocol;
    }
 
    public static function configPath() {
        if (is_null(self::$configPath)) { self::getEnv(); }
        return self::$configPath;
    }
 
    public static function vendorPath() {
        if (is_null(self::$vendorPath)) { self::getEnv(); }
        return self::$vendorPath;
    }
}