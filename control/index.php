<?php

require_once(dirname(__DIR__) . '/includes/includes.php');

# Make safe
$f = isset($_REQUEST['f'])?$_REQUEST['f']:null;
$a = isset($_REQUEST['a'])?$_REQUEST['a']:null;
$p = isset($_REQUEST['p'])?$_REQUEST['p']:null;

# If page has been submitted, restrict unauthorized user to core
if($f != NULL && $a != NULL) { 
	if(isset($_SESSION['control__user__allowed']) && $_SESSION['control__user__allowed'] == 1) {
		include("sections/$f/$a");
	} else {
		include("sections/authenticate/$a");
	}
}
?>
<!DOCTYPE html>
<?php
    
    # NOTE: Stripe '@' from twitter handles in live__node__data table

    /*$S = "SELECT * FROM live__node__data";
    $Q = $DB->prepare($S);
    $Q->execute();
    for($cc = 0; $cc < $Q->rowCount(); $cc++) {
        $D = $Q->fetchObject();
        
        $S2 = "UPDATE FROM live__node__data SET cv = ? WHERE id = ? LIMIT 1";
        $Q2 = $DB->prepare($S2);
        $Q2->execute(array(str_replace('@', '', $D->cv), $D->id));
        
    }*/

?>
<html>
<head>

	<title></title>
	<meta charset="UTF-8" />
	<meta name="viewport" id="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src="scripts/equalheights.min.js"></script>
	<script src="scripts/sortable/jquery.sortable.js"></script>	
	<script>
		$(function() {
			
			$('.sortable').sortable();

/*
			$('.sortable').sortable().bind('sortupdate', function() {			
				alert('Ready to send...');
			});
*/

			// Highlight the sub nav page clicked on
			
			function getUrlParameter(sParam) {
			    var sPageURL = window.location.search.substring(1);
			    var sURLVariables = sPageURL.split('&');
			    for (var i = 0; i < sURLVariables.length; i++) {
			        var sParameterName = sURLVariables[i].split('=');
			        if (sParameterName[0] == sParam) {
			            return sParameterName[1];
			        }
			    }
			}  

			
			var the__page = getUrlParameter('p');			
			var subnav__url = $('a[href*="' + the__page + '"]');
			$(subnav__url).addClass('on');
			
			
			var folder = 'li.folder__<?php print $_GET['f']; ?>';	
			var sub = $('#subnav ul').detach();
			$(folder).append(sub);
			
			// Confirm delete
			
			
			$('.confirm').click(function() {
				var answer = confirm('Are you sure?');
				return answer;
			});

			
			// Table helper, count dt, and equally space
			
			
			$('dl.table__dt').each(function() {	
				var dt__num = $(this).find('dt').length;
				// alert(dt__num);
				$('dl.table__dt').addClass('amount' + dt__num);
				$('dl.table__dd').addClass('amount' + dt__num);

			});

		});

		$(window).bind("load", function() {
			$('#page, #nav').equalHeights();
		});

	</script>

	<link rel="stylesheet" href="style/default.css" />
<!--
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="clean.css" />
-->

</head>
<body>

<?php

# Check the tables exist first, the connect file should be present through bridge.php
$check__s = "SELECT id FROM live__node__core LIMIT 1";
$check__q = $DB->query($check__s);

if($check__q->rowCount() != NULL) {

	# Check if logged in
	if(isset($_SESSION['control__user__allowed']) && $_SESSION['control__user__allowed'] == 1) {
		# Registered
		print '<div id="frame">';
		print '<div id="banner"><h1>Content Management System</h1></div>';
		print '<div id="inner">';

		# Privilage based navigation
		$navigation__s = "SELECT two.id AS uid, two.aim AS twoaim, six.id AS sixid, six.aim AS sixaim, six.av as access,
					six.ak, six.bk, four.id, four.av AS name, four.bv AS folder, four.cv AS active, four.dv AS icon
		FROM live__node__core AS two
		INNER JOIN live__node__core AS six ON two.id = six.ak
		INNER JOIN live__node__core AS four ON six.bk = four.id
		WHERE two.id = ? AND six.av = 'user__has__access' ORDER BY four.av ASC";
		$navigation__q = $DB->prepare($navigation__s);
		$navigation__q->execute(array($_SESSION['control__user__id']));

		print '<div id="nav">';
		print '<ul><li><a href="?"';
		if($f == '') { print 'class="on"'; } else { print ''; }
		print '><img src="sections/welcome/house.png">Home</a></li>';

		for($na = 0; $na < $navigation__q->rowCount(); $na++) {
			$navigation__d = $navigation__q->fetchObject();
			if($navigation__d->folder == $f) { $nav__class = 'on'; } else { $nav__class = ''; }
			print "<li class='folder__$navigation__d->folder'><a href='index.php?f=$navigation__d->folder' class='$nav__class'><img src='sections/$navigation__d->folder/$navigation__d->icon'>$navigation__d->name</a></li>";
		}

		print '<li><a href="?f=authenticate&a=logout.php" class="confirm"><img src="sections/welcome/door.png">Logout</a></li></ul>';
		print '</div>';

		print '<div id="page">';
		print '<div id="content">';
		print '<div id="subnav">'; @include("sections/$f/subnav.php"); print '</div>';

		if ($f == NULL) { 
	
			include("sections/welcome/home.php"); 
	
		} else { 
	
			if($f != NULL && $p == NULL) { 
	
				include("sections/$f/index.php"); 
	
			} else { 
	
				include("sections/$f/$p"); 
	
			} 
		}

		print '</div>';

	} else {
		# login
		print '<div id="frame" class="small">';
		print '<div id="banner"><h1>Login</h1></div>';
		print '<div id="inner">';
		print '<div id="content">';
		print '<h2>Welcome</h2>';
		print '<p>Enter your username and password, your IP address (' . $_SERVER["REMOTE_ADDR"] . ') will be logged.</p>';

		if(!empty($_REQUEST['msg'])) { print '<p><strong> ' . urldecode($_REQUEST['msg']) . '</strong></p>'; }

		print '<form action="index.php" method="post">';
		print '<fieldset>';
		print '<dl><dt>Username:</dt><dd><input type="text" name="username" /></dd></dl>';
		print '<dl><dt>Password:</dt><dd><input type="password" name="password" /></dd></dl>';
		print '</fieldset>';
		print '<input type="hidden" name="f" value="authenticate" />';
		print '<input type="hidden" name="a" value="login.php" />';
		print '<p><input type="submit" name="submit" value="Login" /></p>';
		print '</form>';	
		
	}

} else {

# Install tables

	print '<div id="frame" class="small">';
	print '<div id="banner"><h1>Install Tables</h1></div>';
	print '<div id="inner">';
	print '<div id="content">';
	print '<h2>Install MYSQL Tables</h2>';
	print '<p>Well done, connect file found. All we need to do is create the tables and we are done!</p>';
	print '<form action="index.php" method="post">';
	print '<input type="hidden" name="f" value="authenticate" />';
	print '<input type="hidden" name="a" value="setup.php" />';
	print '<p><input type="submit" name="submit" value="Install" /></p>';
	print '</form>';


}




# Github versioning footer (later)


print '</div>';
print '</div>';
print '<div id="footer">';
print 'Running in <strong>' . Config::env() . '</strong> mode';
print '</div>';
print '</div>';
print '</body>';
print '</html>';




# Common functions


# This function should stop SQL Injection

function sqlSafe($field) {
	$field = trim(addslashes($field));
	$field = preg_replace("/'/", "_", $field);
	$field = preg_replace("/;/", "_", $field);
	$field = preg_replace("/=/", "_", $field);
	return $field;
};

//include_once 'sections/authenticate/setup.php';
