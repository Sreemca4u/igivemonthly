<?php

require_once '../bridge.php';
require_once 'sections/authenticate/setup.php';

$DB->query('
  create table users (id int not null unique auto_increment, email varchar(255) not null unique,
   first_name varchar(255), last_name varchar(255), password varchar(255), profile text, status varchar(255),
    gender varchar(255), facebook_id varchar(255), google_id varchar(255), image_url varchar(255),
    created_at timestamp not null, updated_at timestamp not null)');

$DB->query('insert into users (select null, cv, av, bv, dv, ax, ev, \'Male\',
  bx, bx from live__node__data where aim = 18 group by email)');

$DB->query('
  create table emails (id int not null unique auto_increment, user_id int, email varchar(255) not null,
   charity_name varchar(255) not null, amount float(10, 2) not null, when_donated datetime not null,
   reference varchar(255), original_text longtext)');

$DB->query('
  create table donations (id int not null unique auto_increment, user_id int not null, charity_id int not null, email varchar(255) not null,
   charity_name varchar(255) not null, amount float(10, 2) not null, when_donated datetime not null,
   reference varchar(255), via varchar(255))');

$DB->query('create table charities (id int not null unique auto_increment, name varchar(255) not null unique,
  image_name varchar(255), number varchar(10), website varchar(255), description text, twitter varchar(255))');

$DB->query('insert into charities (name, image_name, number, website, description, twitter)
  (select av, dv, ev, bv, ax, cv from live__node__data where aim = 14)');

$DB->query('create table charity_user (charity_id int not null, user_id int not null, primary key (charity_id, user_id))');