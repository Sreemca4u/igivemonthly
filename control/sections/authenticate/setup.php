<?php

/****
 * TODO : FIX IN HERE FOR PREPARED QUERIES
 */
/*
$table__array = array(TBC, TBD, TBL);

for($t = 0; $t < count($table__array); $t++) {
	
	$DB->query("CREATE TABLE `$table__array[$t]`(
	`id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary unique id',`aim` int(11) DEFAULT NULL COMMENT 'Group identifier',
	`av` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data', `bv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data',
	`cv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data', `dv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data',
	`ev` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data', `fv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data',
	`gv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data', `hv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data',
	`iv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data', `jv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data',
	`kv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data', `lv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data',
	`mv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data', `nv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data',
	`ov` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data', `pv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data',
	`qv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data', `rv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data',
	`sv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data', `tv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data',
	`uv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data', `vv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data',
	`wv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data', `xv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data',
	`yv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data', `zv` varchar(255) DEFAULT NULL COMMENT 'Varchar for small data',
	`ai` int(11) DEFAULT NULL COMMENT 'Integer, handy for ordering', `bi` int(11) DEFAULT NULL COMMENT 'Integer, handy for ordering',
	`ci` int(11) DEFAULT NULL COMMENT 'Integer, handy for ordering', `di` int(11) DEFAULT NULL COMMENT 'Integer, handy for ordering',
	`ei` int(11) DEFAULT NULL COMMENT 'Integer, handy for ordering',
	`ap` decimal(10,2) DEFAULT NULL COMMENT 'Decimal, handy for prices', `bp` decimal(10,2) DEFAULT NULL COMMENT 'Decimal, handy for prices',
	`cp` decimal(10,2) DEFAULT NULL COMMENT 'Decimal, handy for prices', `dp` decimal(10,2) DEFAULT NULL COMMENT 'Decimal, handy for prices',
	`ep` decimal(10,2) DEFAULT NULL COMMENT 'Decimal, handy for prices',
	`ax` text COMMENT 'Text for long data/information', `bx` text COMMENT 'Text for long data/information',
	`cx` text COMMENT 'Text for long data/information', `dx` text COMMENT 'Text for long data/information',
	`ex` text COMMENT 'Text for long data/information', `fx` text COMMENT 'Text for long data/information',
	`gx` text COMMENT 'Text for long data/information',
	`ad` datetime DEFAULT NULL COMMENT 'Date and time fields', `bd` datetime DEFAULT NULL COMMENT 'Date and time fields',
	`cd` datetime DEFAULT NULL COMMENT 'Date and time fields', `dd` datetime DEFAULT NULL COMMENT 'Date and time fields',
	`ed` datetime DEFAULT NULL COMMENT 'Date and time fields',
	`ae` enum('0','1') CHARACTER SET latin1 DEFAULT '0' COMMENT 'Can be only 0 or 1', `be` enum('0','1') CHARACTER SET latin1 DEFAULT '0' COMMENT 'Can be only 0 or 1',
	`ce` enum('0','1') CHARACTER SET latin1 DEFAULT '0' COMMENT 'Can be only 0 or 1', `de` enum('0','1') CHARACTER SET latin1 DEFAULT '0' COMMENT 'Can be only 0 or 1',
	`ee` enum('0','1') CHARACTER SET latin1 DEFAULT '0' COMMENT 'Can be only 0 or 1',
	`ab` longblob COMMENT 'For holding file/image data', `bb` longblob COMMENT 'For holding file/image data',
	`cb` longblob COMMENT 'For holding file/image data', `db` longblob COMMENT 'For holding file/image data',
	`eb` longblob COMMENT 'For holding file/image data',
	`ak` int(11) DEFAULT NULL COMMENT 'Foreign key field, handy for joins', `bk` int(11) DEFAULT NULL COMMENT 'Foreign key field, handy for joins',
	`ck` int(11) DEFAULT NULL COMMENT 'Foreign key field, handy for joins', `dk` int(11) DEFAULT NULL COMMENT 'Foreign key field, handy for joins',
	`ek` int(11) DEFAULT NULL COMMENT 'Foreign key field, handy for joins',
	PRIMARY KEY(`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Single table build, designed to handle most data types.';");
}

# Add date installed

$DB->query("INSERT INTO live__node__core (aim, ad) VALUES ('1', NOW())");
	

# Add user


$salt__password = md5(SALT . 'thorsday');

$user__q = $DB->query("INSERT INTO live__node__core (aim, av, bv, cv, dv, ev, fv) VALUES ('2', 'Andrew', 'Webb', 'awebbdesign', '$salt__password', 'not__ip__strict', 'user__active')");
$user__m = $DB->lastInsertId();


# Add navigation and privillages


$nav__q = $DB->query("INSERT INTO live__node__core (aim, av, bv, cv, dv, ax) VALUES ('3', 'Settings', 'settings', 'droplet__active', 'wrench.png', 'Settings management system. VIP.')");
$nav__m = $DB->lastInsertId();
$privillage__q = $DB->query("INSERT INTO live__node__core (aim, ak, bk, av) VALUES ('4', '$user__m', '$nav__m', 'user__has__access')");


$nav__q = $DB->query("INSERT INTO live__node__core (aim, av, bv, cv, dv, ax) VALUES ('3', 'Users', 'users', 'droplet__active', 'vcard.png', 'Users management system. VIP.')");
$nav__m = $DB->lastInsertId();
$privillage__q = $DB->query("INSERT INTO live__node__core (aim, ak, bk, av) VALUES ('4', '$user__m', '$nav__m', 'user__has__access')");


$nav__q = $DB->query("INSERT INTO live__node__core (aim, av, bv, cv, dv, ax) VALUES ('3', 'Shape', 'shape', 'droplet__active', 'bricks.png', 'Shape. For generating unique aim numbers VIP.')");
$nav__m = $DB->lastInsertId();
$privillage__q = $DB->query("INSERT INTO live__node__core (aim, ak, bk, av) VALUES ('4', '$user__m', '$nav__m', 'user__has__access')");

*/

# Themes

header('location: index.php');