<?php

# Collect data, clean

$username = $_POST['username'];
$password = $_POST['password'];
$password = md5(SALT . $password);

# Search user
$search__user__s = "SELECT * FROM live__node__core WHERE aim = '2' AND cv = ? AND dv = ? LIMIT 1";
$search__user__q = $DB->prepare($search__user__s);
$search__user__q->execute(array($username, $password));

if($search__user__q->rowCount() != NULL) {
    $search__user__d = $search__user__q->fetchObject();

    # IP restricted
    if($search__user__d->ev == 'ip__strict') {
    
        $ipS = "SELECT ID FROM live__node__core WHERE NODE = 7 AND AK = ? AND AV = ? LIMIT 1";
        $ipQ = $DB->prepare($ipS);
        $ipQ->execute(array($uD->id, $_SERVER["REMOTE_ADDR"]));
    
        if($ipQ->rowCount() != 1) {
        
            $msg = urlencode("Sorry IP Restricted, you cannot login from this location");
            header("location: index.php?msg=$msg");
            exit();
        
        } else {
        
            $_SESSION['control__user__allowed'] = 1;  
            $_SESSION['control__user__id'] = $search__user__d->id;
            $_SESSION['control__user__name'] = $search__user__d->av;      
            session_write_close();
        
        }

    } else {

		$_SESSION['control__user__allowed'] = 1;  
		$_SESSION['control__user__id'] = $search__user__d->id;
		$_SESSION['control__user__name'] = $search__user__d->av;       
        session_write_close();

    }
    
    header("location: index.php");
     
} else {
    $msg = urlencode("Login not recognised");
    //print "<p>$search__user__s</p>";
    header("location: index.php?msg=$msg");
    exit();
}