<h2>Insert User</h2>

		
<p>Insert a user before configuring their privileges</p>


<form action="index.php" method="post">


	<fieldset>
		<dl><dt>First name:</dt><dd><input type="text" name="fname" id="fname" data-mini="true" /></dd></dl>
		<dl><dt>Last name:</dt><dd><input type="text" name="lname" id="lname" data-mini="true" /></dd></dl>
		<dl><dt>Username:</dt><dd><input type="text" name="username" id="username" data-mini="true"  value="<?php print $uD->cv; ?>" /></dd></dl>
		<dl><dt>Password:</dt><dd><input type="text" name="password" id="password" data-mini="true"  /></dd></dl>
	</fieldset>

	
	<fieldset>
		<legend>User Active or Not Active</legend>
		<dl><dd><input type="radio" name="active" value="user__active" /> User Active</dd></dl>
     	<dl><dd><input type="radio" name="active" value="user__not__active" /> User Not Active</dd></dl>
	</fieldset>

	
	<fieldset>
		<legend>User IP Restricted</legend>
		<dl><dd><input type="radio" name="strict" value="ip__strict" /> Restricted</dd></dl>
     	<dl><dd><input type="radio" name="strict" value="not__ip__strict" /> Not Restricted</dd></dl>
	</fieldset>

	
		<input type="hidden" name="f" value="users" />
		<input type="hidden" name="a" value="insert__user__post.php" />

	
	<p><input type="submit" name="submit" value="Insert User" /></p>

	
</form>
