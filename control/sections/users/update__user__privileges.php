<h2>Users Privileges</h2>


<?php


$id = sqlSafe($_REQUEST['id']);


$user__s = "SELECT id, av, bv, cv, dv, ev, fv FROM live__node__core WHERE id = ? AND aim = '2'";
$user__q = $DB->prepare($user__s);
$user__q->execute(array($id));
$user__d = $user__q->fetchObject();


print "<p>Here you can manage the user's privileges for <strong>$user__d->av $user__d->bv</strong></p>";
		



# Setup already? if not, install


$nav__s = "SELECT id FROM live__node__core WHERE aim = '3'";
$nav__q = $DB->query($nav__s);


for($i = 0; $i < $nav__q->rowCount(); $i++) {

	
	$nav__d = $nav__q->fetchObject();

	
	

# Search for the privillage


	$priv__s = "SELECT id, av, ak, bk FROM live__node__core WHERE aim = '4' AND ak = ? AND bk = ?";
	$priv__q = $DB->prepare($priv__s);
	$priv__q->execute(array($id, $nav__d->id));
	if($priv__q->rowCount() == NULL) {
		$DB->prepare("INSERT INTO live__node__core (aim, av, ak, bk) VALUES ('4', 'user__has__no__access', ?, ?)")
			->execute(array($id, $nav__d->id));
	};

	
}



	
# Run the query for real


$actual__priv__s = "SELECT two.id AS uid, two.aim AS twoaim, 
six.id AS sixid, six.aim AS sixaim, six.av as access, six.ak, six.bk, 
four.id, four.av AS name, four.bv AS folder, four.cv AS active, four.dv AS icon, four.ax as description
FROM live__node__core AS two
INNER JOIN live__node__core AS six ON two.id = six.ak
INNER JOIN live__node__core AS four ON six.bk = four.id
WHERE two.id = ? ORDER BY four.av ASC";


$actual__priv__q = $DB->prepare($actual__priv__s);
$actual__priv__q->execute(array($id));


if($actual__priv__q->rowCount() != NULL) {

	
	print '<form id="privileges"><fieldset>';

	
	for($x = 0; $x < $actual__priv__q->rowCount(); $x++) {

		
		$actual__priv__d = $actual__priv__q->fetchObject();


		@$va = "va__" . "$x"; 
		@$$va;
		@$vnid = "vnid__" . "$x"; 
		@$$vnid;		

		
		print '<dl class="strip">';

		
		if($actual__priv__d->access == 'user__has__access') { 
			print "<dd>";
			print "<input type='checkbox' name='$va' checked='checked' value='user__has__access' /> ";
			print "<b>$actual__priv__d->name</b> <em>$actual__priv__d->description</em>";
			print "<input type='hidden' name='$vnid' value='$actual__priv__d->sixid' />";
			print "</dd>"; 	
		} else { 
			print "<dd>";
			print "<input type='checkbox' name='$va' value='user__has__access' />"; 
			print "<b>$actual__priv__d->name</b> <em>$actual__priv__d->description</em> ";
			print "<input type='hidden' name='$vnid' value='$actual__priv__d->sixid' />";
			print "</dd>"; 
		}

		
		print '</dl>';

	
	}	

	
	print "<input type='hidden' name='f' value='users' />";
	print "<input type='hidden' name='a' value='update__user__privileges__post.php' />";
	print "<input type='hidden' name='l' value='$x' />";
	print "<input type='hidden' name='uid' value='$id' />";
	print "</fieldset>";
	print '<p><input type="submit" name="submit" value="Update User"  /></p>';
	print "</form>";

	
} else {

	
	print "<p>No Navigation, or an error occurred.</p>";

	
}


?>