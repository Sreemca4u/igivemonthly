<?php


$id = sqlSafe($_REQUEST['id']);


$users__s = "SELECT id, av, bv, cv, dv, ev, fv FROM live__node__core WHERE id = ? AND aim = '2'";
$users__q = $DB->prepare($users__s);
$users__q->execute(array($id));
$users__d = $users__q->fetchObject();


?>


<h2>Update User</h2>


<form action="index.php" method="post">
	
	
	<fieldset>
		<dl><dt>First name:</dt><dd><input type="text" name="fname" id="fname" value="<?php print $users__d->av; ?>" /></dd></dl>
		<dl><dt>Last name:</dt><dd><input type="text" name="lname" id="lname" value="<?php print $users__d->bv; ?>" /></dd></dl>
		<dl><dt>Username:</dt><dd><input type="text" name="username" value="<?php print $users__d->cv; ?>" /></dd></dl>
		<dl><dt>Password (leave blank to not change):</dt><dd><input type="text" name="password" /></dd></dl>
	</fieldset>
	
	
	<fieldset>
		<legend>User IP Restricted</legend>
		<dl><dd><input type="radio" name="strict" value="not__ip__strict" <?php if($users__d->ev == 'not__ip__strict') { print "checked='checked'"; }; ?> /> Not Restricted</dd></dl>
		<dl><dd><input type="radio" name="strict" value="ip__strict" <?php if($users__d->ev == 'ip__strict') { print "checked='checked'"; }; ?> /> Restricted</dd></dl>
	</fieldset>
	
	
	<fieldset>
		<legend>User Active or Not Active</legend>
		<dl><dd><input type="radio" name="active" value="user__active" <?php if($users__d->fv == 'user__active') { print "checked='checked'"; }; ?> /> User Active</dd></dl>
     	<dl><dd><input type="radio" name="active" value="user__not__active" <?php if($users__d->fv == 'user__not__active') { print "checked='checked'"; }; ?> /> User Not Active</dd></dl>
	</fieldset>
	
	
		<input type="hidden" name="id" value="<?php print $id; ?>" />
		<input type="hidden" name="f" value="users" />
		<input type="hidden" name="a" value="update__user__post.php" />
	
	
	<p><input type="submit" name="submit" value="Update User"  /></p>
	
	
</form>


<hr />	
		
		
<h2>IP Addresses</h2>


<p>If the user is strict, then they will need to have IP addresses added to enable them to login from their location.</p>


<p><strong>IP address for this computer: <?php print $_SERVER['REMOTE_ADDR']; ?></strong></p>


<form action="index.php" method="post">
	
	
	<fieldset>
		<dl><dt>IP Address:</dt><dd><input type="text" name="ip" /></dd></dl>
		<dl><dt>Description:</dt><dd><input type="text" name="description" /></dd></dl>
	</fieldset>
	
	
	<input type="hidden" name="f" value="users" />
	<input type="hidden" name="a" value="users__ip__post.php" />
	<input type="hidden" name="id" value="<?php print $id; ?>" />
	<p><input type="submit" name="submit" value="Add IP address for <?php print $users__d->av; ?>" /></p>


</form>


<hr />


<h2>Table of IPs for <?php print "$users__d->av $users__d->bv"; ?></h2>


<?php


$ip__s = "SELECT id, av, bv, ak FROM live__node__core WHERE ak = ? AND aim = '5' ORDER BY av";
$ip__q = $DB->prepare($ip__s);
$ip__q->execute(array($id));


if($ip__q->rowCount() != NULL) {
	
	
	print "<dl class='strip'><dt>IP Address</dt><dt>Description</dt><dt>Delete</dt></dl>";


	for($n = 0; $n < $ip__q->rowCount(); $n++) {
		$ip__d = $ip__q->fetchObject();
		print "<dl class='strip'><dd>$ip__d->av</dd><dd>$ip__d->bv</dd><dd><a href='index.php?f=users&a=users__ip__delete__post.php&id=$ip__d->id&uid=$id'>Delete</a></dd></dl>";
	}
	
	
} else {
	
	
	if($users__d->ev == 'strict') {
	
		print "<p><strong>No IP's. User cannot login</strong></p>";
	
	} else {
	
		print "<p><strong>No IP's</strong></p>";
	
	}
	
	
}

		
		

