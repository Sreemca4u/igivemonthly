<h2>Users</h2>
	
	
<p>Here you can see all the users and configure their privileges</p>
	
				
<?php


$users__s = "SELECT id, av, bv FROM live__node__core WHERE aim = '2' ORDER BY av";
$users__q = $DB->query($users__s);
		
		
if($users__q->rowCount()) {
	
	print '<dl class="table__dt">';
	print '<dt>Selected</dt>';
	print '<dt>Theme</dt>';
	print '<dt>Description</dt>';
	print '</dl>';
	
	
	for ($i = 0; $i < $users__q->rowCount(); $i++) {
	
	
		$users__d = $users__q->fetchObject();
	
	
		print '<dl class="table__dd">';
		print "<dd><a href='?f=users&p=update__user.php&id=$users__d->id'>$users__d->av $users__d->bv</a></dd>";
		print "<dd><a href='?f=users&p=update__user__privileges.php&id=$users__d->id'>Privileges</a></dd>";
		print "<dd><a href='?f=users&a=delete__user__post.php&id=$users__d->id' class='confirm'>Delete</a></dd>"; 
		print '</dl>';


	}
	
	
} else {

	
	print '<p><strong>No users!</strong></p>';


}