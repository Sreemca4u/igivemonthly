<h2>Shape</h2>


<p>Here you can generate unique AIM numbers for your droplets.</p>


<?php




# Loop through the droplets in the core table


$droplets__s = "SELECT * FROM live__node__core WHERE aim = 7";
$droplets__q = $DB->query($droplets__s);


if($droplets__q->rowCount() != NULL) {
	
	
	print '<dl class="table__dt">';
	print '<dt>ID</dt>';
	print '<dt>Name</dt>';
	print '<dt>Description</dt>';
	print '</dl>';
	
	
	for ($i = 0; $i < $droplets__q->rowCount(); $i++) {
	
	
		$droplets__d = $droplets__q->fetchObject();
	
	
		print '<dl class="table__dd">';
		print "<dd>$droplets__d->id</a></dd>";
		print "<dd>$droplets__d->av</a></dd>";
		print "<dd>$droplets__d->ax</dd>"; 
		print '</dl>';


	}
	
	
} else {
	
	print '<p>No unique aim droplets found</p>';
	
}