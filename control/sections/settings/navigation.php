<h2>Navigation</h2>


<p>You can re-order the navigation to suit, or leave it alphabetical.</p>


<dl class="table__dt">
	<dt>Navigation</dt>
	<dt>Folder Name</dt>
	<dt>Active</dt>
</dl>


<?php


$nav__s = "SELECT id, av, bv, cv FROM live__node__core WHERE aim = '3' ORDER BY av";
$nav__q = $DB->query($nav__s);


print '<ul class="sortable">';


for($na = 0; $na < $nav__q->rowCount(); $na++) {
	
	
	$nav__d = $nav__q->fetchObject();
	
	
	print '<li>';
	print '<dl class="table__dd">';
	print "<dd>$nav__d->av</dd>";
	print "<dd>$nav__d->bv</dd>";
	
	
	if($nav__d->cv == 'droplet__active') { print "<dd><img src='style/icons/accept.png' alt='link' /></dd>"; } else { print "<dd><img src='style/icons/cancel.png' alt='link' /></dd>"; }
	

	print '</dl>';
	print '</li>';
	
	
}


print '</ul>';