<?php

$id = $_REQUEST['id'];

$S = "UPDATE sponsorships SET expires_time = ? WHERE charity_id = ? ORDER BY expires_time DESC LIMIT 1";
$Q = $DB->prepare($S);
$Q->execute(array(date('Y-m-d H:i:s', time() - 10), $id));

header('location: index.php?f=igm__charities&p=update__charity.php&id=' . $id);
