<h2>Charities</h2>
<p>Here you can see all the charities.</p>
<?php

$S = "SELECT * FROM charities where status_id = 2 ORDER BY name ASC";
$Q = $DB->query($S);


print '<dl class="table__dt">';
print '<dt>Charity</dt>';
print '<dt>Sponsored</dt>';
print '<dt>Donations</dt>';
#print '<dt>Delete</dt>';
print '</dl>';


for($cc = 0; $cc < $Q->rowCount(); $cc++) {


    $D = $Q->fetchObject();


    print '<dl class="table__dd">';
    print "<dd><a href='index.php?f=igm__charities&p=update__charity.php&id=$D->id'>$D->name</a></dd>";

# Sponsored
    $sponsored__s = "SELECT * FROM sponsorships WHERE charity_id = ? order by expires_time desc limit 1";
    $sponsored__q = $DB->prepare($sponsored__s);
    $sponsored__q->execute(array($D->id));

    if($sponsored__q->rowCount() != NULL) {
        $sponsored__d = $sponsored__q->fetchObject();
        if (strtotime($sponsored__d->expires_time) < time()) {
            echo '<dd>Expired: <span style="color:#cc0000;">' . date('H:i:s, d/m/Y', strtotime($sponsored__d->expires_time)) . '</span></dd>';
        } else {
            echo '<dd>Expires: <span style="color:#0000cc;">' . date('H:i:s, d/m/Y', strtotime($sponsored__d->expires_time)) . '</span></dd>';
        }
    } else {
        print '<dd>No</dd>';

    }



# Donations
    $sponsored__s = "SELECT sum(amount) as total FROM donations WHERE charity_id = ?";
    $sponsored__q = $DB->prepare($sponsored__s);
    $sponsored__q->execute(array($D->id));

    if($sponsored__q->rowCount() != NULL) {
        $sponsored__d = $sponsored__q->fetchObject();
        print "<dd>&pound; ".number_format($sponsored__d->total, 2)."</dd>";
    } else {
        print '<dd></dd>';

    }


    #print "<dd><a href='index.php?f=igm__charities&a=delete__post.php&id=$D->id' class='confirm'>Delete</a></dd>";
    print '</dl>';

    // print "<p>$D->ax</p>";


}