<?php

$id = $_REQUEST['id'];
$status_id = $_REQUEST['status_id'];
$name = $_REQUEST['name'];
$url = $_REQUEST['url'];
$twitter = str_replace(['@', 'https://twitter.com/'], '', $_REQUEST['twitter']);
$text = $_REQUEST['text'];
$contact_name = $_REQUEST['contact_name'];
$contact_job_title = $_REQUEST['contact_job_title'];
$contact_telephone = $_REQUEST['contact_telephone'];
$contact_email = $_REQUEST['contact_email'];


$S = "UPDATE charities SET name = ?, status_id = ?, website = ?, twitter = ?, description = ?, contact_name = ?, contact_job_title = ?, contact_telephone = ?, contact_email = ? WHERE id = ? LIMIT 1";
$Q = $DB->prepare($S);
$Q->execute(array($name, $status_id, $url, $twitter, $text, $contact_name, $contact_job_title, $contact_telephone, $contact_email, $id));


header('location: index.php?f=igm__charities');
