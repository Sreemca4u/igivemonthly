<?php

$id = $_REQUEST['id'];

$file__to__upload = $_FILES['file__to__upload']['name'];
$safe__file__to__upload = $_FILES['file__to__upload']['tmp_name'];
$stamp = mktime();
$extension = pathinfo($file__to__upload, PATHINFO_EXTENSION);
$new__name = $stamp . "__" . $_FILES['file__to__upload']['name'];


$target__path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/charity-logos/';
$target__file =  str_replace('//','/', $target__path) . $new__name;


# Check the height and width
# If it is not the width and height assigned delete image and folder


$size = getimagesize($safe__file__to__upload);
$maxWidth = 200;
$maxHeight = 100;


if ($size[0] <= $maxWidth && $size[1] <= $maxHeight) {


	move_uploaded_file($safe__file__to__upload, $target__file);


	$update__s = "UPDATE charities SET image_name = ? WHERE id = ? LIMIT 1";
	$update__q = $DB->prepare($update__s);
	$update__q->execute(array($new__name, $id));

	// print "<p>Size: $size[0] / $size[1]</p>";


}


header("location: index.php?f=igm__charities&p=view__submitted.php&id=$id");
