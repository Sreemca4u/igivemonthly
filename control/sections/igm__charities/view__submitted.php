<?php

$id = $_REQUEST['id'];

$S = "SELECT * FROM submitted_charities WHERE status_id = 1 AND id = ? ORDER BY created_at ASC";
$Q = $DB->prepare($S);
$Q->execute(array($id));
$D = $Q->fetchObject();

print "<h2>View submitted charity: $D->name</h2>";
print "<p>Here you can modify the suggested charity details and accept/decline the suggestion.</p>";
print "<p><small><i>- Declining a suggested charity will add it to your archive.</i></small><br />";
print "<small><i>- Accepting a suggested charity will make it live immediatley and add it to your archive.</i></small><br />";

print '<form action="index.php" method="post">';
print '<fieldset>';
print '<dl><dt>Charity Name</dt><dd><input type="text" name="name" value="' . $D->name . '" /></dd></dl>';
print '<dl><dt>Charity Website *</dt><dd><input type="text" name="website" value="' . $D->website . '" /></dd></dl>';
print '<dl><dt>Charity Number</dt><dd><input type="text" name="charity__number" value="' . $D->number . '" /></dd></dl>';
print '<dl><dt>Twitter Page</dt><dd><input type="text" name="twitter_page" value="' . $D->twitter . '" /></dd></dl>';
print '</fieldset>';
print '<fieldset><legend>(not editable)</legend>';
print '<dl><dt>Your Name</dt><dd><input type="text" disabled="disabled" name="person_name" value="' . $D->submitted_name . '" /></dd></dl>';
print '<dl><dt>Your Email Address</dt><dd><input type="text" disabled="disabled" name="person_email" value="' . $D->email . '" /></dd></dl>';
print '</fieldset>';
print '<p><input type="submit" value="Updated suggested charity" /></p>';
print '<input type="hidden" name="f" value="igm__charities" />';
print '<input type="hidden" name="a" value="do__update__suggested__charity.php" />';
print '<input type="hidden" name="id" value="' . $id . '" />';
print '</form>';

print '<hr />';

print '<h2>Upload a logo</h2>';
print '<form action="index.php" method="post" enctype="multipart/form-data">';
print '<fieldset>';
print '<dl><dt>File:</dt><dd><input type="file" name="file__to__upload" id="file__to__upload"></dd></dl>';
print '</fieldset>';
print '<p><input type="submit" value="Upload Logo" /></p>';
print '<input type="hidden" name="f" value="igm__charities" />';
print '<input type="hidden" name="a" value="update__submitted__logo__post.php" />';
print '<input type="hidden" name="id" value="' . $id . '" />';
print '</form>';

if($D->image_name != '') { print "<img src='/uploads/charity-logos/$D->image_name' />"; } else { print "<p>No logo uploaded.</p>"; };

print '<hr />';

print '<h2>Approve/Disaprove</h2>';
print '<p>Here you can approve/disaprove a suggested charity and send a message to the sender ' . $D->submitted_name . ' </p>';
print "<p><small><i>- Remember approving a charity will make it live immediatley and add it to your archive.</i></small></p>";

print '<form action="index.php" method="post">';
print '<fieldset>';
print '<dl><dt>Reason (if declined)</dt><dd><textarea name="text"></textarea></dd></dl>';
print '<dl><dt>Approved?</dt><dd><input type="checkbox" name="approved" value="1" /></dd></dl>';
print '</fieldset>';
print '<p><input type="submit" value="Send response to ' . $D->submitted_name . '" /></p>';
print '<input type="hidden" name="f" value="igm__charities" />';
print '<input type="hidden" name="a" value="do__approval.php" />';
print '<input type="hidden" name="id" value="' . $id . '" />';
print '<input type="hidden" name="name" value="' . $D->submitted_name . '" />';
print '<input type="hidden" name="email" value="' . $D->email . '" />';
print '</form>';