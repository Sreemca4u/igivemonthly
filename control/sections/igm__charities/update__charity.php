<h2>Update Charity</h2>


<p>Here you can update a charity and upload a logo.</p>


<?php

$id = $_REQUEST['id'];
$S = "SELECT * FROM charities WHERE id = ? LIMIT 1";
$Q = $DB->prepare($S);
$Q->execute(array($id));
$D = $Q->fetchObject();

$sponsored__s = "SELECT * FROM sponsorships WHERE charity_id = ? order by expires_time desc limit 1";
$sponsored__q = $DB->prepare($sponsored__s);
$sponsored__q->execute(array($D->id));

if($sponsored__q->rowCount() != NULL) {
    $sponsored__d = $sponsored__q->fetchObject();
    if (strtotime($sponsored__d->expires_time) > time()) {
        ?>
        <form action="index.php" method="post">
            <input type="hidden" name="f" value="igm__charities" />
            <input type="hidden" name="a" value="update__charity__sponsorship.php" />
            <input type="hidden" name="id" value="<?php print $id; ?>" />
            <p><input type="submit" name="submit" value="Unsponsor Charity - <?php echo $D->name; ?>" /> <strong style="color:#cc0000;">Warning, this can't be undone</strong></p>
        </form>
        <hr />
        <?php
    }
}
?>
<form action="index.php" method="post">


	<fieldset>
		<dl><dt>Name:</dt><dd><input type="text" name="name" <?php print "value='$D->name'"; ?> /></dd></dl>
        <dl><dt>Status:</dt><dd><select name="status_id">
        <?php
            $S = "SELECT * FROM charity_status order by id";
            $Q = $DB->prepare($S);
            $Q->execute(array($id));
            for($cc = 0; $cc < $Q->rowCount(); $cc++) {
                $S = $Q->fetchObject();
                $selected = '';
                if ($D->status_id == $S->id) {
                    $selected = ' selected="selected"';
                }
                echo '<option value="' . $S->id . '"' . $selected . '>' . $S->status_name . '</option>';
            }
        ?>
            </select></dd></dl>
        <dl><dt>Sponsored:</dt><dd>
        <?php
        
        if($sponsored__q->rowCount() != NULL) {
            if (strtotime($sponsored__d->expires_time) < time()) {
                echo 'Expired: <span style="color:#cc0000;">' . date('H:i:s, d/m/Y', strtotime($sponsored__d->expires_time)) . '</span>';
            } else {
                echo 'Expires: <span style="color:#0000cc;">' . date('H:i:s, d/m/Y', strtotime($sponsored__d->expires_time)) . '</span>';
            }
        } else {
            print 'No';

        }
        
        
        ?>
            </dd></dl>
		<dl><dt>Website:</dt><dd><input type="text" name="url" <?php print "value='$D->website'"; ?> /></dd></dl>
		<dl><dt>Twitter:</dt><dd><input type="text" name="twitter" <?php print "value='$D->twitter'"; ?> /></dd></dl>
		<dl><dt>Decription:</dt><dd><textarea name="text" rows="21"><?php print "$D->description"; ?></textarea></dd></dl>
        <dl><dt>Contact Name:</dt><dd><input type="text" name="contact_name" <?php print "value='$D->contact_name'"; ?> /></dd></dl>
        <dl><dt>Contact Job Title:</dt><dd><input type="text" name="contact_job_title" <?php print "value='$D->contact_job_title'"; ?> /></dd></dl>
        <dl><dt>Contact Telephone:</dt><dd><input type="text" name="contact_telephone" <?php print "value='$D->contact_telephone'"; ?> /></dd></dl>
        <dl><dt>Contact Email:</dt><dd><input type="text" name="contact_email" <?php print "value='$D->contact_email'"; ?> /></dd></dl>
	</fieldset>

		<input type="hidden" name="f" value="igm__charities" />
		<input type="hidden" name="a" value="update__charity__post.php" />
		<input type="hidden" name="id" value="<?php print $id; ?>" />

	<p><input type="submit" name="submit" value="Update Charity" /></p>


</form>

<hr />

<form action="index.php" method="post" enctype="multipart/form-data">
	<fieldset>
		<dl><dt>File:</dt><dd><input type="file" name="file__to__upload" id="file__to__upload"></dd></dl>
	</fieldset>
	<p><input type="submit" value="Upload Logo" /></p>
	<input type="hidden" name="f" value="igm__charities" />
	<input type="hidden" name="a" value="update__charity__logo__post.php" />
	<input type="hidden" name="id" value="<?php print $id; ?>" />
</form>


<?php if($D->image_name != '') { print "<img src='/uploads/charity-logos/$D->image_name' />"; } else { print "<p>No logo uploaded.</p>"; };