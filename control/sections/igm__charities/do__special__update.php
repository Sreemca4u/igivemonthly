<?php

$loops = $_REQUEST['loops'];

for($n = 0; $n < $loops; $n++) {

    $name = 'name_' . $n;
    $theid = 'id_' . $n;

    $var__name = $_POST[$name];
    $var__id = $_POST[$theid];

    $s = "UPDATE charities SET number = ? WHERE id = ?";
    //print "<p>$s</p>";
    $q = $DB->prepare($s);
    $q->execute(array($var__name, $var__id));

}

header("location: index.php?f=igm__charities&p=special__update.php");