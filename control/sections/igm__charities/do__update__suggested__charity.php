<?php

$id = $_REQUEST['id'];
$name = $_REQUEST['name'];
$website = $_REQUEST['website'];
$charity__number = $_REQUEST['charity__number'];
$twitter = str_replace(['@', 'https://twitter.com/'], '', $_REQUEST['twitter_page']);
$person_name = $_REQUEST['person_name'];
$person_email = $_REQUEST['person_email'];

$S = "UPDATE charities SET name = ?, website = ?, twitter = ?, number = ? WHERE id = ? LIMIT 1";
$Q = $DB->prepare($S);
$Q->execute(array($name, $website, $twitter, $charity__number, $id));

header("location: index.php?f=igm__charities&p=view__submitted.php&id=$id");