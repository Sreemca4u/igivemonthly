<?php

echo '<h2>Submitted Charities</h2>';
#echo '<p>Here you can see all the charities. To see your archive, <a href="index.php?f=igm__charities&p=submitted__archive.php">click here</a></p>';

$S = "SELECT * FROM submitted_charities";
$Q = $DB->query($S);

print '<dl class="table__dt">';
print '<dt>Charity</dt>';
print '<dt>Submitted by</dt>';
print '<dt>Date</dt>';
print '<dt>View</dt>';
#print '<dt>Delete</dt>';
print '</dl>';

for($cc = 0; $cc < $Q->rowCount(); $cc++) {
    $D = $Q->fetchObject();

    print '<dl class="table__dd">';
    print "<dd>$D->name</a></dd>";

    print "<dd>$D->submitted_name</dd>";
    print '<dd>' . date('d/m/Y', strtotime($D->created_at)) . '</dd>';
    print "<dd><a href='?f=igm__charities&p=view__submitted.php&id=$D->id'>View</a></dd>";
    #print "<dd><a href='?f=igm__charities&a=do__delete__submitted.php&id=$D->id' class='confirm'>Delete</a></dd>";
    print '</dl>';
}