<?php

$id = $_REQUEST['id'];
$name = $_REQUEST['name'];
$email = $_REQUEST['email'];
$reason = nl2br(strip_tags(htmlspecialchars($_REQUEST['text'])));

if (isset($_POST['approved'])) {
    $approved = $_REQUEST['approved'];
} else {
    $approved = 0;
}

if ($approved == 1) {

    $message = "Hi $name,<br /><br />
    Thank you for your charity suggestion.<br /><br />
    Your charity has been approved.<br /><br />
    You can also be the first to sponsor your charity by <a href='http://www.igivemonthly.com/pages/charities/browse/'>clicking here</a>.<br /><br />
    Many Thanks<br />
    iGiveMonthly";

    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= "From: info@igivemonthly.com" . "\r\n";

    mail($email, 'IGM - Charity Approval', $message, $headers);

    $archive__s = "UPDATE charities SET status_id = 2 WHERE id = ?";
    $archive__q = $DB->prepare($archive__s);
    $archive__q->execute(array($id));

} else {

    // Get charity
    $suggested__s = "SELECT status_id, submitted_id FROM submitted_charities WHERE id = ? limit 1";
    $suggested__q = $DB->prepare($suggested__s);
    $suggested__q->execute(array($id));
    $suggested__d = $suggested__q->fetchObject();

    $archive__s = "UPDATE charities SET status_id = 3 WHERE id = ?";
    $archive__q = $DB->prepare($archive__s);
    $archive__q->execute(array($id));

    $archive__s = "UPDATE submitted SET reason = ? WHERE id = ?";
    $archive__q = $DB->prepare($archive__s);
    $archive__q->execute(array($reason, $suggested__d->submitted_id));

    $message = "Hi $name,<br /><br />
    Thank you for your charity suggestion.<br /><br />
    Unfortunatly your charity has <strong>not</strong> been approved.<br /><br />
    $reason <br /><br />
    Many Thanks<br />
    I Give Monthly";
    
    if ($suggested__d->status_id == 1) {
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= "From: noreply@igivemonthly.com" . "\r\n";
        mail($email, 'IGM - Charity Disapproval', $message, $headers);
    }
}

header("location: index.php?f=igm__charities&p=view__submitted.php&id=$id");