<?php

/*

- There are some form field names without 2 underscores.
- Need to add security to submitted fields.

*/

# Collect the charity information...

$name = $_REQUEST['name'];
$website = $_REQUEST['website'];
$charity__number = $_REQUEST['charity__number'];
$twitter__page = $_REQUEST['twitter_page'];

# Collect the personal details...

$person__name = $_REQUEST['person_name'];
$person__email = $_REQUEST['person_email'];

# Insert suggested charity into database

$s = "INSERT INTO charities (name, status_id, website, twitter, number) VALUES (?,1,?,?,?)";
$q = $DB->prepare($s);
$q->execute(array($name, $website, $twitter__page, $charity__number));
$m = $DB->lastInsertId();

# Add the person seperate who recommended the charity

$s = "INSERT INTO submitted (charity_id, name, email, reason) VALUES (?, ?, ?, '')";
$q = $DB->prepare($s);
$q->execute(array($m, $person__name, $person__email));

# Send email to Andy Doyle notifying him of the suggested charity

$body_client = "<p>A charity called <strong>''$name'</strong> has been suggested by <strong>''$person__name'</strong>.</p>";
$body_client .= "<p><strong>Suggested charity details.</strong></p>";
$body_client .= "<p>Charity name: $name.<br />";
$body_client .= "Website address: $website.<br />";
$body_client .= "Charity number: $charity__number.<br />";
$body_client .= "Twitter page: $twitter__page.</p>";
$body_client .= "<p><em>Please login to your admin for further details.</em></p>";

$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= "From: noreply@igivemonthly.com \r\n";
$headers .= "Reply-To: noreply@igivemonthly.com \r\n";

mail("info@igivemonthly.com", "IGM - New charity recommended", $body_client, $headers);

# Go home

header('location: pages/charities/not-listed-thankyou/');