<?php

define('THUMBNAIL_IMAGE_WIDTH', 150);
define('THUMBNAIL_IMAGE_HEIGHT', 150);

function generate_image_thumbnail($source_image_path, $thumbnail_image_path)
{
    list($source_image_width, $source_image_height, $source_image_type) = getimagesize($source_image_path);
    switch ($source_image_type) {
        case IMAGETYPE_GIF:
            $source_gd_image = imagecreatefromgif($source_image_path);
            break;
        case IMAGETYPE_JPEG:
            $source_gd_image = imagecreatefromjpeg($source_image_path);
            break;
        case IMAGETYPE_PNG:
            $source_gd_image = imagecreatefrompng($source_image_path);
            break;
    }
    if ($source_gd_image === false) {
        return false;
    }
    
    /*$source_aspect_ratio = $source_image_width / $source_image_height;
    $thumbnail_aspect_ratio = THUMBNAIL_IMAGE_WIDTH / THUMBNAIL_IMAGE_HEIGHT;
    if ($source_image_width <= THUMBNAIL_IMAGE_WIDTH && $source_image_height <= THUMBNAIL_IMAGE_HEIGHT) {
        $thumbnail_image_width = $source_image_width;
        $thumbnail_image_height = $source_image_height;
    } elseif ($thumbnail_aspect_ratio > $source_aspect_ratio) {
        $thumbnail_image_width = (int) (THUMBNAIL_IMAGE_HEIGHT * $source_aspect_ratio);
        $thumbnail_image_height = THUMBNAIL_IMAGE_HEIGHT;
    } else {
        $thumbnail_image_width = THUMBNAIL_IMAGE_WIDTH;
        $thumbnail_image_height = (int) (THUMBNAIL_IMAGE_WIDTH / $source_aspect_ratio);
    }*/
    
    if ($source_image_width > $source_image_height) {
      $y = 0;
      $x = ($source_image_width - $source_image_height) / 2;
      $smallestSide = $source_image_height;
    } else {
      $x = 0;
      $y = ($source_image_height - $source_image_width) / 2;
      $smallestSide = $source_image_width;
    }
    
    $thumbnail_gd_image = imagecreatetruecolor(THUMBNAIL_IMAGE_WIDTH, THUMBNAIL_IMAGE_HEIGHT);
    imagecopyresampled($thumbnail_gd_image, $source_gd_image, 0, 0, $x, $y, THUMBNAIL_IMAGE_WIDTH, THUMBNAIL_IMAGE_HEIGHT, $smallestSide, $smallestSide);
    
    switch ($source_image_type) {
        case IMAGETYPE_GIF:
            imagegif($thumbnail_gd_image, $thumbnail_image_path);
            break;
        case IMAGETYPE_JPEG:
            imagejpeg($thumbnail_gd_image, $thumbnail_image_path, 90);
            break;
        case IMAGETYPE_PNG:
            imagepng($thumbnail_gd_image, $thumbnail_image_path);
            break;
    }
    
    imagedestroy($source_gd_image);
    imagedestroy($thumbnail_gd_image);
    return true;
}


try {

    // Undefined | Multiple Files | $_FILES Corruption Attack
    // If this request falls under any of them, treat it invalid.
    if (!isset($_FILES['file-avatar']['error']) || is_array($_FILES['file-avatar']['error'])) {
        throw new RuntimeException('Invalid parameters.');
    }

    if ($_FILES['file-avatar']['error'] != UPLOAD_ERR_NO_FILE) {

        // Check $_FILES['upfile']['error'] value.
        switch ($_FILES['file-avatar']['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new RuntimeException('No file sent.');
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                throw new RuntimeException('Exceeded filesize limit.');
            default:
                throw new RuntimeException('Unknown errors.');
        }

        // You should also check filesize here. 
        if ($_FILES['file-avatar']['size'] > 1000000) {
            throw new RuntimeException('Exceeded filesize limit.');
        }

        // DO NOT TRUST $_FILES['upfile']['mime'] VALUE !!
        // Check MIME Type by yourself.
        $finfo = finfo_open(FILEINFO_MIME_TYPE);

        if (false === $ext = array_search(
            finfo_file($finfo, $_FILES['file-avatar']['tmp_name']),
            array(
                'jpg' => 'image/jpeg',
                'png' => 'image/png',
                'gif' => 'image/gif'
            ),
            true
        )) {
            throw new RuntimeException('Invalid file format.');
        }
        finfo_close($finfo);

        // You should name it uniquely.
        // DO NOT USE $_FILES['upfile']['name'] WITHOUT ANY VALIDATION !!
        // On this example, obtain safe unique name from its binary data.
        $filename = sprintf('./images/avatars/%s.%s', $_SESSION['user_id'], $ext);
        $image_url = sprintf('images/avatars/%s.%s', $_SESSION['user_id'], $ext);
        if (!move_uploaded_file($_FILES['file-avatar']['tmp_name'], $filename)) {
            throw new RuntimeException('Failed to move uploaded file.');
        }

        if (!generate_image_thumbnail($filename, $filename)) {
            throw new RuntimeException('Failed to resize uploaded file.');
        }

    } else {
        $image_url = $_REQUEST['image_url'];
    }
    
    $member__s = "UPDATE users SET image_url = ? WHERE id = ? LIMIT 1";
    $member__q = $DB->prepare($member__s);
    $member__q->execute(array($image_url, $_SESSION['user_id']));

} catch (RuntimeException $e) {

    echo $e->getMessage();
    exit;

}

header('location: member/' . $_SESSION['user_id'] . '/');