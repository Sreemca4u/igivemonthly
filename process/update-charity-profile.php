<?php


$profile = $_REQUEST['profile'];
$contact_name = trim($_REQUEST['contact_name']);
$contact_job_title = trim($_REQUEST['contact_job_title']);
$contact_telephone = trim($_REQUEST['contact_telephone']);
$contact_email = trim($_REQUEST['contact_email']);


$member__s = "UPDATE charities SET profile = ?, contact_name = ?, contact_job_title = ?, contact_telephone = ?, contact_email = ? WHERE id = ? LIMIT 1";
$member__q = $DB->prepare($member__s);
$member__q->execute(array($profile, $contact_name, $contact_job_title, $contact_telephone, $contact_email, $_SESSION['charity__id']));


header("location: charity/$_SESSION[charity_id]/");
