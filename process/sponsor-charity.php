<?php
	

// Check for a form submission:
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$referer = $_SERVER['HTTP_REFERER'];

	// Stores errors:
	$errors = array();

	// Need a payment token:
	if (isset($_POST['stripeToken'])) {

		# Capture customer details
		$cid = $_SESSION['charity_id'] = $_POST['cid'];
		$uid = $_SESSION['user_id'];
		$price_int = 85.00;
		$price = 8500; // price in pence for stripe
		$token  = $_POST['stripeToken'];
		$email = $_POST['stripeEmail'];

	}

	// If no errors, process the order:
	if (empty($errors)) {

		// create the charge on Stripe's servers - this will charge the user's card
		try {

			// Include the Stripe library:
			require_once('scripts/stripe/lib/Stripe.php');

			// Set secret key:
			Stripe::setApiKey(Config::stripeApiKey());# NOTE: Config Stripe API Key

			// Create customer
			$customer = Stripe_Customer::create(array(
			  'email' => $email,
			  'card'  => $token
			));

			// Charge the order
			$charge = Stripe_Charge::create(array(
			  'customer' => $customer->id,
			  'amount'   => $price,
			  'currency' => 'gbp',
			  'receipt_email' => $email // Used for success & refund e-mails
			));

			// Check that it was paid:
			if ($charge->paid == true) {

                $s_add_order = "INSERT INTO sponsorships (charity_id, user_id, stripe_token, amount, expires_time) VALUES (?, ?, ?, ?, NOW() + INTERVAL 1 YEAR)";
				$q_add_order = $DB->prepare($s_add_order);
				$q_add_order->execute(array($cid, $uid, $token, $price_int));

				# Go away // existing code from before stripe implemented. unsure of temp cid.
				#$_SESSION['temp__cid'] = $cid;
				#session_write_close();
				
				header("location: pages/charities/thank-you/");

			} else { // Charge was not paid!
				$error = true;
			}

		} catch(Stripe_CardError $e) {
			$error = true;
		} catch (Stripe_InvalidRequestError $e) {
			$error = true;
		} catch (Stripe_AuthenticationError $e) {
			$error = true;
		} catch (Stripe_ApiConnectionError $e) {
			$error = true;
		} catch (Stripe_Error $e) {
			$error = true;
		} catch (Exception $e) {
			$error = true;
		}
		if($error == true) {
			header("location:" . $referer . '?error=true');
		}

	}

}