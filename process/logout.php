<html>
    <head>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="1082852130523-fre2gv3lahar34ercctpcqh8j3v81ass.apps.googleusercontent.com">
    <script>
        function signOut() {
            var auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function () {
                window.open('index.php', '_self');
            });
        }

        function onLoad() {
            gapi.load('auth2', function() {
                gapi.auth2.init();
            });

            setTimeout(signOut, 1000);
        }
    </script>
    <script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script>
</head>
<body>

</body>
</html>

<?php

session_destroy();

//header('location: index.php');