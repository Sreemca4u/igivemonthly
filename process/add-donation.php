<?php

$uid = $_SESSION['user_id'];

$stmt = $DB->prepare('select * from users where id = ?');
$stmt->execute(array($uid));
$user = $stmt->fetchObject();

if (array_key_exists('imported__email__id', $_REQUEST)) {
	$imported__email__id = $_REQUEST['imported__email__id'];
} else {
	$imported__email__id = null;
}
$charity = trim($_REQUEST['charity']);
$charity_id = 0;
if (is_numeric($charity)) {
	$charity_id = $charity;
	$charity_stmt = $DB->prepare('select * from charities where id = ?');
	$charity_stmt->execute([$charity]);
	$charity = $charity_stmt->fetch(PDO::FETCH_OBJ)->name;
}
$donation__via = trim($_REQUEST['donation__via']);
$relevant__date = trim($_REQUEST['relevant__date']);
$amount = trim($_REQUEST['amount']);
$time__ticker = isset($_REQUEST['time__ticker']);
if($time__ticker != 'y') { $time__ticker = 'MONEY'; } else { $time__ticker = 'TIME'; }

$info = trim(strip_tags($_REQUEST['info']));

$date_explode = explode("/", $relevant__date);
$makedate = date('Y-m-d', mktime(0, 0, 0, $date_explode[1], $date_explode[0], $date_explode[2]));

$Q = $DB->prepare('insert into donations (charity_id, user_id, amount, when_donated, reference, via) values (?, ?, ?, ?, ?, ?)');
$Q->execute(array($charity_id, $uid, $amount, $makedate, $info, $donation__via));# NOTE: Ref is always blank!

# Delete imported email if present
if($imported__email__id != NULL) {

	$Q = $DB->prepare("update emails set processed = 1 where id = ?");
	$Q->execute(array($imported__email__id));

};

# Go home

header("location: member/$_SESSION[user_id]/");
//print "<p><a href='member/$_SESSION[user_id]/'>Back</a></p>";
