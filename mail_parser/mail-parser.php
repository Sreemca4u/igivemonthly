<?php

function errorHandler($errno = null, $errstr = null, $errfile = null, $errline = null) {
    global $num,$imap,$i,$moved;
    if (!$moved && $num > 0) {
        #imap_createmailbox($imap, '{mail.gridhost.co.uk:143/notls/novalidate-cert}INBOX.errors');
        imap_mail_move($imap, $i + 1, 'INBOX.errors');
        $moved = true;
    }
    return true;
}

set_error_handler('errorHandler');
    
#require_once('../includes/includes.php');

require_once('../includes/config.php');
require_once('../config/config.php');
require_once('../includes/connect.php');

$imap = imap_open('{mail.gridhost.co.uk:143/notls/novalidate-cert}INBOX', 'donations@igivemonthly.com', 'dMalibu13');
if ($imap == false) {
    die('imap_open connection failed');
}
$num = imap_num_msg($imap);
if ($num > 0) {
    #echo 'num = '.$num;
    for ($i = 0; $i < $num; $i++) {

        $headers = imap_header($imap, $i + 1);
        
        #$body = quoted_printable_decode(imap_fetchbody($imap, $i + 1, 1));
        
        // pull the plain text for message $n 
        /*$structure = imap_fetchstructure($imap, $i + 1);
        
        var_dump($structure);
        
        if(isset($structure->parts) && is_array($structure->parts) && isset($structure->parts[1])) {
            $part = $structure->parts[1];
            $body = imap_fetchbody($imap, $i + 1,2);
            echo $part->encoding . '<br />';
            if($part->encoding == 3) {
                $body = imap_base64($body);
            } else if($part->encoding == 1) {
                $body = imap_8bit($body);
            } else {
                $body = quoted_printable_decode($body);
            }
        }*/
        
        $htmlmsg = $plainmsg = $charset = '';
        $moved = false;
        $attachments = array();
        getmsg($imap, $i + 1);
        #$body = base64_decode($body); // potential fix for emails sent from outlook not being recognised correctly. needs further investigation.

        $from = $headers->from[0]->mailbox . '@' . $headers->from[0]->host;
        $information = array();

        #var_dump($body);
        // die();
        if (stristr($plainmsg, 'justgiving.com')) {
            $information = parse_justgiving($plainmsg);
        } else if (stristr($plainmsg, 'globalgiving.co.uk')) {
            $information = parse_globalgiving($plainmsg);
        } else if (stristr($plainmsg, 'thebiggive.org.uk')) {
            $information = parse_thebiggive($plainmsg);
        } else if (stristr($plainmsg, 'virginmoneygiving.com')) {
            $information = parse_virginmoneygiving($plainmsg);
        } else if (stristr($plainmsg, 'givenow.com.au')) {
            $information = parse_givenow($plainmsg);
        } else if (stristr($plainmsg, 'mydonate')) {
            $information = parse_mydonate($plainmsg);
        }

        if (!empty($information)) {
            save_donation($information, $from, $DB, $plainmsg);
            #imap_createmailbox($imap, '{mail.gridhost.co.uk:143/notls/novalidate-cert}INBOX.processed');
            imap_mail_move($imap, $i + 1, 'INBOX.processed');
            $moved = true;
        }
    }
}

imap_expunge($imap);
imap_close($imap);

function getmsg($mbox,$mid) {
    // input $mbox = IMAP stream, $mid = message id
    // output all the following:
    global $charset,$htmlmsg,$plainmsg,$attachments;
    $htmlmsg = $plainmsg = $charset = '';
    $attachments = array();

    // HEADER
    $h = imap_header($mbox,$mid);
    // add code here to get date, from, to, cc, subject...

    // BODY
    $s = imap_fetchstructure($mbox,$mid);
    if (!isset($s->parts))  // simple
        getpart($mbox,$mid,$s,0);  // pass 0 as part-number
    else {  // multipart: cycle through each part
        foreach ($s->parts as $partno0=>$p)
            getpart($mbox,$mid,$p,$partno0+1);
    }
}

function getpart($mbox,$mid,$p,$partno) {
    // $partno = '1', '2', '2.1', '2.1.3', etc for multipart, 0 if simple
    global $htmlmsg,$plainmsg,$charset,$attachments;
    try {
        // DECODE DATA
        $data = ($partno)?
            imap_fetchbody($mbox,$mid,$partno):  // multipart
            imap_body($mbox,$mid);  // simple
        // Any part may be encoded, even plain text messages, so check everything.
        if ($p->encoding==4)
            $data = quoted_printable_decode($data);
        elseif ($p->encoding==3)
            $data = base64_decode($data);

        // PARAMETERS
        // get all parameters, like charset, filenames of attachments, etc.
        $params = array();
        if ($p->parameters)
            foreach ($p->parameters as $x)
                $params[strtolower($x->attribute)] = $x->value;
        if ($p->dparameters)
            foreach ($p->dparameters as $x)
                $params[strtolower($x->attribute)] = $x->value;

        // ATTACHMENT
        // Any part with a filename is an attachment,
        // so an attached text file (type 0) is not mistaken as the message.
        if ($params['filename'] || $params['name']) {
            // filename may be given as 'Filename' or 'Name' or both
            $filename = ($params['filename'])? $params['filename'] : $params['name'];
            // filename may be encoded, so see imap_mime_header_decode()
            $attachments[$filename] = $data;  // this is a problem if two files have same name
        }

        // TEXT
        if ($p->type==0 && $data) {
            // Messages may be split in different parts because of inline attachments,
            // so append parts together with blank row.
            if (strtolower($p->subtype)=='plain')
                $plainmsg .= trim($data) ."\n\n";
            else
                $htmlmsg .= $data ."<br><br>";
            $charset = $params['charset'];  // assume all parts are same charset
        }

        // EMBEDDED MESSAGE
        // Many bounce notifications embed the original message as type 2,
        // but AOL uses type 1 (multipart), which is not handled here.
        // There are no PHP functions to parse embedded messages,
        // so this just appends the raw source to the main message.
        elseif ($p->type==2 && $data) {
            $plainmsg .= $data."\n\n";
        }

        // SUBPART RECURSION
        if ($p->parts) {
            foreach ($p->parts as $partno0=>$p2)
                getpart($mbox,$mid,$p2,$partno.'.'.($partno0+1));  // 1.2, 1.2.1, etc.
        }
    } catch(Exception $e) {
        errorHandler();
    }
}

function save_donation($info, $email, $DB, $original_text)
{
    $stmt = $DB->prepare('select id from users where email = ?');
    $stmt->execute([$email]);

    // No matching user
    if ($stmt->rowCount() == 0) {
        $user_id = null;
    } else {
        $user_id = $stmt->fetchObject()->id;
    }

    $stmt = $DB->prepare('insert into emails (user_id, email, charity_name, amount, when_donated, reference, original_text, via) values (?, ?, ?, ?, ?, ?, ?, ?)');
    $stmt->execute(array($user_id, $email, $info['charity'], $info['amount'], $info['date']->format('Y-m-d H:i:s'), $info['reference'], $original_text, $info['via']));
}

function parse_justgiving($body) {

    try {
        preg_match('/Donation reference\s*\n(.*)\n/m', $body, $donation_reference);
        preg_match('/Donation amount\s*\n(.*)\n/m', $body, $donation_amount);
        preg_match('/Charity name\s*\n(.*)\n/m', $body, $charity_name);
        preg_match('/Date\s*\n(.*)\s\((.*)\)\s\n/m', $body, $date);

        $information = array(
            'reference' => trim($donation_reference[1]),
            'amount' => str_replace("£", "", trim($donation_amount[1])),
            'charity' => trim($charity_name[1]),
            'date' => new DateTime(trim($date[1])),
            'via' => 'Just Giving'
        );
    } catch(Exception $e) {
        errorHandler();
        $information = array();
    }

    return $information;
}

function parse_globalgiving($body) {
    try {
        preg_match('/Invoice #:\s(.*)\n/m', $body, $donation_reference);
        preg_match('/Amount:\s*\n(.*)\n/m', $body, $donation_amount);
        preg_match('/Project Title:\s*\n(.*)\n/m', $body, $charity_name);

        $information = array(
            'reference' => trim($donation_reference[1]),
            'amount' => str_replace("£", "", trim($donation_amount[1])),
            'charity' => trim($charity_name[1]),
            'date' => new DateTime(),
            'via' => 'Global Giving'
        );
    } catch(Exception $e) {
        errorHandler();
        $information = array();
    }

    return $information;
}

function parse_thebiggive($body) {
    try {
        preg_match('/reference number is (.*). Please/m', $body, $donation_reference);
        preg_match('/Your donation of (.*) has/m', $body, $donation_amount);
        preg_match('/supporting (.*) with/m', $body, $charity_name);

        $information = array(
            'reference' => trim($donation_reference[1]),
            'amount' => str_replace("£", "", trim($donation_amount[1])),
            'charity' => trim($charity_name[1]),
            'date' => new DateTime(),
            'via' => 'The Big Give'
        );
    } catch(Exception $e) {
        errorHandler();
        $information = array();
    }

    return $information;
}

function parse_virginmoneygiving($body) {
    try {
        preg_match('/Donation Reference :\s*\n(.*)\n/m', $body, $donation_reference);
        preg_match('/Amount :\s*\n(.*)\n/m', $body, $donation_amount);
        preg_match('/Charity :\s*\n(.*)\n/m', $body, $charity_name);

        $information = array(
            'reference' => trim($donation_reference[1]),
            'amount' => str_replace("£", "", trim($donation_amount[1])),
            'charity' => trim($charity_name[1]),
            'date' => new DateTime(),
            'via' => 'Virgin Money Giving'
        );
    } catch(Exception $e) {
        errorHandler();
        $information = array();
    }

    return $information;
}

function parse_givenow($body) {
    try {
        preg_match('/donation of \$(.*) in support/m', $body, $donation_amount);
        preg_match('/work of (.*).\n/m', $body, $charity_name);

        $information = array(
            'reference' => uniqid('giving-'),
            'amount' => str_replace("£", "", trim($donation_amount[1])),
            'charity' => trim($charity_name[1]),
            'date' => new DateTime(),
            'via' => 'GiveNow'
        );
    } catch(Exception $e) {
        errorHandler();
        $information = array();
    }

    return $information;
}

function parse_mydonate($body) {
    try {
        preg_match('/Donation Reference:\s*\n(.*)\n/m', $body, $donation_reference);
        preg_match('/Amount:\s*\n(.*)\n/m', $body, $donation_amount);
        preg_match('/Charity:\s*\n(.*)\n/m', $body, $charity_name);

        $information = array(
            'reference' => trim($donation_reference[1]),
            'amount' => str_replace("£", "", trim($donation_amount[1])),
            'charity' => trim($charity_name[1]),
            'date' => new DateTime(),
            'via' => 'Just Giving'
        );
     } catch(Exception $e) {
        errorHandler();
        $information = array();
    }

    return $information;
}