<?php

require_once('includes/includes.php');

$password = md5(SALT . $_REQUEST['password']);
$confirm = $_REQUEST['confirm'];
$unique__key = $_REQUEST['key'];
$reset__s = "UPDATE users SET password = ? WHERE reset_key = ? LIMIT 1";
$reset__q = $DB->prepare($reset__s);
$reset__q->execute(array($password, $unique__key));

// print "<p>Reset: $reset__s -- $unique__key</p>";

header('location: /pages/admin/reset-done/');