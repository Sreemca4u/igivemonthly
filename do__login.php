<?php

require_once('includes/includes.php');

$form__email = strtolower($_POST['email']);
$form__password = $_POST['password'];
$form__password = md5(SALT . $form__password);

if (isset($_SESSION['user_token'])) {
    $user_token = $_SESSION['user_token'];
    #$email = $_SESSION['email'];
    #$pic = $_SESSION['pic'];
} else {
    $user_token = null;
}

if (isset($_POST['fname'])) {
    $form__firstname = $_POST['fname'];
    $form__lastname = $_POST['lname'];
    #$form__dob = $_POST['dob'];
    $form__gender = $_POST['gender'];
    #$form__remember = $_POST['remember'];
}

# First we need to see if the user token in in the system first, if not, then we need to check via the form.
if ((GetUserIdForFBToken($user_token) == null) && (GetUserIdForGoogleToken($user_token) == null)) {
    # Social login NOT found, therefore check against OUR database
    $S = "SELECT * FROM users WHERE email = ? AND password = ? LIMIT 1";
    $Q = $DB->prepare($S);
    $Q->execute(array($form__email, $form__password));

    // print "<p>$S</p>"; exit();

    if ($Q->rowCount() != NULL) {

        #FOUND
        $D = $Q->fetchObject();
        $_SESSION['user_id'] = $D->id;
        
        header("location: member/$D->id/");

    } else {

        header('location: pages/members/not-found/');
    }

} else {


    # Social login FOUND
    if (GetUserIdForFBToken($user_token) == null) {
        $_SESSION['user_id'] = GetUserIdForGoogleToken($user_token)->id;
    } else {
        $_SESSION['user_id'] = GetUserIdForFBToken($user_token)->id;
    }
    session_write_close();
    header("location: member/$_SESSION[user_id]/");


}

