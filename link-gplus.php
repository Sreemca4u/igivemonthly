<?php

require_once('includes/includes.php');

include_once 'vendor/google-api-php-client/vendor/autoload.php';

$token = base64_decode($_GET['token']);

$google = new Google_Client();
//$google->setApplicationName('I Give Monthly');
//$google->setClientId($google_tokens->client_id);
//$google->setDeveloperKey($google_tokens->private_key);
$google->setAccessToken($token);
$google->authenticate($token);

$service = new Google_Service_Plus($google);
$me = $service->people->get('me');

$DB->prepare('update users set google_id = ? where id = ?')
    ->execute(array($me['id'], $_SESSION['user_id']));

header('location: pages/members/link/');