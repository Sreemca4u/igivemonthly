<?php

require_once('includes/includes.php');

$fb = new \Facebook\Facebook(array('app_id'=>Config::facebookAppId(), 'app_secret'=>Config::facebookAppSecret(), 'default_graph_version'=>Config::facebookVersion()));# NOTE: Config Facebook

$response = $fb->get('/me?fields=id,first_name,last_name,email,gender', $_GET['token']);

$_SESSION['fb_token'] = $response->getGraphUser()->getId();

$_POST['fname'] = $response->getGraphUser()->getFirstName();
$_POST['lname'] = $response->getGraphUser()->getLastName();
$_POST['email'] = $response->getGraphUser()->getField('email');
$_POST['gender'] = $response->getGraphUser()->getGender();
$_POST['password'] = $_SESSION['user_token'] = $_SESSION['fb_token'];

if (GetUserIdForFBToken($_SESSION['fb_token']) == null) {
    include_once 'do__register.php';
} else {
    include_once 'do__login.php';
}