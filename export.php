<?php
$DB = new PDO('mysql:host=10.169.0.12;dbname=igivemon_w6wr628', 'igivemon_w6wr628', 'd5tNbRan');

$tables = $DB->query('show tables')->fetchAll(PDO::FETCH_ASSOC);

$fp = fopen('output.sql', 'w');

$create_stmt = $DB->prepare('show create table ?');
foreach ($tables as $table) {
    $build_query = $DB->query('show create table ' . $table['Tables_in_igivemon_w6wr628'])->fetch(PDO::FETCH_ASSOC);
    $build_query = $build_query['Create Table'];
    fwrite($fp, $build_query.";\n\n");

    $file = __DIR__ . '/' . $table['Tables_in_igivemon_w6wr628'] . '.export';
    $entry_query = $DB->query('select * from ' . $table['Tables_in_igivemon_w6wr628']);

    while (($row = $entry_query->fetch(PDO::FETCH_ASSOC)) != null) {
        $values = array();
        foreach ($row as $col => $value) {
            $values[] = $DB->quote($value);
        }

        $query = 'insert into ' . $table['Tables_in_igivemon_w6wr628'] . ' (' . implode(',', array_keys($row)) . ') values (' . implode(",", $values) . ');';

        fwrite($fp, $query . "\n");
    }
}

fclose($fp);
