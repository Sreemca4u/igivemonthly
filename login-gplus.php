<?php

require_once('includes/includes.php');

require_once(Env::vendorPath() . 'autoload.php');

$client = new Google_Client();
$client->setDeveloperKey(Config::googleKey());
$client->setApplicationName('iGiveMonthly');
$client->setClientId(Config::googleClientId());
$client->setClientSecret(Config::googleClientSecret());
$client->setRedirectUri(Config::googleRedirectUri());
$client->addScope("https://www.googleapis.com/auth/userinfo.email");
$client->addScope("https://www.googleapis.com/auth/userinfo.profile");
$loginUrl = $client->createAuthUrl();

if (isset($_GET['code'])) {
    $client->authenticate($_GET['code']);
    $_SESSION['access_token'] = $client->getAccessToken();
    $service = new Google_Service_Oauth2($client);
    $me = $service->userinfo->get();
    $_SESSION['gplus_token'] = $_SESSION['user_token'] = $me['id'];
    $_POST['fname'] = $me['givenName'];
    $_POST['lname'] = $me['familyName'];
    $_POST['email'] = $me['email'];
    $_POST['gender'] = $me['gender'];
    $form__image = $me['picture'] . '?sz=150';
    $_POST['password'] = rand().uniqid();
    if (GetUserIdForGoogleToken($_SESSION['gplus_token']) == null) {
        include_once 'do__register.php';
    } else {
        include_once 'do__login.php';
    }
} elseif (!isset($_SESSION['access_token'])) {
    header('Location: ' . filter_var($loginUrl, FILTER_SANITIZE_URL));
    exit;
}